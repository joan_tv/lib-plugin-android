package com.npaw.youbora.lib6;

import static junit.framework.Assert.*;

import org.junit.Test;
import java.util.concurrent.CountDownLatch;

public class YouboraLogTest {

    @Test
    public void testLogCallbacks() throws Exception {

        final CountDownLatch lock = new CountDownLatch(6);

        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        YouboraLog.addLogger(new YouboraLog.YouboraLogger() {
            @Override
            public void logYouboraMessage(String message, YouboraLog.Level logLevel) {
                System.out.println(message);
                lock.countDown();
            }
        });

        YouboraLog.requestLog("requestLog");
        YouboraLog.debug("debug");
        YouboraLog.notice("notice");
        YouboraLog.warn("warn");
        YouboraLog.error("error");
        YouboraLog.error(new Exception());

        assertEquals(0, lock.getCount());
    }

    @Test
    public void testAddRemoveLoggers() {

        YouboraLog.YouboraLogger logger = new YouboraLog.YouboraLogger() {
            @Override
            public void logYouboraMessage(String message, YouboraLog.Level logLevel) {

            }
        };

        assertFalse(YouboraLog.removeLogger(logger));
        YouboraLog.addLogger(logger);
        YouboraLog.addLogger(logger);
        YouboraLog.addLogger(null);
        assertTrue(YouboraLog.removeLogger(logger));
        assertTrue(YouboraLog.removeLogger(logger));
        assertFalse(YouboraLog.removeLogger(logger));
    }
}
