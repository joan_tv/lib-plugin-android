package com.npaw.youbora.lib6.comm.transform.resourceparse;

import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnConfig;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnParsableResponseHeader;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class CdnParserTest {

    private static final String CUSTOM_CDN_CODE = "CSTM_CDN_CODE";
    private static final String CUSTOM_CDN_NAME = "CustomCdnName";

    @Before
    public void setUp() {

        // Create basic custom cdn definition
        CdnConfig cdnConfig = new CdnConfig(CUSTOM_CDN_CODE);
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.TypeAndHost, "X-Header", "(.+)\\sfrom\\s.+\\(.+\\/(.+)\\).*"))
                .setRequestHeader("header-name", "header-value")
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "TCP_HAT":
                                return Type.Hit;
                            case "TCP_MESS":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        CdnParser.addCdn(CUSTOM_CDN_NAME, cdnConfig);
    }

    @Test
    public void testDefaults() {

        CdnParser parser = CdnParser.create(CdnParser.CDN_NAME_AKAMAI);

        assertNull(parser.getNodeTypeString());
        assertEquals(CdnTypeParser.Type.Unknown, parser.getNodeType());
        assertNull(parser.getNodeHost());
        assertNull(parser.getCdnName());
    }

    @Test
    public void testCdnResolution() {

        final boolean[] callbackInvoked = {false};

        CdnParser parser = spy(CdnParser.create(CUSTOM_CDN_NAME));

        parser.addCdnTransformListener(new CdnParser.CdnTransformListener() {
            @Override
            public void onCdnTransformDone(CdnParser cdnParser) {
                callbackInvoked[0] = true;
            }
        });

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        parser.parse("resourceurl", null);

        // Verify requests fields are set
        verify(mockRequest, times(1)).setMethod(eq("HEAD"));
        verify(mockRequest, times(1)).setRequestHeaders(anyMap());
        verify(mockRequest, times(1)).setMaxRetries(eq(0));

        // Capture request callback
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(captor.capture());

        // Mock connection to invoke callback
        HttpURLConnection mockConnection = mock(HttpURLConnection.class);
        Map<String, List<String>> headers = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("TCP_HAT from a(a/HOST_VALUE)");
        headers.put("X-Header", headerValues);

        when(mockConnection.getHeaderFields()).thenReturn(headers);

        captor.getValue().onRequestSuccess(mockConnection, "");

        assertTrue(callbackInvoked[0]);
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST_VALUE", parser.getNodeHost());
        assertEquals("TCP_HAT", parser.getNodeTypeString());
        assertEquals(CUSTOM_CDN_CODE, parser.getCdnName());

    }

    @Test
    public void testRequestError() {
        final boolean[] callbackInvoked = {false};

        CdnParser parser = spy(CdnParser.create(CUSTOM_CDN_NAME));

        parser.addCdnTransformListener(new CdnParser.CdnTransformListener() {
            @Override
            public void onCdnTransformDone(CdnParser cdnParser) {
                callbackInvoked[0] = true;
            }
        });

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        parser.parse("resourceurl", null);

        // Capture request callback
        ArgumentCaptor<Request.RequestErrorListener> captor = ArgumentCaptor.forClass(Request.RequestErrorListener.class);
        verify(mockRequest, atLeastOnce()).addOnErrorListener(captor.capture());

        captor.getValue().onRequestError(mock(HttpURLConnection.class));

        // Callback called
        assertTrue(callbackInvoked[0]);

        // Check default values
        assertNull(parser.getNodeTypeString());
        assertEquals(CdnTypeParser.Type.Unknown, parser.getNodeType());
        assertNull(parser.getNodeHost());
        assertNull(parser.getCdnName());
    }

    @Test
    public void testListeners() {
        CdnParser parser = CdnParser.create(CUSTOM_CDN_NAME);

        CdnParser.CdnTransformListener listener = new CdnParser.CdnTransformListener() {
            @Override
            public void onCdnTransformDone(CdnParser cdnParser) {

            }
        };

        assertFalse(parser.removeCdnTransformListener(listener));
        assertFalse(parser.removeCdnTransformListener(null));

        parser.addCdnTransformListener(listener);

        assertFalse(parser.removeCdnTransformListener(null));
        assertTrue(parser.removeCdnTransformListener(listener));
        assertFalse(parser.removeCdnTransformListener(listener));
    }

    @Test
    public void testTwoElementsCdn() {

        // Create custom cdn definition
        CdnConfig cdnConfig = new CdnConfig("TWO_ELEMENT_CDN");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Type, "Header-type", "(.+)"))
                .addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Host, "Header-host", "(.+)"))
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "HIT":
                                return Type.Hit;
                            case "MISS":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        CdnParser.addCdn("TwoElementCdn", cdnConfig);

        CdnParser parser = spy(CdnParser.create("TwoElementCdn"));

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        parser.parse("resource", null);

        // Capture request callback
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(captor.capture());

        // Mock connection to invoke callback
        HttpURLConnection mockConnection = mock(HttpURLConnection.class);
        Map<String, List<String>> headers = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("MISS");
        headers.put("Header-type", headerValues);

        headerValues = new ArrayList<>();
        headerValues.add("cdn_host_value");
        headers.put("Header-host", headerValues);

        when(mockConnection.getHeaderFields()).thenReturn(headers);

        captor.getValue().onRequestSuccess(mockConnection, "");

        assertEquals(CdnTypeParser.Type.Miss, parser.getNodeType());
        assertEquals("MISS", parser.getNodeTypeString());
        assertEquals("cdn_host_value", parser.getNodeHost());
        assertEquals("TWO_ELEMENT_CDN", parser.getCdnName());
    }

    @Test
    public void testInvalidCdnName() {
        assertNull(CdnParser.create("NonExistentCdnName"));
        assertNull(CdnParser.create(null));
    }

    @Test
    public void testCdnResolutionWithResponses() {
        // This cdn resolution should be done without creating a request, since
        // we will provide the needed responses.
        CdnParser parser = spy(CdnParser.create(CUSTOM_CDN_NAME));

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, String> request = new HashMap<>();
        request.put("header-name", "header-value");

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("TCP_MESS from a(a/HOST_VALUE)");
        response.put("X-Header", headerValues);

        responses.put(request, response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Miss, parser.getNodeType());
        assertEquals("HOST_VALUE", parser.getNodeHost());
        assertEquals("TCP_MESS", parser.getNodeTypeString());
        assertEquals(CUSTOM_CDN_CODE, parser.getCdnName());

        // Check that no Requests have been created
        verify(parser, times(0)).createRequest(anyString(), anyString());

    }

    @Test
    public void testCdnNameFromBalancer() {
        CdnParser.setBalancerHeaderName("cdn-name");

        CdnParser parser = CdnParser.create(CdnParser.CDN_BALANCER);

        String cdnName = "cdn-name-from-balancer";

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add(cdnName);
        response.put("cdn-name", headerValues);

        responses.put(new HashMap<String, String>(), response);

        parser.parse("resource", responses);

        assertEquals(cdnName.toUpperCase(), parser.getCdnName());
    }

    @Test
    public void testHostAndType() {
        // Create basic custom cdn definition
        CdnConfig cdnConfig = new CdnConfig("HOST_AND_TYPE_CDN");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.HostAndType, "X-Header", "(.+)\\/(.+)"))
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "TCP_HAT":
                                return Type.Hit;
                            case "TCP_MESS":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        CdnParser.addCdn("HostAndTypeCdn", cdnConfig);

        CdnParser parser = CdnParser.create("HostAndTypeCdn");

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("HOST/TCP_HAT");
        response.put("X-Header", headerValues);

        responses.put(new HashMap<String, String>(), response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST", parser.getNodeHost());
        assertEquals("TCP_HAT", parser.getNodeTypeString());
        assertEquals("HOST_AND_TYPE_CDN", parser.getCdnName());
    }

    // CDN-specific tests
    @Test
    public void testLevel3() {
        CdnParser parser = CdnParser.create(CdnParser.CDN_NAME_LEVEL3);

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, String> request = new HashMap<>();
        request.put("X-WR-DIAG", "host");

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("Host:HOST123 Type:TCP_MEM_HIT");
        response.put("X-WR-DIAG", headerValues);

        responses.put(request, response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST123", parser.getNodeHost());
        assertEquals("TCP_MEM_HIT", parser.getNodeTypeString());
        assertEquals("LEVEL3", parser.getCdnName());
    }

    @Test
    public void testCloudfront() {
        CdnParser parser = CdnParser.create(CdnParser.CDN_NAME_CLOUDFRONT);

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("HOST123");
        response.put("X-Amz-Cf-Id", headerValues);
        headerValues = new ArrayList<>();
        headerValues.add("Hit a");
        response.put("X-Cache", headerValues);

        responses.put(new HashMap<String, String>(), response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST123", parser.getNodeHost());
        assertEquals("Hit", parser.getNodeTypeString());
        assertEquals("CLOUDFRT", parser.getCdnName());
    }

    @Test
    public void testHighwinds() {
        CdnParser parser = CdnParser.create(CdnParser.CDN_NAME_HIGHWINDS);

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("a,0123.HOST123.c");
        response.put("X-HW", headerValues);

        responses.put(new HashMap<String, String>(), response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST123", parser.getNodeHost());
        assertEquals("c", parser.getNodeTypeString());
        assertEquals("HIGHNEGR", parser.getCdnName());
    }

    @Test
    public void testFastly() {
        CdnParser parser = CdnParser.create(CdnParser.CDN_NAME_FASTLY);

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, String> request = new HashMap<>();
        request.put("X-WR-DIAG", "host");

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("HOST123");
        response.put("X-Served-By", headerValues);
        headerValues = new ArrayList<>();
        headerValues.add("HIT");
        response.put("X-Cache", headerValues);

        responses.put(request, response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST123", parser.getNodeHost());
        assertEquals("HIT", parser.getNodeTypeString());
        assertEquals("FASTLY", parser.getCdnName());
    }

    @Test
    public void testAkamai() {
        CdnParser parser = CdnParser.create(CdnParser.CDN_NAME_AKAMAI);

        Map<Map<String, String>, Map<String, List<String>>> responses = new HashMap<>();

        Map<String, List<String>> response = new HashMap<>();
        List<String> headerValues = new ArrayList<>();
        headerValues.add("TCP_HIT from a(a/HOST123)");
        response.put("X-Cache", headerValues);

        responses.put(new HashMap<String, String>(), response);

        parser.parse("resource", responses);

        // Check successful parsing
        assertEquals(CdnTypeParser.Type.Hit, parser.getNodeType());
        assertEquals("HOST123", parser.getNodeHost());
        assertEquals("TCP_HIT", parser.getNodeTypeString());
        assertEquals("AKAMAI", parser.getCdnName());
    }
}