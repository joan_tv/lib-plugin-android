package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.adapter.PlaybackChronos;
import com.npaw.youbora.lib6.adapter.PlaybackFlags;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Communication;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.FlowTransform;
import com.npaw.youbora.lib6.comm.transform.Nqs6Transform;
import com.npaw.youbora.lib6.comm.transform.ResourceTransform;
import com.npaw.youbora.lib6.comm.transform.ViewTransform;

import org.junit.Before;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PluginMocker {

    protected class TestPlugin extends Plugin {
        public TestPlugin(Options options) {
            super(options);
        }
        public TestPlugin(Options options, PlayerAdapter adapter) {
            super(options, adapter);
        }

        @Override
        Chrono createChrono() {
            return mockChrono;
        }

        @Override
        Options createOptions() {
            return mockOptions;
        }

        @Override
        Timer createTimer(Timer.TimerEventListener listener, long interval) {
            // Capture timer listener
            timerEventListener = listener;
            return mockTimer;
        }

        @Override
        RequestBuilder createRequestBuilder(Plugin plugin) {
            return mockRequestBuilder;
        }

        @Override
        ResourceTransform createResourceTransform(Plugin plugin) {
            return mockResourceTransform;
        }

        @Override
        ViewTransform createViewTransform(Plugin plugin) {
            return mockViewTransform;
        }

        @Override
        Communication createCommunication(){
            return mockCommunication;
        }

        @Override
        Nqs6Transform createNqs6Transform() {
            return mockNqs6Transform;
        }

        @Override
        FlowTransform createFlowTransform() {
            return mockFlowTransform;
        }

        @Override
        Request createRequest(String host, String service) {
            return mockRequest;
        }
    }

    protected TestPlugin p;

    protected Chrono mockChrono = null;
    protected Timer mockTimer = null;
    protected Options mockOptions = null;
    protected RequestBuilder mockRequestBuilder = null;
    protected ResourceTransform mockResourceTransform = null;
    protected FlowTransform mockFlowTransform = null;
    protected Nqs6Transform mockNqs6Transform = null;
    protected ViewTransform mockViewTransform = null;
    protected Communication mockCommunication = null;
    protected Request mockRequest = null;
    protected PlayerAdapter mockAdapter = null;
    protected PlayerAdapter mockAdAdapter = null;
    protected PlayerAdapter.AdapterEventListener adapterEventListener = null;
    protected PlayerAdapter.AdapterEventListener adAdapterEventListener = null;
    protected Timer.TimerEventListener timerEventListener = null;

    protected static final Double [] DOUBLE_INVALID = new Double[] {null, Double.MAX_VALUE,
            Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN};

    protected static final Integer [] INTEGER_INVALID = new Integer[] {null, Integer.MAX_VALUE,
            Integer.MIN_VALUE};

    protected static final Long [] LONG_INVALID = new Long[] {null, Long.MAX_VALUE, Long.MIN_VALUE};

    @Before
    public void before() throws Exception {

        // Mock classes used by the Plugin
        mockChrono = mock(Chrono.class);
        mockTimer = mock(Timer.class);
        mockOptions = mock(Options.class);
        mockRequestBuilder = mock(RequestBuilder.class);
        mockResourceTransform = mock(ResourceTransform.class);
        mockFlowTransform = mock(FlowTransform.class);
        mockNqs6Transform = mock(Nqs6Transform.class);
        mockViewTransform = mock(ViewTransform.class);
        mockCommunication = mock(Communication.class);
        mockRequest = mock(Request.class);
        mockAdapter = mock(PlayerAdapter.class);
        mockAdAdapter = mock(PlayerAdapter.class);

        p = new TestPlugin(mockOptions, mockAdapter);

        // Capture adapter listener
        ArgumentCaptor<PlayerAdapter.AdapterEventListener> listenerCaptor = ArgumentCaptor.forClass(PlayerAdapter.AdapterEventListener.class);
        verify(mockAdapter).addEventListener(listenerCaptor.capture());
        adapterEventListener = listenerCaptor.getValue();

        p.setAdsAdapter(mockAdAdapter);

        // Capture adAdapter listener
        verify(mockAdAdapter).addEventListener(listenerCaptor.capture());
        adAdapterEventListener = listenerCaptor.getValue();

        // Provide the adapters with real flags and chronos
        PlaybackFlags adapterFlags = new PlaybackFlags();
        PlaybackChronos adapterChronos = new PlaybackChronos();
        when(mockAdapter.getFlags()).thenReturn(adapterFlags);
        when(mockAdapter.getChronos()).thenReturn(adapterChronos);

        PlaybackFlags adAdapterFlags = new PlaybackFlags();
        PlaybackChronos adAdapterChronos = new PlaybackChronos();
        when(mockAdAdapter.getFlags()).thenReturn(adAdapterFlags);
        when(mockAdAdapter.getChronos()).thenReturn(adAdapterChronos);
    }
}
