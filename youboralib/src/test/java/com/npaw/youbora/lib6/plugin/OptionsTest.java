package com.npaw.youbora.lib6.plugin;

import android.os.Bundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class OptionsTest {
    private Options o;

    @Before
    public void setUp() throws Exception {
        o = new Options();
    }

    @After
    public void tearDown() throws Exception {
        o = null;
    }

    @Test
    public void testOptions() {
        setValues(o);
        assertValues(o);
    }

    private void setValues(Options o) {

        // Set values
        o.setEnabled(false);
        o.setHttpSecure(true);
        o.setHost("a");
        o.setAccountCode("b");
        o.setUsername("c");
        o.setParseHls(true);
        o.setParseCdnNameHeader("d");
        o.setParseCdnNode(true);
        ArrayList<String> cdnNodes = new ArrayList<>();
        cdnNodes.add("listitem1");
        cdnNodes.add("listitem2");
        o.setParseCdnNodeList(cdnNodes);
        o.setNetworkIP("f");
        o.setNetworkIsp("g");
        o.setNetworkConnectionType("h");
        o.setDeviceCode("i");
        o.setContentResource("j");
        o.setContentIsLive(true);
        o.setContentTitle("k");
        o.setContentTitle2("l");
        o.setContentDuration(1.0);
        o.setContentTransactionCode("m");
        o.setContentBitrate(2L);
        o.setContentThroughput(3L);
        o.setContentRendition("n");
        o.setContentCdn("o");
        o.setContentFps(4.0);
        Bundle b = mock(Bundle.class);
        when(b.getString("p")).thenReturn("q");
        b.putString("p", "q");
        o.setContentMetadata(b);
        Bundle b2 = mock(Bundle.class);
        when(b2.getString("r")).thenReturn("s");
        b.putString("r", "s");
        o.setAdMetadata(b2);
        o.setOffline(false);
        o.setExtraparam1("t");
        o.setExtraparam2("u");
        o.setExtraparam3("v");
        o.setExtraparam4("w");
        o.setExtraparam5("x");
        o.setExtraparam6("y");
        o.setExtraparam7("z");
        o.setExtraparam8("aa");
        o.setExtraparam9("ab");
        o.setExtraparam10("ac");
    }

    private void assertValues(Options o) {

        // Verify
        assertEquals(false, o.isEnabled());
        assertEquals(true, o.isHttpSecure());
        assertEquals("a", o.getHost());
        assertEquals("b", o.getAccountCode());
        assertEquals("c", o.getUsername());
        assertEquals(true, o.isParseHls());
        assertEquals("d", o.getParseCdnNameHeader());
        assertEquals(true, o.isParseCdnNode());
        assertEquals("listitem1", o.getParseCdnNodeList().get(0));
        assertEquals("listitem2", o.getParseCdnNodeList().get(1));
        assertEquals("f", o.getNetworkIP());
        assertEquals("g", o.getNetworkIsp());
        assertEquals("h", o.getNetworkConnectionType());
        assertEquals("i", o.getDeviceCode());
        assertEquals("j", o.getContentResource());
        assertEquals(true, o.getContentIsLive());
        assertEquals("k", o.getContentTitle());
        assertEquals("l", o.getContentTitle2());
        assertEquals((Double) 1.0, o.getContentDuration());
        assertEquals("m", o.getContentTransactionCode());
        assertEquals((Long) 2L, o.getContentBitrate());
        assertEquals((Long) 3L, o.getContentThroughput());
        assertEquals("n", o.getContentRendition());
        assertEquals("o", o.getContentCdn());
        assertEquals((Double) 4.0, o.getContentFps());
        assertEquals("q", o.getContentMetadata().getString("p"));
        assertEquals("s", o.getAdMetadata().getString("r"));
        assertEquals(false,o.isOffline());
        assertEquals("t", o.getExtraparam1());
        assertEquals("u", o.getExtraparam2());
        assertEquals("v", o.getExtraparam3());
        assertEquals("w", o.getExtraparam4());
        assertEquals("x", o.getExtraparam5());
        assertEquals("y", o.getExtraparam6());
        assertEquals("z", o.getExtraparam7());
        assertEquals("aa", o.getExtraparam8());
        assertEquals("ab", o.getExtraparam9());
        assertEquals("ac", o.getExtraparam10());
    }
}