package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.adapter.PlaybackChronos;
import com.npaw.youbora.lib6.adapter.PlaybackFlags;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.Transform;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PluginTest extends PluginMocker {

    @Test
    public void testConstructorPerformedOperations() throws Exception {

        verify(mockViewTransform, times(1)).addTransformDoneListener(any(Transform.TransformDoneListener.class));

        verify(mockViewTransform, times(1)).init();

        verify(mockAdapter, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }

    @Test
    public void testSetOptions() throws Exception {

        // Mock options
        Options o = mock(Options.class);
        when(o.getAccountCode()).thenReturn("a");

        Plugin p = new TestPlugin(o);

        assertEquals("a", p.getOptions().getAccountCode());
    }

    @Test
    public void testAddAndRemoveAdapters() {

        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        p.setAdapter(mockAdapter);

        assertEquals(mockAdapter, p.getAdapter());

        verify(mockAdapter, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        PlayerAdapter mockAdapter2 = mock(PlayerAdapter.class);
        p.setAdapter(mockAdapter2);

        assertEquals(mockAdapter2, p.getAdapter());

        verify(mockAdapter, times(1)).dispose();
        verify(mockAdapter, times(1)).removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

        verify(mockAdapter2, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        p.removeAdapter();
        verify(mockAdapter2, times(1)).dispose();
        verify(mockAdapter2, times(1)).removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

    }

    @Test
    public void testAddAndRemoveAdsAdapters() {

        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        p.setAdsAdapter(mockAdapter);

        assertEquals(mockAdapter, p.getAdsAdapter());

        verify(mockAdapter, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        PlayerAdapter mockAdapter2 = mock(PlayerAdapter.class);
        p.setAdsAdapter(mockAdapter2);

        assertEquals(mockAdapter2, p.getAdsAdapter());

        verify(mockAdapter, times(1)).dispose();
        verify(mockAdapter, times(1)).removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

        verify(mockAdapter2, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        p.removeAdsAdapter();
        verify(mockAdapter2, times(1)).dispose();
        verify(mockAdapter2, times(1)).removeEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }


    @Test
    public void testEnableDisable() {

        p.enable();

        verify(mockOptions).setEnabled(eq(true));

        p.disable();

        verify(mockOptions).setEnabled(eq(false));
    }

    @Test
    public void testPreloads() {

        p.firePreloadBegin();
        p.firePreloadBegin();
        verify(mockChrono, times(1)).start();

        p.firePreloadEnd();
        p.firePreloadEnd();
        verify(mockChrono, times(1)).stop();
    }

    // Test get info
    @Test
    public void testGetHost() {
        when(mockOptions.getHost()).thenReturn("http://host.com");
        when(mockOptions.isHttpSecure()).thenReturn(true);

        assertEquals("https://host.com", p.getHost());
    }

    @Test
    public void testParseHls() {
        when(mockOptions.isParseHls()).thenReturn(true);
        assertTrue(p.isParseHls());

        when(mockOptions.isParseHls()).thenReturn(false);
        assertFalse(p.isParseHls());
    }

    @Test
    public void testCdnNode() {
        when(mockOptions.isParseCdnNode()).thenReturn(true);
        assertTrue(p.isParseCdnNode());

        when(mockOptions.isParseCdnNode()).thenReturn(false);
        assertFalse(p.isParseHls());
    }

    @Test
    public void testParseCdnNodeList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("item1");
        list.add("item2");
        list.add("item3");
        when(mockOptions.getParseCdnNodeList()).thenReturn(null);
        assertNull(p.getParseCdnNodeList());

        when(mockOptions.getParseCdnNodeList()).thenReturn(list);
        assertEquals(list, p.getParseCdnNodeList());
    }

    @Test
    public void testParseCdnNodeHeader() {
        when(mockOptions.getParseCdnNameHeader()).thenReturn("x-header");
        assertEquals("x-header", p.getParseCdnNodeNameHeader());
    }

    @Test
    public void testPlayhead() {

        // Valid values
        when(mockAdapter.getPlayhead()).thenReturn(-10.0);
        assertEquals(Double.valueOf(-10.0), p.getPlayhead());

        when(mockAdapter.getPlayhead()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getPlayhead());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getPlayhead()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getPlayhead());
        }
    }

    @Test
    public void testRate() {

        // Valid values
        when(mockAdapter.getPlayrate()).thenReturn(-10.0);
        assertEquals(Double.valueOf(-10.0), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(-0.5);
        assertEquals(Double.valueOf(-0.5), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getPlayrate());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getPlayrate()).thenReturn(d);
            assertEquals(Double.valueOf(1.0), p.getPlayrate());
        }
    }

    @Test
    public void testFps() {
        when(mockOptions.getContentFps()).thenReturn(25.0);
        when(mockAdapter.getFramesPerSecond()).thenReturn(15.0);
        assertEquals(Double.valueOf(25.0), p.getFramesPerSecond());

        when(mockOptions.getContentFps()).thenReturn(null);
        when(mockAdapter.getFramesPerSecond()).thenReturn(15.5);
        assertEquals(Double.valueOf(15.5), p.getFramesPerSecond());

        when(mockOptions.getContentFps()).thenReturn(null);
        when(mockAdapter.getFramesPerSecond()).thenReturn(null);
        assertNull(p.getFramesPerSecond());
    }

    @Test
    public void testDroppedFrames() {
        // Valid value
        when(mockAdapter.getDroppedFrames()).thenReturn(10);
        assertEquals(Integer.valueOf(10), p.getDroppedFrames());

        // Invalid values
        for (Integer i : INTEGER_INVALID) {
            when(mockAdapter.getDroppedFrames()).thenReturn(i);
            assertEquals(Integer.valueOf(0), p.getDroppedFrames());
        }
    }

    @Test
    public void testDuration() {

        when(mockOptions.getContentDuration()).thenReturn(null);

        // Valid values
        when(mockAdapter.getDuration()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getDuration());

        when(mockAdapter.getDuration()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getDuration());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getDuration()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getDuration());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentDuration()).thenReturn(1.0);

        when(mockAdapter.getDuration()).thenReturn(2.0);
        assertEquals(Double.valueOf(1.0), p.getDuration());
    }

    @Test
    public void testBitrate() {
        when(mockOptions.getContentBitrate()).thenReturn(null);

        // Valid values
        when(mockAdapter.getBitrate()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getBitrate());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getBitrate()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getBitrate());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentBitrate()).thenReturn(1000000L);

        when(mockAdapter.getBitrate()).thenReturn(2000000L);
        assertEquals(Long.valueOf(1000000), p.getBitrate());
    }

    @Test
    public void testThroughput() {
        when(mockOptions.getContentThroughput()).thenReturn(null);

        // Valid values
        when(mockAdapter.getThroughput()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getThroughput());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getThroughput()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getThroughput());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentThroughput()).thenReturn(1000000L);

        when(mockAdapter.getThroughput()).thenReturn(2000000L);
        assertEquals(Long.valueOf(1000000), p.getThroughput());
    }

    @Test
    public void testRendition() {
        when(mockOptions.getContentRendition()).thenReturn(null);

        when(mockAdapter.getRendition()).thenReturn("1Mbps");
        assertEquals("1Mbps", p.getRendition());

        when(mockAdapter.getRendition()).thenReturn("");
        assertEquals("", p.getRendition());

        when(mockAdapter.getRendition()).thenReturn(null);
        assertNull(p.getRendition());

        // Test options prevalence over adapter
        when(mockOptions.getContentRendition()).thenReturn("2Mbps");
        when(mockAdapter.getRendition()).thenReturn("1Mbps");
        assertEquals("2Mbps", p.getRendition());

        // unless it's empty
        when(mockOptions.getContentRendition()).thenReturn("");
        assertEquals("1Mbps", p.getRendition());
    }

    @Test
    public void testTitle() {
        when(mockOptions.getContentTitle()).thenReturn(null);

        when(mockAdapter.getTitle()).thenReturn("batman");
        assertEquals("batman", p.getTitle());

        when(mockAdapter.getTitle()).thenReturn("");
        assertEquals("", p.getTitle());

        when(mockAdapter.getTitle()).thenReturn(null);
        assertNull(p.getTitle());

        // Test options prevalence over adapter
        when(mockOptions.getContentTitle()).thenReturn("iron man");
        when(mockAdapter.getTitle()).thenReturn("batman");
        assertEquals("iron man", p.getTitle());

        // unless it's empty
        when(mockOptions.getContentTitle()).thenReturn("");
        assertEquals("batman", p.getTitle());
    }

    @Test
    public void testTitle2() {
        when(mockOptions.getContentTitle2()).thenReturn(null);

        when(mockAdapter.getTitle2()).thenReturn("episode 1");
        assertEquals("episode 1", p.getTitle2());

        when(mockAdapter.getTitle2()).thenReturn("");
        assertEquals("", p.getTitle2());

        when(mockAdapter.getTitle2()).thenReturn(null);
        assertNull(p.getTitle2());

        // Test options prevalence over adapter
        when(mockOptions.getContentTitle2()).thenReturn("episode 1");
        when(mockAdapter.getTitle2()).thenReturn("episode 2");
        assertEquals("episode 1", p.getTitle2());

        // unless it's empty
        when(mockOptions.getContentTitle2()).thenReturn("");
        assertEquals("episode 2", p.getTitle2());
    }

    @Test
    public void testLive() {
        when(mockOptions.getContentIsLive()).thenReturn(null);

        // Default
        when(mockAdapter.getIsLive()).thenReturn(null);
        assertFalse(p.getIsLive());

        when(mockAdapter.getIsLive()).thenReturn(false);
        assertFalse(p.getIsLive());

        when(mockAdapter.getIsLive()).thenReturn(true);
        assertTrue(p.getIsLive());

        // Test options prevalence over adapter
        when(mockOptions.getContentIsLive()).thenReturn(false);
        assertFalse(p.getIsLive());

        when(mockOptions.getContentIsLive()).thenReturn(true);
        when(mockAdapter.getIsLive()).thenReturn(false);
        assertTrue(p.getIsLive());
    }

    @Test
    public void testGetResource() {

        when(mockResourceTransform.isBlocking(any(Request.class))).thenReturn(true);
        when(mockOptions.getContentResource()).thenReturn("ResourceFromOptions");
        when(mockResourceTransform.getResource()).thenReturn("ResourceFromTransform");
        when(mockAdapter.getResource()).thenReturn("ResourceFromAdapter");

        assertEquals("ResourceFromOptions", p.getResource());

        when(mockResourceTransform.isBlocking(any(Request.class))).thenReturn(false);
        assertEquals("ResourceFromTransform", p.getResource());

        when(mockOptions.getContentResource()).thenReturn(null);
        when(mockResourceTransform.getResource()).thenReturn(null);

        assertEquals("ResourceFromAdapter", p.getResource());

        when(mockAdapter.getResource()).thenReturn(null);

        assertNull(p.getResource());
    }

    @Test
    public void testTransactionCode() {
        when(mockOptions.getContentTransactionCode()).thenReturn("transactionCode");

        assertEquals("transactionCode", p.getTransactionCode());

        when(mockOptions.getContentTransactionCode()).thenReturn(null);

        assertNull(p.getTransactionCode());
    }

    @Test
    public void testPlayerVersion() {
        when(mockAdapter.getPlayerVersion()).thenReturn(null);

        assertEquals("", p.getPlayerVersion());

        when(mockAdapter.getPlayerVersion()).thenReturn("1.2.3");

        assertEquals("1.2.3", p.getPlayerVersion());
    }

    @Test
    public void testPlayerName() {
        when(mockAdapter.getPlayerName()).thenReturn(null);

        assertEquals("", p.getPlayerName());

        when(mockAdapter.getPlayerName()).thenReturn("player-name");

        assertEquals("player-name", p.getPlayerName());
    }

    @Test
    public void testCdn() {
        when(mockResourceTransform.isBlocking(any(Request.class))).thenReturn(true);
        when(mockOptions.getContentCdn()).thenReturn("CdnFromOptions");
        when(mockResourceTransform.getCdnName()).thenReturn("CdnFromTransform");

        assertEquals("CdnFromOptions", p.getCdn());

        when(mockResourceTransform.isBlocking(any(Request.class))).thenReturn(false);
        assertEquals("CdnFromTransform", p.getCdn());

        when(mockResourceTransform.getCdnName()).thenReturn(null);
        assertEquals("CdnFromOptions", p.getCdn());

        when(mockOptions.getContentCdn()).thenReturn(null);
        assertNull(p.getCdn());
    }

    @Test
    public void testPluginVersion() {
        when(mockAdapter.getVersion()).thenReturn(null);

        assertEquals(BuildConfig.VERSION_NAME+"-adapterless", p.getPluginVersion());

        when(mockAdapter.getVersion()).thenReturn("6.0.0-CustomPlugin");

        assertEquals("6.0.0-CustomPlugin", p.getPluginVersion());

        p.removeAdapter();

        assertEquals(BuildConfig.VERSION_NAME+"-adapterless", p.getPluginVersion());
    }

    @Test
    public void testExtraparams() {

        when(mockOptions.getExtraparam1()).thenReturn("value-extraparam1");
        when(mockOptions.getExtraparam2()).thenReturn("value-extraparam2");
        when(mockOptions.getExtraparam3()).thenReturn("value-extraparam3");
        when(mockOptions.getExtraparam4()).thenReturn("value-extraparam4");
        when(mockOptions.getExtraparam5()).thenReturn("value-extraparam5");
        when(mockOptions.getExtraparam6()).thenReturn("value-extraparam6");
        when(mockOptions.getExtraparam7()).thenReturn("value-extraparam7");
        when(mockOptions.getExtraparam8()).thenReturn("value-extraparam8");
        when(mockOptions.getExtraparam9()).thenReturn("value-extraparam9");
        when(mockOptions.getExtraparam10()).thenReturn("value-extraparam10");


        assertEquals("value-extraparam1", p.getExtraparam1());
        assertEquals("value-extraparam2", p.getExtraparam2());
        assertEquals("value-extraparam3", p.getExtraparam3());
        assertEquals("value-extraparam4", p.getExtraparam4());
        assertEquals("value-extraparam5", p.getExtraparam5());
        assertEquals("value-extraparam6", p.getExtraparam6());
        assertEquals("value-extraparam7", p.getExtraparam7());
        assertEquals("value-extraparam8", p.getExtraparam8());
        assertEquals("value-extraparam9", p.getExtraparam9());
        assertEquals("value-extraparam10", p.getExtraparam10());

        when(mockOptions.getExtraparam1()).thenReturn(null);
        when(mockOptions.getExtraparam2()).thenReturn(null);
        when(mockOptions.getExtraparam3()).thenReturn(null);
        when(mockOptions.getExtraparam4()).thenReturn(null);
        when(mockOptions.getExtraparam5()).thenReturn(null);
        when(mockOptions.getExtraparam6()).thenReturn(null);
        when(mockOptions.getExtraparam7()).thenReturn(null);
        when(mockOptions.getExtraparam8()).thenReturn(null);
        when(mockOptions.getExtraparam9()).thenReturn(null);
        when(mockOptions.getExtraparam10()).thenReturn(null);

        assertNull(p.getExtraparam1());
        assertNull(p.getExtraparam2());
        assertNull(p.getExtraparam3());
        assertNull(p.getExtraparam4());
        assertNull(p.getExtraparam5());
        assertNull(p.getExtraparam6());
        assertNull(p.getExtraparam7());
        assertNull(p.getExtraparam8());
        assertNull(p.getExtraparam9());
        assertNull(p.getExtraparam10());

    }

    @Test
    public void testAdPlayerVersion() {
        when(mockAdAdapter.getPlayerVersion()).thenReturn(null);
        assertEquals("", p.getAdPlayerVersion());

        when(mockAdAdapter.getPlayerVersion()).thenReturn("player-version");
        assertEquals("player-version", p.getAdPlayerVersion());

        p.removeAdsAdapter();
        assertEquals("", p.getAdPlayerVersion());
    }

    @Test
    public void testAdPosition() {

        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.PRE);
        assertEquals("pre", p.getAdPosition());

        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.POST);
        assertEquals("post", p.getAdPosition());

        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.MID);
        assertEquals("mid", p.getAdPosition());

        // If ad position is unknown, the plugin will try to infer the position depending on
        // the Buffered status of the adapter. This is a workaround and postrolls will be detected
        // as midrolls
        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.UNKNOWN);

        PlaybackFlags flags = new PlaybackFlags();
        flags.setJoined(false);
        when(mockAdapter.getFlags()).thenReturn(flags);

        assertEquals("pre", p.getAdPosition());

        flags.setJoined(true);

        assertEquals("mid", p.getAdPosition());

        // No ads adapter, this should be the same as it returning UNKNOWN, "mid" expected again
        p.removeAdsAdapter();
        assertEquals("mid", p.getAdPosition());

        // No adapter at all
        p.removeAdapter();
        assertEquals("unknown", p.getAdPosition());
    }

    @Test
    public void testAdPlayhead() {

        // Valid values
        when(mockAdAdapter.getPlayhead()).thenReturn(-10.0);
        assertEquals(Double.valueOf(-10.0), p.getAdPlayhead());

        when(mockAdAdapter.getPlayhead()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getAdPlayhead());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdAdapter.getPlayhead()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getAdPlayhead());
        }
    }

    @Test
    public void testAdDuration() {

        // Valid values
        when(mockAdAdapter.getDuration()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getAdDuration());

        when(mockAdAdapter.getDuration()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getAdDuration());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdAdapter.getDuration()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getAdDuration());
        }
    }

    @Test
    public void testAdBitrate() {

        // Valid values
        when(mockAdAdapter.getBitrate()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getAdBitrate());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdAdapter.getBitrate()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getAdBitrate());
        }
    }

    @Test
    public void testAdTitle() {

        when(mockAdAdapter.getTitle()).thenReturn("batman");
        assertEquals("batman", p.getAdTitle());

        when(mockAdAdapter.getTitle()).thenReturn("");
        assertEquals("", p.getAdTitle());

        when(mockAdAdapter.getTitle()).thenReturn(null);
        assertNull(p.getAdTitle());
    }

    @Test
    public void testAdGetResource() {

        when(mockAdAdapter.getResource()).thenReturn("AdResourceFromAdapter");

        assertEquals("AdResourceFromAdapter", p.getAdResource());

        when(mockAdAdapter.getResource()).thenReturn(null);

        assertEquals(null, p.getAdResource());
    }

    @Test
    public void testAdPluginVersion() {
        when(mockAdAdapter.getVersion()).thenReturn(null);

        assertNull(p.getAdAdapterVersion());

        when(mockAdAdapter.getVersion()).thenReturn("6.0.0-CustomAdapter");

        assertEquals("6.0.0-CustomAdapter", p.getAdAdapterVersion());

        p.removeAdsAdapter();

        assertEquals(null, p.getAdAdapterVersion());
    }

    @Test
    public void testIp() {
        assertNull(p.getIp());
        when(mockOptions.getNetworkIP()).thenReturn("1.2.3.4");
        assertEquals("1.2.3.4", p.getIp());
    }

    @Test
    public void testIsp() {
        assertNull(p.getIsp());
        when(mockOptions.getNetworkIsp()).thenReturn("ISP");
        assertEquals("ISP", p.getIsp());
    }

    @Test
    public void testConnectionType() {
        assertNull(p.getConnectionType());
        when(mockOptions.getNetworkConnectionType()).thenReturn("DSL");
        assertEquals("DSL", p.getConnectionType());
    }

    @Test
    public void testDeviceCode() {
        assertNull(p.getDeviceCode());
        when(mockOptions.getDeviceCode()).thenReturn("42");
        assertEquals("42", p.getDeviceCode());
    }

    @Test
    public void testAccountCode() {
        assertNull(p.getAccountCode());
        when(mockOptions.getAccountCode()).thenReturn("accountcode");
        assertEquals("accountcode", p.getAccountCode());
    }

    @Test
    public void testUsername() {
        assertNull(p.getUsername());
        when(mockOptions.getUsername()).thenReturn("username");
        assertEquals("username", p.getUsername());
    }

    @Test
    public void testNodeHost() {
        assertNull(p.getNodeHost());
        when(mockResourceTransform.getNodeHost()).thenReturn("nodeHost");
        assertEquals("nodeHost", p.getNodeHost());
    }

    @Test
    public void testNodeType() {
        assertNull(p.getNodeType());
        when(mockResourceTransform.getNodeType()).thenReturn("type");
        assertEquals("type", p.getNodeType());
    }

    @Test
    public void testNodeTypeString() {
        assertNull(p.getNodeTypeString());
        when(mockResourceTransform.getNodeTypeString()).thenReturn("typeString");
        assertEquals("typeString", p.getNodeTypeString());
    }

    @Test
    public void testChronoTimes() {
        // Init and preload chronos, don't depend on the adapter
        when(mockChrono.getDeltaTime(anyBoolean())).thenReturn(100L);
        assertEquals(100, p.getInitDuration());

        when(mockChrono.getDeltaTime(anyBoolean())).thenReturn(200L);
        assertEquals(200,p.getPreloadDuration());

        // Adapter chronos
        PlaybackChronos c = new PlaybackChronos();
        c.buffer = mock(Chrono.class);
        c.join = mock(Chrono.class);
        c.seek = mock(Chrono.class);
        c.pause = mock(Chrono.class);
        c.total = mock(Chrono.class);

        when(c.buffer.getDeltaTime(anyBoolean())).thenReturn(100L);
        when(c.join.getDeltaTime(anyBoolean())).thenReturn(200L);
        when(c.seek.getDeltaTime(anyBoolean())).thenReturn(300L);
        when(c.pause.getDeltaTime(anyBoolean())).thenReturn(400L);
        when(c.total.getDeltaTime(anyBoolean())).thenReturn(500L);

        when(mockAdapter.getChronos()).thenReturn(c);

        assertEquals(100, p.getBufferDuration());
        assertEquals(200, p.getJoinDuration());
        assertEquals(300, p.getSeekDuration());
        assertEquals(400, p.getPauseDuration());

        // Change values to test the ads
        when(c.buffer.getDeltaTime(anyBoolean())).thenReturn(1000L);
        when(c.join.getDeltaTime(anyBoolean())).thenReturn(2000L);
        when(c.seek.getDeltaTime(anyBoolean())).thenReturn(3000L);
        when(c.pause.getDeltaTime(anyBoolean())).thenReturn(4000L);
        when(c.total.getDeltaTime(anyBoolean())).thenReturn(5000L);

        when(mockAdAdapter.getChronos()).thenReturn(c);

        assertEquals(1000, p.getAdBufferDuration());
        assertEquals(2000, p.getAdJoinDuration());
        assertEquals(4000, p.getAdPauseDuration());
        assertEquals(5000, p.getAdTotalDuration());

        // No adapters
        p.removeAdsAdapter();

        assertEquals(-1, p.getAdBufferDuration());
        assertEquals(-1, p.getAdJoinDuration());
        assertEquals(-1, p.getAdPauseDuration());
        assertEquals(-1, p.getAdTotalDuration());

        p.removeAdapter();

        assertEquals(-1, p.getBufferDuration());
        assertEquals(-1, p.getJoinDuration());
        assertEquals(-1, p.getSeekDuration());
        assertEquals(-1, p.getPauseDuration());
    }

    @Test
    public void testRequestBuilderInstance() {
        assertEquals(mockRequestBuilder, p.getRequestBuilder());
    }

    // Will send listeners
    @Test
    public void willSendListeners() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        // Make build params return the first argument
        when(mockRequestBuilder.buildParams(anyMap(), anyString())).thenAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArguments()[0];
            }
        });

        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("key", "value");
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        // Init
        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        p.fireInit(paramsMap);
        p.removeOnWillSendInitListener(listener);
        p.fireInit(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Start
        reset(listener);
        p.addOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Join
        reset(listener);
        p.addOnWillSendJoinListener(listener);
        adapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendJoinListener(listener);
        adapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_JOIN), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Pause
        reset(listener);
        p.addOnWillSendPauseListener(listener);
        adapterEventListener.onPause(paramsMap);
        p.removeOnWillSendPauseListener(listener);
        adapterEventListener.onPause(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_PAUSE), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Resume
        reset(listener);
        p.addOnWillSendResumeListener(listener);
        adapterEventListener.onResume(paramsMap);
        p.removeOnWillSendResumeListener(listener);
        adapterEventListener.onResume(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_RESUME), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Buffer
        reset(listener);
        p.addOnWillSendBufferListener(listener);
        adapterEventListener.onBufferEnd(paramsMap);
        p.removeOnWillSendBufferListener(listener);
        adapterEventListener.onBufferEnd(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_BUFFER), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Seek
        reset(listener);
        p.addOnWillSendSeekListener(listener);
        adapterEventListener.onSeekEnd(paramsMap);
        p.removeOnWillSendSeekListener(listener);
        adapterEventListener.onSeekEnd(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_SEEK), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Error
        reset(listener);
        p.addOnWillSendErrorListener(listener);
        adapterEventListener.onError(paramsMap);
        p.removeOnWillSendErrorListener(listener);
        adapterEventListener.onError(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_ERROR), eq(p), mapCaptor.capture());

        // Stop
        reset(listener);
        p.addOnWillSendStopListener(listener);
        adapterEventListener.onStop(paramsMap);
        p.removeOnWillSendStopListener(listener);
        adapterEventListener.onStop(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_STOP), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Ad Start
        reset(listener);
        p.addOnWillSendAdStartListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendAdStartListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Ad Join
        reset(listener);
        p.addOnWillSendAdJoinListener(listener);
        adAdapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendAdJoinListener(listener);
        adAdapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_JOIN), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Ad Pause
        reset(listener);
        p.addOnWillSendAdPauseListener(listener);
        adAdapterEventListener.onPause(paramsMap);
        p.removeOnWillSendAdPauseListener(listener);
        adAdapterEventListener.onPause(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_PAUSE), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Ad Resume
        reset(listener);
        p.addOnWillSendAdResumeListener(listener);
        adAdapterEventListener.onResume(paramsMap);
        p.removeOnWillSendAdResumeListener(listener);
        adAdapterEventListener.onResume(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_RESUME), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));

        // Ad Buffer
        reset(listener);
        p.addOnWillSendAdBufferListener(listener);
        adAdapterEventListener.onBufferEnd(paramsMap);
        p.removeOnWillSendAdBufferListener(listener);
        adAdapterEventListener.onBufferEnd(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_BUFFER), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));


        // Ad Stop
        reset(listener);
        p.addOnWillSendAdStopListener(listener);
        adAdapterEventListener.onStop(paramsMap);
        p.removeOnWillSendAdStopListener(listener);
        adAdapterEventListener.onStop(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_STOP), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    // Test Youbora methods
    @Test
    public void testInit() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);
        p.fireInit(null);
        verify(p).createRequest(anyString(), eq(Constants.SERVICE_INIT));
    }
}