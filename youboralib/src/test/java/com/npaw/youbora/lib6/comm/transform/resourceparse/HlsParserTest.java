package com.npaw.youbora.lib6.comm.transform.resourceparse;

import com.npaw.youbora.lib6.comm.Request;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.net.HttpURLConnection;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class HlsParserTest {

    private final String topLevelManifest = "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8";
    private final String expectedFinalResource = "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/0640/06400.ts";
    @Test
    public void testInitialValue() {
        HlsParser parser = new HlsParser();
        assertNull(parser.getResource());
    }

    @Test
    public void testFlow() {


        HlsParser parser = spy(new HlsParser());

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        // Start parsing
        parser.parse(topLevelManifest, null);

        // Capture request callback
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(captor.capture());

        String response = "#EXTM3U\n" +
                "#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=688301\n" +
                "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/0640_vod.m3u8\n" +
                "#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=165135\n" +
                "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/0150_vod.m3u8\n";

        // Invoke callback
        captor.getValue().onRequestSuccess(mock(HttpURLConnection.class), response);

        // Recursive call has been performed, capture again
        ArgumentCaptor<Request.RequestSuccessListener> captor2 = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(captor2.capture());

        response = "#EXTM3U\n" +
                "#EXT-X-TARGETDURATION:10\n" +
                "#EXT-X-MEDIA-SEQUENCE:0\n" +
                "#EXTINF:10,\n" +
                "0640/06400.ts\n" +
                "#EXTINF:10,\n" +
                "0640/06401.ts\n";

        // Invoke callback
        captor.getValue().onRequestSuccess(mock(HttpURLConnection.class), response);

        // Finished
        assertEquals(expectedFinalResource, parser.getResource());
    }

    @Test
    public void testListeners() {
        HlsParser parser = new HlsParser();

        HlsParser.HlsTransformListener listener = new HlsParser.HlsTransformListener() {
            @Override
            public void onHlsTransformDone(String parsedResource) {

            }
        };

        // Add listener
        parser.addHlsTransformListener(listener);
        assertFalse(parser.removeHlsTransformListener(null));
        assertTrue(parser.removeHlsTransformListener(listener));
        parser.addHlsTransformListener(listener);

    }

    @Test
    public void testListenerSuccess() {

        final boolean[] callbackInvoked = {false};

        HlsParser parser = spy(new HlsParser());

        HlsParser.HlsTransformListener listener = new HlsParser.HlsTransformListener() {
            @Override
            public void onHlsTransformDone(String parsedResource) {
                callbackInvoked[0] = true;
            }
        };

        // Add listener
        parser.addHlsTransformListener(listener);

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        // Start parsing with final resource (doesn't need parsing)
        parser.parse(expectedFinalResource, null);

        assertTrue(callbackInvoked[0]);
        assertEquals(expectedFinalResource, parser.getResource());

    }

    @Test
    public void testNonHlsUrl() {
        final boolean[] callbackInvoked = {false};

        HlsParser parser = spy(new HlsParser());

        HlsParser.HlsTransformListener listener = new HlsParser.HlsTransformListener() {
            @Override
            public void onHlsTransformDone(String parsedResource) {
                callbackInvoked[0] = true;
            }
        };

        // Add listener
        parser.addHlsTransformListener(listener);

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        // Start parsing with invalid hls url
        String mp4url = "http://www.example.com/path/resource.mp4";
        parser.parse(mp4url, null);

        assertTrue(callbackInvoked[0]);

        assertEquals(mp4url, parser.getResource());
    }

    @Test
    public void testInvalidResource() {
        final boolean[] callbackInvoked = {false};

        HlsParser parser = spy(new HlsParser());

        HlsParser.HlsTransformListener listener = new HlsParser.HlsTransformListener() {
            @Override
            public void onHlsTransformDone(String parsedResource) {
                callbackInvoked[0] = true;
            }
        };

        // Add listener
        parser.addHlsTransformListener(listener);

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        // Start parsing with invalid hls url
        String mp4url = "http://www.example.com/path/resourcewithnoextension";
        parser.parse(mp4url, null);

        assertTrue(callbackInvoked[0]);

        assertNull(parser.getResource());
    }

    @Test
    public void testErrorListener() {
        final boolean[] callbackInvoked = {false};

        HlsParser parser = spy(new HlsParser());

        HlsParser.HlsTransformListener listener = new HlsParser.HlsTransformListener() {
            @Override
            public void onHlsTransformDone(String parsedResource) {
                callbackInvoked[0] = true;
            }
        };

        // Add listener
        parser.addHlsTransformListener(listener);

        // Inject mock Request
        Request mockRequest = mock(Request.class);
        doReturn(mockRequest).when(parser).createRequest(anyString(), anyString());

        // Start parsing with invalid hls url
        parser.parse(topLevelManifest, null);

        // Capture request error callback
        ArgumentCaptor<Request.RequestErrorListener> captor = ArgumentCaptor.forClass(Request.RequestErrorListener.class);
        verify(mockRequest, atLeastOnce()).addOnErrorListener(captor.capture());

        // Mock callback
        captor.getValue().onRequestError(mock(HttpURLConnection.class));

        assertTrue(callbackInvoked[0]);

        assertNull(parser.getResource());
    }

}
