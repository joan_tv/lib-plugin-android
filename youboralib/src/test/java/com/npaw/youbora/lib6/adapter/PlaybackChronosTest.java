package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.Chrono;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlaybackChronosTest {

    @Test
    public void testNonNull() {
        PlaybackChronos chronos = new PlaybackChronos();

        assertNotNull(chronos.buffer);
        assertNotNull(chronos.seek);
        assertNotNull(chronos.join);
        assertNotNull(chronos.pause);
        assertNotNull(chronos.total);
    }

    @Test
    public void testReset() {
        PlaybackChronos chronos = new PlaybackChronos();

        Chrono chronobuffer = chronos.buffer;
        Chrono chronoseek = chronos.seek;
        Chrono chronojoin = chronos.join;
        Chrono chronopause = chronos.pause;
        Chrono chronototal = chronos.total;

        chronos.reset();

        assertNotEquals(chronobuffer, chronos.buffer);
        assertNotEquals(chronoseek, chronos.seek);
        assertNotEquals(chronojoin, chronos.join);
        assertNotEquals(chronopause, chronos.pause);
        assertNotEquals(chronototal, chronos.total);
    }

}