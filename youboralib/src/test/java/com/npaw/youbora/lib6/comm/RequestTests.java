package com.npaw.youbora.lib6.comm;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RequestTests {

    @Test
    public void testRequestFields() {
        Request r = new Request("example.com", "/service");

        // Check default values
        assertEquals(r.getHost(), "example.com");
        assertEquals(r.getService(), "/service");
        assertEquals(r.getMethod(), Request.METHOD_GET);
        assertEquals(r.getParams(), null);
        assertEquals(r.getParam("a"), null);
        assertEquals(r.getRequestHeaders(), null);

        // Change properties
        r.setHost("abc.com");
        r.setService("/anotherService");
        r.setMethod(Request.METHOD_DELETE);
        r.setMaxRetries(10);
        r.setRetryInterval(10000);
        Map<String, Object> params = new HashMap<>();
        params.put("param1", "value1");
        params.put("param2", "value2");
        params.put("param3", "value3");
        r.setParams(params);
        r.setParam("param4", "value4");

        Map<String, String> reqHeaders = new HashMap<>();
        reqHeaders.put("header1", "valueheader1");
        reqHeaders.put("header2", "valueheader2");
        r.setRequestHeaders(reqHeaders);

        // Check new values
        assertEquals(r.getHost(), "abc.com");
        assertEquals(r.getService(), "/anotherService");
        assertEquals(r.getMethod(), Request.METHOD_DELETE);
        assertEquals(r.getMaxRetries(), 10);
        assertEquals(r.getRetryInterval(), 10000);

        params = r.getParams();

        assertEquals(params.size(), 4);
        assertEquals(params.get("param1"), "value1");
        assertEquals(params.get("param2"), "value2");
        assertEquals(params.get("param3"), "value3");
        assertEquals(params.get("param4"), "value4");

        reqHeaders = r.getRequestHeaders();
        assertEquals(reqHeaders.get("header1"), "valueheader1");
        assertEquals(reqHeaders.get("header2"), "valueheader2");

        assertNull(r.getParam("unexisting_key"));
    }
}
