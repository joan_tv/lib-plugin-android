package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.comm.Request;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

public class TransformTest {

    private CustomTransform t;

    @Before
    public void setUp() throws Exception {
        t = new CustomTransform();
    }

    @After
    public void tearDown() throws Exception {
        t = null;
    }

    @Test
    public void testBusyInitial() {
        assertTrue(t.isBlocking(mock(Request.class)));
        assertTrue(t.isBlocking(null));
    }

    @Test
    public void testBusyAfterDone() {
        t.callDone();
        assertFalse(t.isBlocking(mock(Request.class)));
        assertFalse(t.isBlocking(null));
    }

    @Test
    public void testListenerCalled() throws InterruptedException {

        final Boolean[] callbackCalled = {false};

        t.addTransformDoneListener(new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {
                callbackCalled[0] = true;
            }
        });

        t.callDone();

        assertTrue(callbackCalled[0]);
    }

    @Test
    public void testListenerNotCalledAfterRemove() throws InterruptedException {

        final Boolean[] callbackCalled = {false};

        Transform.TransformDoneListener listener = new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {
                callbackCalled[0] = true;
            }
        };

        t.addTransformDoneListener(listener);
        t.removeTransformDoneListener(listener);

        t.callDone();

        assertFalse(callbackCalled[0]);
    }

    @Test
    public void testListenerRemove() {
        Transform.TransformDoneListener listener = new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {

            }
        };

        t.addTransformDoneListener(listener);

        assertFalse(t.removeTransformDoneListener(null));
        assertFalse(t.removeTransformDoneListener(new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {

            }
        }));

        assertTrue(t.removeTransformDoneListener(listener));

    }

    private class CustomTransform extends Transform {
        @Override
        public void parse(Request request) {

        }

        public void callDone() {
            done();
        }
    }
}