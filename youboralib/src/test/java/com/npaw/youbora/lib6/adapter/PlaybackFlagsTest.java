package com.npaw.youbora.lib6.adapter;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlaybackFlagsTest {

    @Test
    public void testDefaultValues() {
        PlaybackFlags flags = new PlaybackFlags();

        assertFalse(flags.isBuffering());
        assertFalse(flags.isJoined());
        assertFalse(flags.isPaused());
        assertFalse(flags.isPreloading());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isStarted());
    }

    @Test
    public void testSetValues() {
        PlaybackFlags flags = new PlaybackFlags();

        flags.setBuffering(true);
        flags.setJoined(true);
        flags.setPaused(true);
        flags.setPreloading(true);
        flags.setSeeking(true);
        flags.setStarted(true);

        assertTrue(flags.isBuffering());
        assertTrue(flags.isJoined());
        assertTrue(flags.isPaused());
        assertTrue(flags.isPreloading());
        assertTrue(flags.isSeeking());
        assertTrue(flags.isStarted());

    }

    @Test
    public void testReset() {
        PlaybackFlags flags = new PlaybackFlags();

        flags.setBuffering(true);
        flags.setJoined(true);
        flags.setPaused(true);
        flags.setPreloading(true);
        flags.setSeeking(true);
        flags.setStarted(true);

        flags.reset();

        assertFalse(flags.isBuffering());
        assertFalse(flags.isJoined());
        assertFalse(flags.isPaused());
        assertFalse(flags.isPreloading());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isStarted());

    }

}