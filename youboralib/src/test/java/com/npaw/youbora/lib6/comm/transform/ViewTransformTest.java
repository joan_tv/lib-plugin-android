package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.plugin.RequestBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.net.HttpURLConnection;
import java.util.HashMap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ViewTransformTest {

    private static final String CODE = "U_12345_1s7yd9x92gypdrt5";
    private static final String HOST = "debug-nqs-lw2.nice264.com";
    private static final String PING_TIME = "5";
    private Request mockRequest;
    private JSONObject mockOuterJson;
    private Plugin mockPlugin;
    private YouboraLog.YouboraLogger mockLogger;

    private class TestViewTransform extends ViewTransform {

        public TestViewTransform(Plugin plugin) {
            super(plugin);
        }

        @Override
        JSONObject createJSONFromString(String string) throws JSONException {
            return mockOuterJson;
        }

        @Override
        Request createRequest(String host, String service) {
            return mockRequest;
        }
    }

    @Before
    public void setUp() throws Exception {

        // Log listener
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        // Mocks
        mockRequest = mock(Request.class);
        mockOuterJson = mock(JSONObject.class);

        mockPlugin = mock(Plugin.class);
        when(mockPlugin.getHost()).thenReturn("host.com");
        RequestBuilder mockBuilder = mock(RequestBuilder.class);
        when(mockBuilder.buildParams(any(HashMap.class), anyString())).thenReturn(new HashMap());
        when(mockPlugin.getRequestBuilder()).thenReturn(mockBuilder);

        // Mock JSON methods
        JSONObject mockInnerJson = mock(JSONObject.class);

        when(mockOuterJson.has("q")).thenReturn(true);
        when(mockOuterJson.getJSONObject("q")).thenReturn(mockInnerJson);

        when(mockInnerJson.has("h")).thenReturn(true);
        when(mockInnerJson.getString("h")).thenReturn(HOST);
        when(mockInnerJson.has("c")).thenReturn(true);
        when(mockInnerJson.getString("c")).thenReturn(CODE);
        when(mockInnerJson.has("pt")).thenReturn(true);
        when(mockInnerJson.getString("pt")).thenReturn(PING_TIME);
    }

    @Test
    public void testGetCodeWithoutInit() {
        ViewTransform viewTransform = new ViewTransform(mockPlugin);

        assertNull(viewTransform.nextView());
        assertNull(viewTransform.nextView());
    }

    private void verifyErrorCalled(String fastDataResponse) {
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Capture callback when set
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);

        viewTransform.init(); // Callback set here

        // Capture callback
        verify(mockRequest).addOnSuccessListener(captor.capture());

        // Mock callback response
        captor.getValue().onRequestSuccess(mock(HttpURLConnection.class), fastDataResponse);

        verify(mockLogger, times(1)).logYouboraMessage(anyString(), eq(YouboraLog.Level.ERROR));
    }

    @Test
    public void testEmptyResponse() {
        verifyErrorCalled("");
    }

    @Test
    public void testNullResponse() {
        verifyErrorCalled(null);
    }

    @Test
    public void testErrorRequest() {
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Capture callback when set
        ArgumentCaptor<Request.RequestErrorListener> captor = ArgumentCaptor.forClass(Request.RequestErrorListener.class);

        viewTransform.init(); // Callback set here

        // Capture callback
        verify(mockRequest).addOnErrorListener(captor.capture());

        // Mock callback response
        captor.getValue().onRequestError(mock(HttpURLConnection.class));

        verify(mockLogger, times(1)).logYouboraMessage(anyString(), eq(YouboraLog.Level.ERROR));
    }

    @Test
    public void testRequestData() throws Exception {

        // ViewTransform to test
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Capture callback when set
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);

        viewTransform.init(); // Callback set here

        // Capture callback
        verify(mockRequest).addOnSuccessListener(captor.capture());

        // Mock callback response
        captor.getValue().onRequestSuccess(mock(HttpURLConnection.class), "response");

        // Shouldn't block anymore (Due to offline may be blocked anyway) TODO Take a better look to this
        //assertFalse(viewTransform.isBlocking(null));

        //ViewTransform.FastDataConfig config = viewTransform.fastDataConfig;

        // Assert config params
        //assertNotNull(config);
        //assertEquals((Integer) Integer.parseInt(PING_TIME), config.pingTime);
        //assertEquals(CODE, config.code);
        //assertEquals("http://" + HOST, config.host);
    }

    @Test
    public void testParseRequests() {
        // ViewTransform to test
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Fill FastData responses
        viewTransform.fastDataConfig = new TestViewTransform.FastDataConfig();
        viewTransform.fastDataConfig.host = "http://" + HOST;
        viewTransform.fastDataConfig.pingTime = Integer.parseInt(PING_TIME);
        viewTransform.fastDataConfig.code = CODE;

        // create first view code
        viewTransform.nextView();

        // Mock /start
        Request mockStart = mock(Request.class);
        HashMap<String, Object> mockMap = new HashMap<>();
        when(mockStart.getParams()).thenReturn(mockMap);
        when(mockStart.getService()).thenReturn(Constants.SERVICE_START);

        // Parse requests
        viewTransform.parse(mockStart);

        assertEquals(CODE + "_0", mockMap.get("code"));
        assertEquals(PING_TIME, mockMap.get("pingTime").toString());
        verify(mockStart, times(1)).setHost("http://" + HOST);

        // increment view code
        viewTransform.nextView();

        // Mock /ping
        Request mockPing = mock(Request.class);
        mockMap = new HashMap<>();
        when(mockPing.getParams()).thenReturn(mockMap);
        when(mockPing.getService()).thenReturn(Constants.SERVICE_START);

        viewTransform.parse(mockPing);

        assertEquals(CODE + "_1", mockMap.get("code"));
        assertEquals(PING_TIME, mockMap.get("pingTime").toString());
        verify(mockStart, times(1)).setHost("http://" + HOST);
    }

}