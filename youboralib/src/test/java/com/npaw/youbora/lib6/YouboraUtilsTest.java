package com.npaw.youbora.lib6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class YouboraUtilsTest {

    @Test
    public void testStropProtocol() {
        assertNull(YouboraUtil.stripProtocol(null));
        assertEquals(YouboraUtil.stripProtocol(""), "");
        assertEquals(YouboraUtil.stripProtocol("http://google.com"), "google.com");
        assertEquals(YouboraUtil.stripProtocol("https://google.com"), "google.com");
        assertEquals(YouboraUtil.stripProtocol("//google.com"), "google.com");
    }

    @Test
    public void testAddProtocol() {
        assertEquals(YouboraUtil.addProtocol(null, true), "https://");
        assertEquals(YouboraUtil.addProtocol("google.com", true), "https://google.com");
        assertEquals(YouboraUtil.addProtocol("google.com", false), "http://google.com");
    }

    @Test
    public void testParseNumber() {

        // Double tests
        double ddefault = 100;

        assertTrue(YouboraUtil.parseNumber(1.0, ddefault) == 1.0);
        assertTrue(YouboraUtil.parseNumber(1.0, null) == 1.0);
        assertTrue(YouboraUtil.parseNumber(null, ddefault) == ddefault);
        assertTrue(YouboraUtil.parseNumber(Double.POSITIVE_INFINITY, ddefault) == ddefault);
        assertTrue(YouboraUtil.parseNumber(Double.NEGATIVE_INFINITY, ddefault) == ddefault);
        assertTrue(YouboraUtil.parseNumber(Double.NaN, ddefault) == ddefault);
        assertTrue(YouboraUtil.parseNumber(Double.MAX_VALUE, ddefault) == ddefault);

        // Integer tests
        int idefault = 200;

        assertTrue(YouboraUtil.parseNumber(1, idefault) == 1);
        assertTrue(YouboraUtil.parseNumber(1, null) == 1);
        assertTrue(YouboraUtil.parseNumber(null, idefault) == idefault);
        assertTrue(YouboraUtil.parseNumber(Integer.MAX_VALUE, idefault) == idefault);
        assertTrue(YouboraUtil.parseNumber(Integer.MIN_VALUE, idefault) == idefault);

        // Long tests
        long ldefault = 300;

        assertTrue(YouboraUtil.parseNumber(1L, ldefault) == 1);
        assertTrue(YouboraUtil.parseNumber(1L, null) == 1);
        assertTrue(YouboraUtil.parseNumber(null, ldefault) == ldefault);
        assertTrue(YouboraUtil.parseNumber(Long.MAX_VALUE, ldefault) == ldefault);
        assertTrue(YouboraUtil.parseNumber(Long.MIN_VALUE, ldefault) == ldefault);

    }

    @Test
    public void testRenditionStrings() {
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 4567452.9817), "1920x1080@4.57Mbps");
        assertEquals(YouboraUtil.buildRenditionString(0, 1080, 4567452.9817), "4.57Mbps");
        assertEquals(YouboraUtil.buildRenditionString(1920, 0, 4567452.9817), "4.57Mbps");
        assertEquals(YouboraUtil.buildRenditionString(0, 0, 4567452.9817), "4.57Mbps");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 456745.9817), "1920x1080@457Kbps");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 45674.9817), "1920x1080@46Kbps");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 4567.9817), "1920x1080@5Kbps");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 456.9817), "1920x1080@457bps");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 0), "1920x1080");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, -123), "1920x1080");
        assertEquals(YouboraUtil.buildRenditionString(1920, 1080, 1), "1920x1080@1bps");
    }

}
