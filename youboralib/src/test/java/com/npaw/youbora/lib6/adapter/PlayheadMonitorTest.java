package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Timer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class PlayheadMonitorTest {

    private PlayerAdapter adapter;
    private Chrono mockChrono;
    private Timer mockTimer;
    private PlaybackFlags mockFlags;

    private PlayheadMonitor monitor;
    private final int MONITORING_INTERVAL = 800;

    // Captured params when setting the timer
    private Timer.TimerEventListener timerListener;
    private long timerInterval;

    private class TestPlayheadMonitor extends PlayheadMonitor {

        public TestPlayheadMonitor(PlayerAdapter adapter, int type, int interval) {
            super(adapter, type, interval);
        }

        @Override
        Chrono createChrono() {
            return mockChrono;
        }

        @Override
        Timer createTimer(Timer.TimerEventListener listener, long interval) {
            timerListener = listener;
            timerInterval = interval;
            return mockTimer;
        }
    }

    @Before
    public void setUp() {
        adapter = mock(PlayerAdapter.class);
        mockChrono = mock(Chrono.class);
        mockTimer = mock(Timer.class);
        mockFlags = mock(PlaybackFlags.class);
    }

    @Test
    public void testIntervalAndStartStop() throws Exception {

        PlayheadMonitor monitor = new TestPlayheadMonitor(adapter, PlayheadMonitor.TYPE_BUFFER, MONITORING_INTERVAL);

        // Mock flags
        PlaybackFlags mockFlags = mock(PlaybackFlags.class);
        when(adapter.getFlags()).thenReturn(mockFlags);

        long interval = timerInterval;

        assertEquals(MONITORING_INTERVAL, interval);

        monitor.start();
        verify(mockTimer, times(1)).start();

        monitor.stop();
        verify(mockTimer, times(1)).stop();
    }

    @Test
    public void testHealthy() throws Exception {

        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        for (int i = 1; i <= 10; i++) {
            when(adapter.getPlayhead()).thenReturn(i*MONITORING_INTERVAL/1000.0);
            timerListener.onTimerEvent(MONITORING_INTERVAL);
        }

        verifyNotBuffering();
        verifyNotSeeking();
    }

    @Test
    public void testBuffer() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead won't progress
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireBufferBegin(anyBoolean());
        when(mockFlags.isBuffering()).thenReturn(true);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(2 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireBufferEnd();

        verifyNotSeeking();
    }

    @Test
    public void testSeek() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead jump
        when(adapter.getPlayhead()).thenReturn(10 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireSeekBegin(anyBoolean());
        when(mockFlags.isSeeking()).thenReturn(true);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(11 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireSeekEnd();

        verifyNotBuffering();
    }

    @Test
    public void testBufferNotFiredWhenMonitoringSeek() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead won't progress
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(2 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verifyNotBuffering();
        verifyNotSeeking();
    }

    @Test
    public void testSeekNotFiredWhenMonitoringBuffer() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead jump
        when(adapter.getPlayhead()).thenReturn(10 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(11 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verifyNotBuffering();
        verifyNotSeeking();
    }

    @Test
    public void testBufferThenSeek() throws Exception {
        // Here we test buffer -> seek conversion
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead won't progress
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireBufferBegin(anyBoolean());
        when(mockFlags.isBuffering()).thenReturn(true);

        // Playhead jump
        when(adapter.getPlayhead()).thenReturn(10 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);
        verify(adapter, times(1)).fireSeekBegin(anyBoolean());
        when(mockFlags.isBuffering()).thenReturn(false);
        when(mockFlags.isSeeking()).thenReturn(true);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(11 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(0)).fireBufferEnd();
        verify(adapter, times(1)).fireSeekEnd();

    }

    @Test
    public void testSkipNextTick() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // This will prevent buffer from firing the next tick
        monitor.skipNextTick();

        // Playhead progresses a tiny bit, enough to trigger buffer in normal cases
        when(adapter.getPlayhead()).thenReturn(1.1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verifyNotBuffering();
        verifyNotSeeking();
    }

    private void prepareMocks(int monitoringType) throws Exception {

        monitor = new TestPlayheadMonitor(adapter, monitoringType, MONITORING_INTERVAL);

        // Mock flags
        when(adapter.getFlags()).thenReturn(mockFlags);

    }

    private void verifyNotBuffering() {
        verify(adapter, times(0)).fireBufferBegin();
        verify(adapter, times(0)).fireBufferBegin(anyBoolean());
        verify(adapter, times(0)).fireBufferBegin(anyMap(), anyBoolean());
    }

    private void verifyNotSeeking() {
        verify(adapter, times(0)).fireSeekBegin();
        verify(adapter, times(0)).fireSeekBegin(anyBoolean());
        verify(adapter, times(0)).fireSeekBegin(anyMap(), anyBoolean());
    }

}