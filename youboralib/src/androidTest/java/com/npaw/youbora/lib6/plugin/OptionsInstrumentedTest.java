package com.npaw.youbora.lib6.plugin;

import android.os.Bundle;

import org.junit.Test;

import java.util.ArrayList;

import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_AKAMAI;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_CLOUDFRONT;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_FASTLY;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_HIGHWINDS;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_LEVEL3;
import static org.junit.Assert.*;

public class OptionsInstrumentedTest {

    @Test
    public void testToBundleAndBackDefaultValues () {
        Options o = new Options();
        assertDefaults(new Options(o.toBundle()));
    }

    @Test
    public void testToBundleAndBackCustomValues() throws Exception {
        Options o = new Options();
        setValues(o);
        assertValues(new Options(o.toBundle()));
    }

    private void setValues(Options o) {

        // Set values
        o.setEnabled(false);
        o.setHttpSecure(true);
        o.setHost("a");
        o.setAccountCode("b");
        o.setUsername("c");
        o.setParseHls(true);
        o.setParseCdnNameHeader("d");
        o.setParseCdnNode(true);
        ArrayList<String> cdnNodes = new ArrayList<>();
        cdnNodes.add("listitem1");
        cdnNodes.add("listitem2");
        o.setParseCdnNodeList(cdnNodes);
        o.setNetworkIP("f");
        o.setNetworkIsp("g");
        o.setNetworkConnectionType("h");
        o.setDeviceCode("i");
        o.setContentResource("j");
        o.setContentIsLive(true);
        o.setContentTitle("k");
        o.setContentTitle2("l");
        o.setContentDuration(1.0);
        o.setContentTransactionCode("m");
        o.setContentBitrate(2L);
        o.setContentThroughput(3L);
        o.setContentRendition("n");
        o.setContentCdn("o");
        o.setContentFps(4.0);
        Bundle b = new Bundle();
        b.putString("p", "q");
        o.setContentMetadata(b);
        Bundle b2 = new Bundle();
        b2.putString("r", "s");
        o.setAdMetadata(b2);
        o.setExtraparam1("t");
        o.setExtraparam2("u");
        o.setExtraparam3("v");
        o.setExtraparam4("w");
        o.setExtraparam5("x");
        o.setExtraparam6("y");
        o.setExtraparam7("z");
        o.setExtraparam8("aa");
        o.setExtraparam9("ab");
        o.setExtraparam10("ac");
    }

    private void assertDefaults(Options o) {

        // Verify
        assertEquals(true, o.isEnabled());
        assertEquals(true, o.isHttpSecure());
        assertEquals("nqs.nice264.com", o.getHost());
        assertEquals("nicetest", o.getAccountCode());
        assertNull(o.getUsername());
        assertEquals(false, o.isParseHls());
        assertEquals("x-cdn-forward", o.getParseCdnNameHeader());
        assertEquals(false, o.isParseCdnNode());
        ArrayList<String> cdnList = o.getParseCdnNodeList();
        assertEquals(CDN_NAME_AKAMAI, cdnList.get(0));
        assertEquals(CDN_NAME_CLOUDFRONT, cdnList.get(1));
        assertEquals(CDN_NAME_LEVEL3, cdnList.get(2));
        assertEquals(CDN_NAME_FASTLY, cdnList.get(3));
        assertEquals(CDN_NAME_HIGHWINDS, cdnList.get(4));
        assertNull(o.getNetworkIP());
        assertNull(o.getNetworkIsp());
        assertNull(o.getNetworkConnectionType());
        assertNull(o.getDeviceCode());
        assertNull(o.getContentResource());
        assertNull(o.getContentIsLive());
        assertNull(o.getContentTitle());
        assertNull(o.getContentTitle2());
        assertNull(o.getContentDuration());
        assertNull(o.getContentTransactionCode());
        assertNull(o.getContentBitrate());
        assertNull(o.getContentThroughput());
        assertNull(o.getContentRendition());
        assertNull(o.getContentCdn());
        assertNull(o.getContentFps());
        Bundle b = o.getContentMetadata();
        assertNotNull(b);
        assertEquals(0, b.size());
        Bundle b2 = o.getAdMetadata();
        assertNotNull(b2);
        assertEquals(0, b2.size());
        assertNull(o.getExtraparam1());
        assertNull(o.getExtraparam2());
        assertNull(o.getExtraparam3());
        assertNull(o.getExtraparam4());
        assertNull(o.getExtraparam5());
        assertNull(o.getExtraparam6());
        assertNull(o.getExtraparam7());
        assertNull(o.getExtraparam8());
        assertNull(o.getExtraparam9());
        assertNull(o.getExtraparam10());
    }

    private void assertValues(Options o) {

        // Verify
        assertEquals(false, o.isEnabled());
        assertEquals(true, o.isHttpSecure());
        assertEquals("a", o.getHost());
        assertEquals("b", o.getAccountCode());
        assertEquals("c", o.getUsername());
        assertEquals(true, o.isParseHls());
        assertEquals("d", o.getParseCdnNameHeader());
        assertEquals(true, o.isParseCdnNode());
        assertEquals("listitem1", o.getParseCdnNodeList().get(0));
        assertEquals("listitem2", o.getParseCdnNodeList().get(1));
        assertEquals("f", o.getNetworkIP());
        assertEquals("g", o.getNetworkIsp());
        assertEquals("h", o.getNetworkConnectionType());
        assertEquals("i", o.getDeviceCode());
        assertEquals("j", o.getContentResource());
        assertEquals(true, o.getContentIsLive());
        assertEquals("k", o.getContentTitle());
        assertEquals("l", o.getContentTitle2());
        assertEquals((Double) 1.0, o.getContentDuration());
        assertEquals("m", o.getContentTransactionCode());
        assertEquals((Long) 2L, o.getContentBitrate());
        assertEquals((Long) 3L, o.getContentThroughput());
        assertEquals("n", o.getContentRendition());
        assertEquals("o", o.getContentCdn());
        assertEquals((Double) 4.0, o.getContentFps());
        assertEquals("q", o.getContentMetadata().getString("p"));
        assertEquals("s", o.getAdMetadata().getString("r"));
        assertEquals("t", o.getExtraparam1());
        assertEquals("u", o.getExtraparam2());
        assertEquals("v", o.getExtraparam3());
        assertEquals("w", o.getExtraparam4());
        assertEquals("x", o.getExtraparam5());
        assertEquals("y", o.getExtraparam6());
        assertEquals("z", o.getExtraparam7());
        assertEquals("aa", o.getExtraparam8());
        assertEquals("ab", o.getExtraparam9());
        assertEquals("ac", o.getExtraparam10());
    }

}
