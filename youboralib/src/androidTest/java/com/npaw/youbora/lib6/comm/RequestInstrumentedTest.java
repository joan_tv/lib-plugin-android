package com.npaw.youbora.lib6.comm;


import android.net.Uri;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RequestInstrumentedTest {

    private static MockURLStreamHandlerFactory factory = null;

    @Test
    public void testRemoveGlobalListeners() {

        Request.RequestSuccessListener successListener = new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {
            }
        };

        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
            }
        };

        Request.addOnEverySuccessListener(successListener);
        Request.addOnEveryErrorListener(errorListener);

        assertFalse(Request.removeOnEveryErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {

            }
        }));

        assertFalse(Request.removeOnEverySuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {

            }
        }));

        assertTrue(Request.removeOnEveryErrorListener(errorListener));
        assertTrue(Request.removeOnEverySuccessListener(successListener));

        assertFalse(Request.removeOnEveryErrorListener(null));
        assertFalse(Request.removeOnEverySuccessListener(null));
    }

    @Test
    public void testGetQuery() {

        // Test
        Request r = new Request("host.com", "/service");

        assertEquals("", r.getQuery());

        r.setParam("stringKey", "stringValue");
        r.setParam("intKey", 2);
        r.setParam("doubleKey", 23.0);

        Map<String, Object> map = new HashMap<>();
        map.put("mapStringKey", "mapStringValue");
        map.put("mapIntArrayKey", new int[]{1,2,3,4});

        r.setParam("mapKey", map);

        Bundle b = new Bundle();
        b.putFloat("bfloatKey", 12);
        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("string1");
        stringArrayList.add("string2");
        stringArrayList.add("string3");
        b.putStringArrayList("arrayListKey", stringArrayList);

        r.setParam("bundleKey", b);

        String query = r.getQuery();

        String decodedString = Uri.decode(query);

        assertTrue(decodedString.contains("stringKey=stringValue"));
        assertTrue(decodedString.contains("intKey=2"));
        assertTrue(decodedString.contains("doubleKey=23"));
        assertTrue(decodedString.contains("mapKey={"));
        assertTrue(decodedString.contains("\"mapStringKey\":\"mapStringValue\""));
        assertTrue(decodedString.contains("\"mapIntArrayKey\":[1,2,3,4]"));
        assertTrue(decodedString.contains("bundleKey={"));
        assertTrue(decodedString.contains("\"bfloatKey\":12"));
        assertTrue(decodedString.contains("\"arrayListKey\":[\"string1\",\"string2\",\"string3\"]"));

        assertEquals(258, query.length());
    }

    @Test
    public void testBuildUrl() {
        Request r = new Request("http://host.com", "/service");

        r.setParam("key", "value");

        String url = r.getUrl();

        String expected = "http://host.com/service?key=value";

        assertEquals(expected, url);
    }

    @Test
    public void testSendSuccessRequest() throws Exception {

        HttpURLConnection mockHttpURLConnection = mock(HttpURLConnection.class);

        if (factory == null) {
            factory = new MockURLStreamHandlerFactory(mockHttpURLConnection);
            URL.setURLStreamHandlerFactory(factory);
        } else {
            factory.setUrlConnection(mockHttpURLConnection);
        }

        final String content = "Response content with code 200";

        when(mockHttpURLConnection.getResponseCode()).thenReturn(200);
        when(mockHttpURLConnection.getInputStream()).thenReturn(IOUtils.toInputStream(content));

        final CountDownLatch latch = new CountDownLatch(2);

        final Request r = new Request("http://host.com", "/service");

        String headerName = "headerName";
        String headerValue = "headerValue";

        Map<String, String> reqHeaders = new HashMap<>(1);
        reqHeaders.put(headerName, headerValue);
        r.setRequestHeaders(reqHeaders);

        r.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                fail("RequestErrorListener called when it shouldn't have.");
            }
        });

        r.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {
                latch.countDown();
                assertEquals(content, response);
            }
        });

        // "On every" listeners
        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                fail("RequestErrorListener called when it shouldn't have.");
            }
        };
        Request.addOnEveryErrorListener(errorListener);

        Request.RequestSuccessListener successListener = new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {
                latch.countDown();
                assertEquals(content, response);
            }
        };
        Request.addOnEverySuccessListener(successListener);

        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                r.send();
            }
        });

        latch.await(1, TimeUnit.SECONDS);
        assertEquals(0, latch.getCount());

        verify(mockHttpURLConnection, times(1)).setRequestMethod("GET");
        verify(mockHttpURLConnection, times(1)).setRequestProperty(headerName, headerValue);

        Request.removeOnEveryErrorListener(errorListener);
        Request.removeOnEverySuccessListener(successListener);
    }

    @Test
    public void testSendErrorRequest() throws Exception {

        HttpURLConnection mockHttpURLConnection = mock(HttpURLConnection.class);

        if (factory == null) {
            factory = new MockURLStreamHandlerFactory(mockHttpURLConnection);
            URL.setURLStreamHandlerFactory(factory);
        } else {
            factory.setUrlConnection(mockHttpURLConnection);
        }

        when(mockHttpURLConnection.getResponseCode()).thenReturn(500);
        when(mockHttpURLConnection.getInputStream()).thenReturn(IOUtils.toInputStream("Response content"));

        final CountDownLatch latch = new CountDownLatch(2);

        final Request r = new Request("http://host.com", "/service");

        String headerName = "headerName";
        String headerValue = "headerValue";

        Map<String, String> reqHeaders = new HashMap<>(1);
        reqHeaders.put(headerName, headerValue);
        r.setRequestHeaders(reqHeaders);

        r.setMethod(Request.METHOD_HEAD);

        r.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                latch.countDown();
            }
        });

        r.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {
                fail("RequestSuccessListener called when it shouldn't have. Response: " + response);
            }
        });

        // "On every" listeners
        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                latch.countDown();
            }
        };
        Request.addOnEveryErrorListener(errorListener);

        Request.RequestSuccessListener successListener = new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {
                fail("RequestSuccessListener called when it shouldn't have. Response: " + response);
            }
        };
        Request.addOnEverySuccessListener(successListener);

        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                r.send();
            }
        });

        latch.await(1, TimeUnit.SECONDS);
        assertEquals(0, latch.getCount());

        verify(mockHttpURLConnection, times(1)).setRequestMethod("HEAD");
        verify(mockHttpURLConnection, times(1)).setRequestProperty(headerName, headerValue);

        Request.removeOnEveryErrorListener(errorListener);
        Request.removeOnEverySuccessListener(successListener);
    }

    private class MockURLStreamHandlerFactory extends URLStreamHandler implements URLStreamHandlerFactory {

        private HttpURLConnection connection;

        public MockURLStreamHandlerFactory(HttpURLConnection connection) {
            setUrlConnection(connection);
        }

        // URLStreamHandler
        @Override
        protected URLConnection openConnection(URL u) throws IOException {
            return connection;
        }

        // URLStreamHandlerFactory
        @Override
        public URLStreamHandler createURLStreamHandler(String protocol) {
            return this;
        }

        public void setUrlConnection(HttpURLConnection mockHttpURLConnection) {
            this.connection = mockHttpURLConnection;
        }
    }
}
