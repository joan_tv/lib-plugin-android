package com.npaw.youbora.lib6;

import android.os.Bundle;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class UtilsInstrumentedTest {

    @Test
    public void testStringifyBundle() {

        assertNull(YouboraUtil.stringifyBundle(null));

        Bundle b = new Bundle();
        b.putFloat("floatKey", 1);
        b.putString("stringKey", "stringValue");
        b.putIntArray("intArrayKey", new int[]{1,2,3,4});

        Bundle b2 = new Bundle();
        b2.putDouble("doubleKey", 23);
        b2.putStringArray("stringArrayKey", new String[] {"stringArray1", "stringArray2"});
        b.putBundle("bundleKey", b2);

        HashMap<String, String> map = new HashMap<>();
        map.put("mapkey", "mapvalue");
        b.putSerializable("map", map);

        String s = YouboraUtil.stringifyBundle(b);

        assertNotNull(s);
        String expectedString = "{\"bundleKey\":{\"doubleKey\":23,\"stringArrayKey\":[\"stringArray1\",\"stringArray2\"]},\"intArrayKey\":[1,2,3,4],\"stringKey\":\"stringValue\",\"map\":{\"mapkey\":\"mapvalue\"},\"floatKey\":1}";
        assertEquals(expectedString, s);

        assertEquals("{}", YouboraUtil.stringifyBundle(new Bundle()));
    }

    @Test
    public void testStringifyMap() {

        assertNull(YouboraUtil.stringifyMap(null));

        Map<String, Object> map = new HashMap<>();
        map.put("keyInt", 23);
        map.put("keyDouble", 23.5);
        map.put("keyArrayInt", new int[]{1,2,3,4,5});
        map.put("keyArrayString", new String[]{"value1", "value2", "value3"});

        Map<String, Object> map2 = new HashMap<>();
        map2.put("key2String", "StringValue");
        map2.put("key2Float", 45f);

        map.put("keyMap", map2);

        String string = YouboraUtil.stringifyMap(map);

        assertNotNull(string);

        String expected = "{\"keyArrayString\":[\"value1\",\"value2\",\"value3\"],\"keyDouble\":23.5,\"keyArrayInt\":[1,2,3,4,5],\"keyMap\":{\"key2String\":\"StringValue\",\"key2Float\":45},\"keyInt\":23}";

        assertEquals(expected, string);

        assertEquals("{}", YouboraUtil.stringifyMap(new HashMap<String, Object>()));
    }
}
