package com.npaw.youbora.lib6;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TimerInstrumentedTest {

    private long time;

    @Test
    public void testIsRunning() throws InterruptedException {

        final CountDownLatch latch = new CountDownLatch(1);

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                Timer t = new Timer(null);

                assertEquals(false, t.isRunning());

                t.start();

                assertEquals(true, t.isRunning());

                t.stop();

                assertEquals(false, t.isRunning());

                latch.countDown();
            }
        });

        latch.await();
    }


    @Test
    public void testCallbackTicks() throws InterruptedException {

        final int ticks = 2;
        final CountDownLatch latchTicks = new CountDownLatch(ticks);

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {

                Timer t = new Timer(new Timer.TimerEventListener() {
                    @Override
                    public void onTimerEvent(long delta) {
                        latchTicks.countDown();
                    }
                }, 1000);

                time = System.currentTimeMillis();
                t.start();
            }
        });

        latchTicks.await(ticks + 1, TimeUnit.SECONDS);

        assertEquals(0, latchTicks.getCount());

        long diff = System.currentTimeMillis() - time;

        assertEquals(ticks * 1000, diff, ticks * 1000 * 0.05);
    }

    @Test
    public void testSetInterval() throws InterruptedException {

        final CountDownLatch latchTicks = new CountDownLatch(1);

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {

                Timer t = new Timer(new Timer.TimerEventListener() {
                    @Override
                    public void onTimerEvent(long delta) {
                        latchTicks.countDown();
                    }
                }, 1000);

                t.setInterval(500);

                time = System.currentTimeMillis();
                t.start();
            }
        });

        latchTicks.await(1, TimeUnit.SECONDS);

        assertEquals(0, latchTicks.getCount());

        long diff = System.currentTimeMillis() - time;

        assertEquals(500, diff, 50);
    }

    @Test
    public void getChrono() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                Timer t = new Timer(null);

                Chrono c = t.getChrono();
                assertNotNull(c);
                t.start();
                assertEquals(c, t.getChrono());
                t.stop();
                assertEquals(c, t.getChrono());

                latch.countDown();
            }
        });

        latch.await();
    }
}
