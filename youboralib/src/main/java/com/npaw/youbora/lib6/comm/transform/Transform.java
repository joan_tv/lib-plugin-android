package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.comm.Request;

import java.util.ArrayList;
import java.util.List;

/**
 * Transform classes in YOUBORA help the library parse and work with data.
 *
 * A Transform makes some kind of task that may block requests until it's done, or applies changes
 * to the requests right before they're finally sent.
 *
 * {@link ResourceTransform}, {@link ViewTransform}... all extend from this class.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public abstract class Transform {

    /**
     * State to indicate the transform is blocked
     */
    public static int STATE_BLOCKED = 0;

    /**
     * State to indicate the transform is not blocked
     */
    public static int STATE_NO_BLOCKED = 1;

    /**
     * State to indicate the transform to not send the request
     */
    public static int STATE_OFFLINE = 2;


    /**
     * List of listeners that will be notified once the Transform is done, if it's asynchronous or
     * it has to wait for something to happen.
     */
    private List<TransformDoneListener> listeners = new ArrayList<>();

    /**
     * Whether the Transform is currently working or not.
     * @see #done()
     */
    protected boolean isBusy;

    /**
     * Whether the Transform has to send the request or not.
     */
    protected boolean sendRequest;

    /**
     * Constructor.
     * Sets internal isBusy flag to true.
     */
    public Transform() {
        isBusy = true;
        sendRequest = true;
    }

    /**
     * Perform any necessary operations on the request.
     * @param request the request to be transformed.
     */
    public abstract void parse(Request request);

    /**
     * By default this will return true until {@link #done()} is called. This can be overridden
     * in order to block {@link Request}s based on any criteria. For instance its
     * {@link Request#getService()}.
     * @param request request that's about to be sent
     * @return true if this particular request is allowed to be sent by this transform.
     */
    public boolean isBlocking(Request request) {
        return isBusy;
    }

    /**
     * By default this will return true. This can be overriden to don't send the request
     * (mostly for offline use)
     * {@link Request#getService()}.
     * @param request request that's about to be sent
     * @return true if this particular request is allowed to be sent by this transform.
     */
    public boolean hasToSend(Request request) {
        return sendRequest;
    }

    /**
     * Sets the isBusy flag to true and notifies all the registered {@link TransformDoneListener}s
     * with the {@link #addTransformDoneListener(TransformDoneListener)} method.
     */
    protected void done() {
        isBusy = false;
        for (TransformDoneListener l : listeners) {
            l.onTransformDone(this);
        }
    }

    /**
     * Add a {@link TransformDoneListener}
     * @param listener the listener to add.
     */
    public void addTransformDoneListener(TransformDoneListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove a {@link TransformDoneListener}
     * @param listener the listener to remove
     * @return whether the listener has been removed or not.
     */
    public boolean removeTransformDoneListener(TransformDoneListener listener) {
        return listeners.remove(listener);
    }


    /**
     * Checks the state of the transform
     * @return STATE_BLOCKED if the transform is blocked, STATE_NO_BLOCKED if the transform is not blocked, STATE_OFFLINE if the transform is on offline mode
     */
    public int getState(){
        if(!sendRequest){
            return STATE_OFFLINE;
        }
        if(isBusy){
            return STATE_BLOCKED;
        }else{
            return STATE_NO_BLOCKED;
        }
    }

    /**
     * Interface to notify observers that any asynchronous work done by this Transform has been
     * completed.
     */
    public interface TransformDoneListener {
        void onTransformDone(Transform transform);
    }
}
