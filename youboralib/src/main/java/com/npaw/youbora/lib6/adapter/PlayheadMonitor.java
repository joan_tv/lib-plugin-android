package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Timer;

/**
 * This class periodically checks the player's playhead in order to infer buffer and/or seek events.
 *
 * The {@link PlayheadMonitor} is bounded to a {@link PlayerAdapter} and fires its buffer and seek
 * start and end methods.
 *
 * In order to use this feature, {@link PlayerAdapter#monitorPlayhead(boolean, boolean, int)} should
 * be called from the Adapter's overridden constructor.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class PlayheadMonitor {

    // Monitor types
    /**
     * Monitor nothing.
     */
    public static final int TYPE_NONE = 0;
    /**
     * Monitor buffer.
     */
    public static final int TYPE_BUFFER = 1;
    /**
     * Monitor seek.
     */
    public static final int TYPE_SEEK = 2;

    private static final double BUFFER_THRESHOLD_RATIO = 0.5;
    private static final double SEEK_THRESHOLD_RATIO = 2;

    private Timer timer;
    private double lastPlayhead;
    private Chrono chrono;
    private boolean bufferEnabled;
    private boolean seekEnabled;
    private PlayerAdapter adapter;
    private boolean running;

    /**
     * Constructor
     *
     * @param adapter the adapter from where to watch the playhead
     * @param type type of monitoring desired, can be TYPE_BUFFER, TYPE_SEEK or both; TYPE_BUFFER |
     *             TYPE_SEEK
     * @param interval the interval between playhead checks. If this is 0 or negative, 800 ms will
     *                 be used instead.
     */
    public PlayheadMonitor(PlayerAdapter adapter, int type, int interval) {
        this.adapter = adapter;
        seekEnabled = (type & TYPE_SEEK) == TYPE_SEEK;
        bufferEnabled = (type & TYPE_BUFFER) == TYPE_BUFFER;
        interval = interval > 0 ? interval : 800; // 800 ms by default

        chrono = createChrono();
        lastPlayhead = 0;

        if (interval > 0) {
            timer = createTimer(new Timer.TimerEventListener() {
                @Override
                public void onTimerEvent(long delta) {
                    progress();
                }
            }, interval);
        }
    }

    /**
     * Start monitoring playhead
     */
    public void start() {
        if (timer != null) timer.start();

        running = true;
    }

    /**
     * Stop monitoring playhead
     */
    public void stop() {
        if (timer != null) timer.stop();

        running = false;
    }

    /**
     * Used to ignore the immediate next check after this call. Used when the player resumes
     * playback after a pause or a seek.
     */
    public void skipNextTick() {
        lastPlayhead = 0;
    }

    /**
     * Get the playhead from the {@link PlayerAdapter}
     * @return the Adapter's current playhead.
     */
    protected Double getPlayhead() {
        return adapter.getPlayhead();
    }

    /**
     * Call this method at every tick of timeupdate/progress.
     * If you defined an interval, do not fire this method manually.
     */
    public void progress() {

        // Reset timer
        long deltaTime = chrono.stop();
        chrono.start();

        // Define thresholds
        double bufferThreshold = deltaTime * BUFFER_THRESHOLD_RATIO;
        double seekThreshold = deltaTime * SEEK_THRESHOLD_RATIO;

        // Calculate diff playhead
        double currentPlayhead = getPlayhead() != null? getPlayhead() : 0.0;
        double diffPlayhead = Math.abs(lastPlayhead - currentPlayhead) * 1000; // Seconds -> millis

        if (diffPlayhead < bufferThreshold) {
            // Playhead is stalling: buffer
            if (bufferEnabled &&
                    lastPlayhead > 0 &&
                    !adapter.getFlags().isPaused() &&
                    !adapter.getFlags().isSeeking()) {
                adapter.fireBufferBegin(false); // don't convert from seek
            }
        } else if (diffPlayhead > seekThreshold) {
            // Playhead has jumped: seek
            if (seekEnabled && lastPlayhead > 0) {
                adapter.fireSeekBegin(true); // convert from buffer to seek
            }
        } else {
            // Healthy: close buffers and seeks
            if (seekEnabled && adapter.getFlags().isSeeking()) {
                adapter.fireSeekEnd();
            } else if (bufferEnabled && adapter.getFlags().isBuffering()) {
                adapter.fireBufferEnd();
            }
        }

        // Update playhead
        lastPlayhead = currentPlayhead;
    }

    /**
     * Get current monitor state
     * @return current {@link PlayheadMonitor} state
     */
    public boolean isRunning(){
        return running;
    }

    Chrono createChrono() {
        return new Chrono();
    }

    Timer createTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }
}
