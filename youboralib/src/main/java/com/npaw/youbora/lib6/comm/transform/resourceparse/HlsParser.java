package com.npaw.youbora.lib6.comm.transform.resourceparse;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that asynchronously parses an HLS resource in order to get to the transport stream URL.
 *
 * The point behind this class is that some customers do not host the HLS manifest in the same host
 * or even CDN where the actual content chunks are located.
 *
 * Since the CDN detection is performed with the resource url, it is essential that this resource
 * url is pointing to the CDN that is actually hosting the chunks.
 *
 * HLS manifests can be multi-level so this class uses a recursive approach to get to the final
 * chunk file.
 * @author      Nice People at Work
 * @since       6.0
 */
public class HlsParser {

    private List<HlsTransformListener> listeners = new ArrayList<>();

    private static final String REG_EXP_VIDEO_EXTENSION_PATTERN = "(\\S*?(\\.m3u8|\\.m3u|\\.ts|\\.mp4)(?:\\?\\S*|\\n|\\r|$))";
    private Pattern regexPattern;
    private String realResource;
    //private boolean isBusy;

    /**
     * Constructor
     */
    public HlsParser() {
        //isBusy = true;
        realResource = null;
    }

    /**
     * Starts the HLS parsing from the given resource. The first (outside) call should set the
     * parentResource to null.
     * @param resource the resource url to start parsing
     * @param parentResource parent resource, usually null.
     */
    public void parse(String resource, String parentResource) {

        if (parentResource == null /* || parentResource.length() == 0 */) {
            parentResource = "";
        }

        if (regexPattern == null) {
            regexPattern = Pattern.compile(REG_EXP_VIDEO_EXTENSION_PATTERN, Pattern.CASE_INSENSITIVE);
        }

        Matcher matcher = regexPattern.matcher(resource);

        if (matcher.find()) {
            MatchResult result = matcher.toMatchResult();
            String res = result.group(1);
            String extension = result.group(2);
            if (res != null && extension != null) {
                res = res.trim();
                if (!res.toLowerCase(Locale.US).startsWith("http")){
                    int index = parentResource.lastIndexOf('/');
                    if (index != -1) {
                        res = parentResource.substring(0, index + 1) + res;
                    }
                }
                final String nextParentResource = res;
                if (extension.endsWith("m3u8") || extension.endsWith("m3u")){

                    Request request = createRequest(res, null);

                    request.addOnSuccessListener(new Request.RequestSuccessListener() {
                        @Override
                        public void onRequestSuccess(HttpURLConnection connection, String response) {
                            parse(response, nextParentResource);
                        }
                    });

                    request.addOnErrorListener(new Request.RequestErrorListener() {
                        @Override
                        public void onRequestError(HttpURLConnection connection) {
                            done();
                        }
                    });

                    request.send();

                } else {
                    this.realResource = res;
                    done();
                }
            } else {
                this.realResource = res;
                done();
            }
        } else {
            YouboraLog.warn("Parse HLS Regex couldn't match.");
            done();
        }
    }

    /**
     * Get the parsed resource. Will be null if parsing is not yet started and can be a partial
     * (an intermediate manifest) result if the parser is still running.
     * @return the parsed resource.
     */
    public String getResource() {
        return realResource;
    }

    private void done() {
        //isBusy = false;
        for (HlsTransformListener l : listeners) {
            l.onHlsTransformDone(getResource());
        }
    }

    /**
     * Add a listener that will be notified once the parsing finishes.
     * @param listener the listener to add
     */
    public void addHlsTransformListener(HlsTransformListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     * @return whether the listener has been removed or not
     */
    public boolean removeHlsTransformListener(HlsTransformListener listener) {
        return listeners.remove(listener);
    }

    /**
     * Callback interface that will be called when the {@link HlsParser} is done parsing the resource.
     */
    public interface HlsTransformListener {
        void onHlsTransformDone(String parsedResource);
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }
}
