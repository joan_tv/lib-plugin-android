package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.npaw.youbora.lib6.Constants.SERVICE_BUFFER;
import static com.npaw.youbora.lib6.Constants.SERVICE_JOIN;
import static com.npaw.youbora.lib6.Constants.SERVICE_PING;
import static com.npaw.youbora.lib6.Constants.SERVICE_SEEK;
import static com.npaw.youbora.lib6.Constants.SERVICE_START;

/**
 * This class ensures NQS6 backwards compatibility mainly by cloning a few params.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Nqs6Transform extends Transform {

    private static Pattern entityTypeAndValuePattern;

    /**
     * Constructor
     */
    public Nqs6Transform() {
        done();
    }

    /**
     * {@inheritDoc}
     *
     * Clones (duplicates) the needed params for each request. This way they're also sent with the
     * old NQS6 compliant name.
     */
    @Override
    public void parse(Request request) {
        cloneParam(request, "accountCode", "system");
        cloneParam(request, "transactionCode", "transcode");
        cloneParam(request, "username", "user");
        cloneParam(request, "mediaResource", "resource");
        cloneParam(request, "errorMsg", "msg");

        String service = request.getService();
        
        if (service == null || service.length() == 0) {
            return;
        }
        
        if (!service.equals(SERVICE_JOIN)) {
            cloneParam(request, "playhead", "time");
        }

        switch (service) {
            case SERVICE_PING: {
                /*
                 * NQS6 only allows one entity change per ping. In order to be as most backwards
                 * compatible as possible, at least we send one.
                 * The first match returned by the regex will be sent.
                 * We use a regex here since at this point entities is already stringified.
                 */
                Object entities = request.getParam("entities");
                if (entities instanceof String) {

                    if (entityTypeAndValuePattern == null) {
                        entityTypeAndValuePattern = Pattern.compile("\"(.+?)\":\"?(.+?)\"?[,}]");
                    }

                    Matcher m = entityTypeAndValuePattern.matcher((String) entities);
                    if (m.find()) {
                        String entityType = m.group(1);
                        String entityValue = m.group(2);

                        request.setParam("entityType", entityType);
                        request.setParam("entityValue", entityValue);
                    }

                }
            }
                break;

            case SERVICE_BUFFER:
                cloneParam(request, "bufferDuration", "duration");
                break;

            case SERVICE_SEEK:
                cloneParam(request, "seekDuration", "duration");
                break;

            case SERVICE_START:
                cloneParam(request, "mediaDuration", "duration");
                break;

            case SERVICE_JOIN:
                cloneParam(request, "joinDuration", "time");
                cloneParam(request, "playhead", "eventTime");
                break;

            default:
        }
    }

    private static void cloneParam(Request request, String from, String to) {
        Object param = request.getParam(from);
        if (param != null)
            request.setParam(to, param);
    }
}
