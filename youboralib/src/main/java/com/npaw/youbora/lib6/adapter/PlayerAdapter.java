package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main Adapter class. All specific player adapters should extend this class specifying a player
 * class.
 *
 * The Adapter works as the 'glue' between the player and YOUBORA acting both as event translator
 * and as proxy for the {@link Plugin} to get info from the player.
 *
 * It's a good practice when implementing a new Adapter to create protected methods and call those
 * when player events are detected instead of just calling the {@code fire*} methods. This will
 * allow future users of the Adapter to customize its behaviour by overriding these methods.
 * @author      Nice People at Work
 * @since       6.0
 */
public class PlayerAdapter<PlayerT> {

    /**
     * Enum defining the possible Youbora ad positions values. This refers to the timeline position
     * of a particular ad in relation to the content.
     * @see #getPosition()
     */
    public enum AdPosition {
        PRE, // Preroll
        MID, // Midroll
        POST, // Postroll
        UNKNOWN
    }
    protected PlayerT player;

    protected PlayheadMonitor monitor;

    protected PlaybackFlags flags;

    protected PlaybackChronos chronos;

    protected Plugin plugin;

    // Listener list
    protected List<AdapterEventListener> eventListeners = new ArrayList<>();

    /**
     * Constructor.
     * When overriding it, make sure to call super.
     * Implement the logic to register to the player events in the {@link #registerListeners()} method.
     * Usually you will want to call {@link #registerListeners()} from the overridden constructor.
     * @param player the player instance this Adapter will be bounded to.
     */
    public PlayerAdapter(PlayerT player) {
        this.player = player;

        flags = new PlaybackFlags();
        chronos = new PlaybackChronos();

        if (YouboraLog.debugLevel().isAtLeast(YouboraLog.Level.NOTICE)) {
            YouboraLog.notice("Adapter " + getVersion() + " with lib " + BuildConfig.VERSION_NAME + " is ready.");
        }
    }

    /**
     * Override to set event binders.
     * Call this method from the overridden constructor, or from the client code right after creating
     * the specific adapter instance.
     */
    public void registerListeners() {

    }

    /**
     * Override to unset event binders.
     */
    public void unregisterListeners() {

    }

    /**
     * Stops view session if applies and clears resources by calling {@link #unregisterListeners()}.
     */
    public void dispose() {

        if (getMonitor() != null) {
            getMonitor().stop();
        }
        fireStop();
        setPlayer(null);
    }

    /**
     * Creates a {@link PlayheadMonitor} and configures it.
     * @param monitorBuffers whether to watch for buffers or not
     * @param monitorSeeks whether to watch for seeks or not
     * @param interval the interval at which to perform playhead checks. {@link #getPlayhead()} will
     *                 be called once every {@code interval} milliseconds.
     */
    public void monitorPlayhead(boolean monitorBuffers, boolean monitorSeeks, int interval) {

        int type = 0;
        if (monitorBuffers) type |= PlayheadMonitor.TYPE_BUFFER;
        if (monitorSeeks) type |= PlayheadMonitor.TYPE_SEEK;

        if (type != 0) {
            monitor = createPlayheadMonitor(this, type, interval);
        }
    }

    PlayheadMonitor createPlayheadMonitor(PlayerAdapter adapter, int type, int interval) {
        return new PlayheadMonitor(adapter, type, interval);
    }

    /**
     * Returns the player this Adapter is attached to.
     * @return the player.
     */
    public PlayerT getPlayer() {
        return player;
    }

    /**
     * Sets the plugin
     * @param plugin the plugin to set
     */
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Sets a new player, removes the old listeners if needed.
     *
     * @param player Player to be registered.
     */
    public void setPlayer(PlayerT player) {
        if (this.player != null) {
            unregisterListeners();
        }

        this.player = player;

        if (this.player != null) {
            registerListeners();
        }
    }

    // GETTERS
    /**
     * Instance of {@link PlaybackFlags}
     * @return PlaybackFlags
     */
    public PlaybackFlags getFlags() {
        return flags;
    }

    /**
     * Instance of {@link PlayheadMonitor}
     * @return PlayheadMonitor
     */
    public PlayheadMonitor getMonitor() {
        return monitor;
    }

    /**
     * Instance of {@link PlaybackChronos}
     * @return PlaybackChronos
     */
    public PlaybackChronos getChronos() {
        return chronos;
    }

    /**
     * Instance of {@link Plugin}
     * @return the Plugin instance associated with this Adapter
     */
    public Plugin getPlugin() {
        return plugin;
    }

    /** Override to return current playhead of the video
     *
     * @return the current playhead (position) in seconds
     */
    public Double getPlayhead() {
        return null;
    }

    /** Override to return current playrate
     *
     * @return the current play rate of the media (0 is paused, 1 is playing at normal speed)
     */
    public Double getPlayrate() {
        return getFlags().isPaused() ? 0.0 : 1.0;
    }

    /** Override to return Frames Per Second (FPS)
     *
     * @return the current FPS of the media
     */
    public Double getFramesPerSecond() {
        return null;
    }

    /** Override to return dropped frames since start
     *
     * @return the total dropped frames
     */
    public Integer getDroppedFrames() {
        return null;
    }

    /** Override to return video duration
     *
     * @return the duration of the media in seconds
     */
    public Double getDuration() {
        return null;
    }

    /** Override to return current bitrate
     *
     * @return the current real (consumed) bitrate in bits per second
     */
    public Long getBitrate() {
        return null;
    }

    /** Override to return user bandwidth throughput
     *
     * @return the current throughput in bits per second
     */
    public Long getThroughput() {
        return null;
    }

    /** Override to return rendition
     * @see com.npaw.youbora.lib6.YouboraUtil#buildRenditionString(int, int, double)
     * @return a string that represents the current rendition (quality level)
     */
    public String getRendition() {
        return null;
    }

    /** Override to return title
     *
     * @return the title of the media being played
     */
    public String getTitle() {
        return null;
    }

    /** Override to return title2
     *
     * @return the secondary title. It may be program name, episode, etc.
     */
    public String getTitle2() {
        return null;
    }

    /** Override to recurn true if live and false if VOD
     *
     * @return whether the currently playing content is a live stream or not
     */
    public Boolean getIsLive() {
        return null;
    }

    /** Override to return resource URL.
     *
     * @return the currently playing resource (URL)
     */
    public String getResource() {
        return null;
    }

    /** Override to return player version
     *
     * @return the player version
     */
    public String getPlayerVersion() {
        return null;
    }

    /** Override to return player's name
     *
     * @return the player name
     */
    public String getPlayerName() {
        return null;
    }

    /** Override to return adapter version.
     *
     * @return the adapter version
     */
    public String getVersion() {
        return BuildConfig.VERSION_NAME + "-generic-android";
    }

    /**
     * Override to return current ad position (only for ads)
     * @see AdPosition
     * @return the current ad position
     */
    public AdPosition getPosition() {
        return null;
    }
    
    // FLOW METHODS
    /**
     * Shortcut for {@link #fireStart(Map)} with {@code params = null}.
     */
    public void fireStart() {
        fireStart(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    public void fireStart(Map<String, String> params) {
        if (!getFlags().isStarted()) {
            getFlags().setStarted(true);
            if(!getFlags().isAdInitiated()){
                getChronos().join.start();
            }else{
                getChronos().adInit.stop();
            }
            getChronos().total.start();

            for (AdapterEventListener l : eventListeners) {
                l.onStart(params);
            }
        }
    }

    /**
     * Emits related event and set flags if current status is valid. Only for ads
     */

    public void fireAdInit(){
        fireAdInit(null);
    }

    /**
     * Emits related event and set flags if current status is valid. Only for ads
     */

    public void fireAdInit(Map<String, String> params){
        if(!getFlags().isAdInitiated()){
            getFlags().setAdInitiated(true);

            getChronos().adInit.start();
            getChronos().join.start();
            getChronos().total.start();
        }
        for (AdapterEventListener l : eventListeners) {
            l.onAdInit(params);
        }
    }

    /**
     * Shortcut for {@link #fireJoin(Map)} with {@code params = null}.
     */
    public void fireJoin() {
        fireJoin(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    protected void fireJoin(Map<String, String> params) {
        if (getFlags().isStarted() && !getFlags().isJoined()) {
            if (getMonitor() != null) getMonitor().start();

            getFlags().setJoined(true);
            getChronos().join.stop();

            for (AdapterEventListener l : eventListeners) {
                l.onJoin(params);
            }
        }
    }

    /**
     * Shortcut for {@link #firePause(Map)} with {@code params = null}.
     */
    public void firePause() {
        firePause(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    public void firePause(Map<String, String> params) {
        if (getFlags().isJoined() && !getFlags().isPaused()) {
            getFlags().setPaused(true);
            getChronos().pause.start();
            for (AdapterEventListener l : eventListeners) {
                l.onPause(params);
            }
        }
    }

    /**
     * Shortcut for {@link #fireResume(Map)} with {@code params = null}.
     */
    public void fireResume() {
        fireResume(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    public void fireResume(Map<String, String> params) {
        if (getFlags().isJoined() && getFlags().isPaused()) {
            getFlags().setPaused(false);
            getChronos().pause.stop();

            if (getMonitor() != null) getMonitor().skipNextTick();

            for (AdapterEventListener l : eventListeners) {
                l.onResume(params);
            }
        }
    }

    /**
     * Shortcut for {@link #fireBufferBegin(Map, boolean)} with {@code params = null} and
     * {@code convertFromSeek = false}.
     */
    public void fireBufferBegin() {
        fireBufferBegin(null, false);
    }

    /**
     * Shortcut for {@link #fireBufferBegin(Map, boolean)} with {@code params = null}.
     * @param convertFromSeek whether to convert an existing seek into buffer or not
     */
    public void fireBufferBegin(boolean convertFromSeek) {
        fireBufferBegin(null, convertFromSeek);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     * @param convertFromSeek whether to convert an existing seek into buffer or not
     */
    public void fireBufferBegin(Map<String, String> params, boolean convertFromSeek) {
        if (getFlags().isJoined() && !getFlags().isBuffering()) {

            if (getFlags().isSeeking()) {
                if (convertFromSeek) {
                    YouboraLog.notice("Converting current buffer to seek");

                    getChronos().buffer = getChronos().seek.copy();
                    getChronos().seek.reset();

                    getFlags().setSeeking(false);
                } else {
                    return;
                }

            } else {
                getChronos().buffer.start();
            }

            getFlags().setBuffering(true);

            for (AdapterEventListener l : eventListeners) {
                l.onBufferBegin(params, convertFromSeek);
            }
        }
    }

    /**
     * Shortcut for {@link #fireBufferEnd(Map)} with {@code params = null}.
     */
    public void fireBufferEnd() {
        fireBufferEnd(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    public void fireBufferEnd(Map<String, String> params) {
        if (getFlags().isJoined() && getFlags().isBuffering()) {

            getFlags().setBuffering(false);
            getChronos().buffer.stop();

            for (AdapterEventListener l : eventListeners) {
                l.onBufferEnd(params);
            }
        }
    }

    /**
     * Shortcut for {@link #fireSeekBegin(Map, boolean)} with {@code params = null} and
     * {@code convertFromBuffer = false}.
     */
    public void fireSeekBegin() {
        fireSeekBegin(null, true);
    }

    /**
     * Shortcut for {@link #fireSeekBegin(Map, boolean)} with {@code params = null}.
     * @param convertFromBuffer whether to convert an existing buffer into seek or not
     */
    public void fireSeekBegin(boolean convertFromBuffer) {
        fireSeekBegin(null, convertFromBuffer);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     * @param convertFromBuffer whether to convert an existing buffer into seek or not
     */
    public void fireSeekBegin(Map<String, String> params, boolean convertFromBuffer) {
        if (getFlags().isJoined() && !getFlags().isSeeking()) {
            if (getFlags().isBuffering()) {
                if (convertFromBuffer) {
                    YouboraLog.notice("Converting current buffer to seek");

                    getChronos().seek = getChronos().buffer.copy();
                    getChronos().buffer.reset();

                    getFlags().setBuffering(false);
                } else {
                    return;
                }
            } else {
                getChronos().seek.start();
            }

            getFlags().setSeeking(true);

            for (AdapterEventListener l : eventListeners) {
                l.onSeekBegin(params, convertFromBuffer);
            }
        }
    }

    /**
     * Shortcut for {@link #fireSeekEnd(Map)} with {@code params = null}.
     */
    public void fireSeekEnd() {
        fireSeekEnd(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    public void fireSeekEnd(Map<String, String> params) {
        if (getFlags().isJoined() && getFlags().isSeeking()) {

            getFlags().setSeeking(false);
            getChronos().seek.stop();

            if (getMonitor() != null) getMonitor().skipNextTick();

            for (AdapterEventListener l : eventListeners) {
                l.onSeekEnd(params);
            }

        }
    }

    /**
     * Shortcut for {@link #fireStop(Map)} with {@code params = null}.
     */
    public void fireStop() {
        fireStop(null);
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request
     */
    public void fireStop(Map<String, String> params) {
        if (getFlags().isStarted()) {
            if(getFlags().isEnded() && !getFlags().isStopped()){
                int adsAfterStop = getPlugin().getOptions().getAdsAfterStop();
                if(adsAfterStop != 0){
                    getPlugin().getOptions().setAdsAfterStop(--adsAfterStop);
                    return;
                }
            }

            if (getMonitor() != null) getMonitor().stop();

            getFlags().reset();

            if(plugin != null){
                //We inform of the pauseDuration here to save it before the reset
                if(plugin.getPauseDuration() != -1){
                    params = new HashMap<String, String>() {{
                        put("pauseDuration",String.valueOf(plugin.getPauseDuration()));
                    }};
                }
            }

            getChronos().total.stop();
            getChronos().join.reset();
            getChronos().pause.reset();
            getChronos().buffer.reset();
            getChronos().seek.reset();
            getChronos().adInit.reset();

            for (AdapterEventListener l : eventListeners) {
                l.onStop(params);
            }
        }
    }

    /**
     * Basic error handler. msg, code, errorMetadata and level params can be included in the params
     * argument.
     * @param params params to add to the request. If it is null default values will be added.
     */
    public void fireError(Map<String, String> params) {
        params = YouboraUtil.buildErrorParams(params);
        for (AdapterEventListener l : eventListeners) {
            l.onError(params);
        }
    }

    /**
     * Sends a non-fatal error (with {@code level = "error"}).
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     */
    public void fireError(String msg, String code, String errorMetadata) {
        fireError(YouboraUtil.buildErrorParams(msg, code, errorMetadata, null));
    }

    /**
     * Shortcut for {@link #fireError(Map)} setting an entry in the map as
     * {@code errorLevel = "fatal"}. This method will also send a stop after the error.
     * @param params params to add to the request. If it is null default values will be added.
     */
    public void fireFatalError(Map<String, String> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put("errorLevel", "fatal");
        fireError(YouboraUtil.buildErrorParams(params));
        fireStop();
    }

    /**
     * Sends a fatal error (with {@code level = "fatal"}).
     * This method will also send a stop after the error.
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     */
    public void fireFatalError(String msg, String code, String errorMetadata) {
        fireError(YouboraUtil.buildErrorParams(msg, code, errorMetadata, "fatal"));
        fireStop();
    }

    /**
     * Shortcut for {@link #fireClick(Map)} with {@code params = null}.
     */

    public void fireClick(){
        fireClick(null);
    }

    /**
     * Emits related event and set flags if current status is valid. Only for ads
     */

    public void fireClick(Map<String, String> params){
        for (AdapterEventListener l : eventListeners)  {
            l.onClick(params);
        }
    }

    /**
     * Adds an event listener that will be invoked whenever a "fire*" method is called.
     * @param eventListener the listener to add
     */
    public void addEventListener(AdapterEventListener eventListener) {
        eventListeners.add(eventListener);
    }

    /**
     * Remove an event listener
     * @param eventListener listener ot remove
     * @return whether the listener has been removed or not
     */
    public boolean removeEventListener(AdapterEventListener eventListener) {
        return eventListeners.remove(eventListener);
    }

    /**
     * Listener interface. The methods will be called whenever each corresponding event is fired.
     * These events are listened by the {@link Plugin} in order to send the corresponding request.
     */
    public interface AdapterEventListener {

        /**
         * Adapter detected a start event.
         * @param params params to add to the request
         */
        void onStart(Map<String, String> params);

        /**
         * Adapter detected a join event.
         * @param params params to add to the request
         */
        void onJoin(Map<String, String> params);

        /**
         * Adapter detected a pause event.
         * @param params params to add to the request
         */
        void onPause(Map<String, String> params);

        /**
         * Adapter detected a resume event.
         * @param params params to add to the request
         */
        void onResume(Map<String, String> params);

        /**
         * Adapter detected a stop event.
         * @param params params to add to the request
         */
        void onStop(Map<String, String> params);

        /**
         * Adapter detected a buffer begin event.
         * @param params params to add to the request
         * @param convertFromSeek whether the buffer has been converted from a seek or not
         */
        void onBufferBegin(Map<String, String> params, boolean convertFromSeek);

        /**
         * Adapter detected a buffer end event.
         * @param params params to add to the request
         */
        void onBufferEnd(Map<String, String> params);

        /**
         * Adapter detected a seek begin event.
         * @param params params to add to the request
         * @param convertFromBuffer whether the seek has been converted from a buffer or not
         */
        void onSeekBegin(Map<String, String> params, boolean convertFromBuffer);

        /**
         * Adapter detected a seek end event.
         * @param params params to add to the request
         */
        void onSeekEnd(Map<String, String> params);

        /**
         * Adapter an ad click
         * @param params params to add to the request
         */
        void onClick(Map<String, String> params);

        /**
         * Adapter an ad init
         * @param params params to add to the request
         */
        void onAdInit(Map<String, String> params);

        /**
         * Adapter detected an error event.
         * @param params params to add to the request
         */
        void onError(Map<String, String> params);
    }

    /**
     * Convenience class that provides an empty implementation for all the methods in the
     * {@link AdapterEventListener}.
     * Use in case that you only want to register to a subset of all the listeners.
     */
    public static class AdapterEventListenerImpl implements AdapterEventListener {

        @Override
        public void onStart(Map<String, String> params) { }
        @Override
        public void onJoin(Map<String, String> params) { }
        @Override
        public void onPause(Map<String, String> params) { }
        @Override
        public void onResume(Map<String, String> params) { }
        @Override
        public void onStop(Map<String, String> params) { }
        @Override
        public void onBufferBegin(Map<String, String> params, boolean convertFromBuffer) { }
        @Override
        public void onBufferEnd(Map<String, String> params) { }
        @Override
        public void onSeekBegin(Map<String, String> params, boolean convertFromBuffer) { }
        @Override
        public void onSeekEnd(Map<String, String> params) { }
        @Override
        public void onClick(Map<String, String> params) { }
        @Override
        public void onAdInit(Map<String, String> params) { }
        @Override
        public void onError(Map<String, String> params) { }
    }
}
