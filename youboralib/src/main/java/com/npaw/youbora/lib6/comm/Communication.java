package com.npaw.youbora.lib6.comm;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.transform.Transform;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Communication implements an abstraction layer over API requests.
 * Internally, Communication implements queues of {@link Request} objects.
 * This queue can be blocked depending on its {@link Transform}.
 * @author      Nice People at Work
 * @since       6.0
 */

public class Communication {

    private List<Transform> transforms;
    private Queue<Request> requests;

    /**
     * Constructor
     */
    public Communication() {
        transforms = new ArrayList<>();
        requests = new LinkedList<>(); // Queue
    }

    /**
     * Adds the {@link Request} to the queue. Doing this will process the pending requests.
     * @param request the request to add to the queue
     * @param callback if not null, added as a success listener to the Request
     */
    public void sendRequest(Request request, Request.RequestSuccessListener callback) {
        if (request != null) {
            if (callback != null) {
                request.addOnSuccessListener(callback);
            }
            registerRequest(request);
        }
    }

    /**
     * Add a {@link Transform} to the transforms list
     * @param transform transform to add
     */
    public void addTransform(Transform transform) {
        if (transform != null) {
            transform.addTransformDoneListener(new Transform.TransformDoneListener() {
                @Override
                public void onTransformDone(Transform transform) {
                    processRequests();
                }
            });
            transforms.add(transform);
        } else {
            YouboraLog.warn("Transform is null");
        }
    }

    /**
     * Remove a {@link Transform} from the transforms list
     * @param transform the transform to remove
     */
    public void removeTransform(Transform transform) {
        if (!transforms.remove(transform)) {
            YouboraLog.warn("Trying to remove unexisting Transform: " + transform);
        }
    }

    // Private methods
    private void registerRequest(Request request) {
        requests.add(request);
        processRequests();
    }

    private void processRequests() {

        Iterator<Request> it = requests.iterator();

        while (it.hasNext()) {
            Request request = it.next();


            if (transform(request) == Transform.STATE_OFFLINE) {
                it.remove();
            }else{
                if (transform(request) == Transform.STATE_NO_BLOCKED) {
                    it.remove();
                    request.send();
                }
            }
        }
    }

    private int transform(Request request) {

        for (Transform transform : transforms) {
            if (transform.isBlocking(request)){
                return Transform.STATE_BLOCKED;
            } else {
                transform.parse(request);
            }
            if(transform.getState() == Transform.STATE_OFFLINE){
                return Transform.STATE_OFFLINE;
            }
        }
        return Transform.STATE_NO_BLOCKED;
    }

    /*private boolean transform(Request request) {

        for (Transform transform : transforms) {
            if (transform.isBlocking(request)){
                return false;
            } else {
                transform.parse(request);
            }
        }

        return true;
    }*/
}
