package com.npaw.youbora.lib6.plugin;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_AKAMAI;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_CLOUDFRONT;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_FASTLY;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_HIGHWINDS;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_LEVEL3;

/**
 * This class stores all the Youbora configuration settings.
 * Any value specified in this class, if set, will override the info the plugin is able to get on
 * its own.
 *
 * The only <b>required</b> option is the {@link #accountCode}.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Options {

    /**
     * If enabled the plugin won't send NQS requests.
     * Default: true
     */
    private boolean enabled;

    /**
     * Define the security of NQS calls.
     * If true it will use "https://".
     * If false it will use "http://".
     * Default: true
     */
    private boolean httpSecure;

    /**
     * Host of the Fastdata service.
     */
    private String host;

    /**
     * NicePeopleAtWork account code that indicates the customer account.
     */
    private String accountCode;

    /**
     * User ID value inside your system.
     */
    private String username;

    /**
     * If true the plugin will store the events and send them later when there's connection
     */
    private boolean offline;

    /**
     * If true the plugin will parse HLS files to use the first .ts file found as resource.
     * It might slow performance down.
     * Default: false
     */
    private boolean parseHls;

    /**
     * If defined, resource parse will try to fetch the CDN code from the custom header defined
     * by this property, e.g. "x-cdn-forward"
     */
    private String parseCdnNameHeader;

    /**
     * If true the plugin will query the CDN to retrieve the node name.
     * It might slow performance down.
     * Default: false
     */
    private boolean parseCdnNode;

    /**
     * List of CDN names to parse. This is only used when {@link #parseCdnNode} is enabled.
     * Order is respected when trying to match against a CDN.
     * Default: ["Akamai", "Cloudfront", "Level3", "Fastly", "Highwinds"].
     */
    private ArrayList<String> parseCdnNodeList;

    /**
     * IP of the viewer/user, e.g. "48.15.16.23".
     */
    private String networkIP;

    /**
     * Name of the internet service provider of the viewer/user.
     */
    private String networkIsp;

    /**
     * See a list of codes in <a href="http://mapi.youbora.com:8081/connectionTypes">http://mapi.youbora.com:8081/connectionTypes</a>.
     */
    private String networkConnectionType;

    /**
     * Youbora's device code. If specified it will rewrite info gotten from user agent.
     * See a list of codes in <a href="http://mapi.youbora.com:8081/devices">http://mapi.youbora.com:8081/devices</a>.
     */
    private String deviceCode;

    /**
     * Flag to send the start by the adapter or not
     */
    private boolean autoStart;

    /**
     * URL/path of the current media resource.
     */
    private String contentResource;

    /**
     * true if the content is Live. false if VOD.
     */
    private Boolean contentIsLive;

    /**
     * Title of the media.
     */
    private String contentTitle;

    /**
     * Secondary title of the media. This could be program name, season, episode, etc.
     */
    private String contentTitle2;

    /**
     * Duration of the media <b>in seconds</b>.
     */
    private Double contentDuration;

    /**
     * Custom unique code to identify the view.
     */
    private String contentTransactionCode;

    /**
     * Bitrate of the content in bits per second.
     */
    private Long contentBitrate;

    /**
     * Throughput of the client bandwidth in bits per second.
     */
    private Long contentThroughput;

    /**
     * Name or value of the current rendition (quality) of the content.
     */
    private String contentRendition;

    /**
     * Codename of the CDN where the content is streaming from.
     * See a list of codes in <a href="http://mapi.youbora.com:8081/cdns">http://mapi.youbora.com:8081/cdns</a>.
     */
    private String contentCdn;

    /**
     * Frames per second of the media being played.
     */
    private Double contentFps;

    /**
     * {@link Bundle} containing mixed extra information about the content like: director, parental rating,
     * device info or the audio channels.
     */
    private transient Bundle contentMetadata;

    /**
     * {@link Bundle} containing mixed extra information about the ads like: director, parental rating,
     * device info or the audio channels.
     */
    private transient Bundle adMetadata;

    /**
     *  If true, youbora blocks ad events and calculates jointime ignoring ad time.
     */
    private boolean ignoreAds;

    /**
     * Set to integer positive value indicating how many ads
     * will be shown as post-rolls if they do it after content player triggers stop event.
     */
    private int adsAfterStop;

    /**
     *  Stop the plugin automatically when the user goes to background
     */
    private boolean autoDetectBackground;

    /**
     * Custom parameter 1.
     */
    private String extraparam1;

    /**
     * Custom parameter 2.
     */
    private String extraparam2;

    /**
     * Custom parameter 3.
     */
    private String extraparam3;

    /**
     * Custom parameter 4.
     */
    private String extraparam4;

    /**
     * Custom parameter 5.
     */
    private String extraparam5;

    /**
     * Custom parameter 6.
     */
    private String extraparam6;

    /**
     * Custom parameter 7.
     */
    private String extraparam7;

    /**
     * Custom parameter 8.
     */
    private String extraparam8;

    /**
     * Custom parameter 9.
     */
    private String extraparam9;

    /**
     * Custom parameter 10.
     */
    private String extraparam10;

    // Keys for Bundle
    private static final String KEY_ENABLED = "enabled";
    private static final String KEY_HTTP_SECURE = "httpSecure";
    private static final String KEY_HOST = "host";
    private static final String KEY_ACCOUNT_CODE = "config.accountCode";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_OFFLINE = "offline";
    private static final String KEY_BACKGROUND = "autoDetectBackground";
    private static final String KEY_AUTOSTART = "autoStart";

    private static final String KEY_PARSE_HLS = "parse.Hls";
    private static final String KEY_PARSE_CDN_NAME_HEADER = "parse.CdnNameHeader";
    private static final String KEY_PARSE_CDN_NODE = "parse.CdnNode";
    private static final String KEY_PARSE_CDN_NODE_LIST = "parse.CdnNodeList";

    private static final String KEY_NETWORK_IP = "network.IP";
    private static final String KEY_NETWORK_ISP = "network.Isp";
    private static final String KEY_NETWORK_CONNECTION_TYPE = "network.connectionType";

    private static final String KEY_DEVICE_CODE = "device.code";

    private static final String KEY_CONTENT_RESOURCE = "content.resource";
    private static final String KEY_CONTENT_IS_LIVE = "content.isLive";
    private static final String KEY_CONTENT_TITLE = "content.title";
    private static final String KEY_CONTENT_TITLE2 = "content.title2";
    private static final String KEY_CONTENT_DURATION = "content.duration";
    private static final String KEY_CONTENT_TRANSACTION_CODE = "content.transactionCode";
    private static final String KEY_CONTENT_BITRATE = "content.bitrate";
    private static final String KEY_CONTENT_THROUGHPUT = "content.throughput";
    private static final String KEY_CONTENT_RENDITION = "content.rendition";
    private static final String KEY_CONTENT_CDN = "content.cdn";
    private static final String KEY_CONTENT_FPS = "content.fps";
    private static final String KEY_CONTENT_METADATA = "content.metadata";

    private static final String KEY_AD_METADATA = "ad.metadata";
    private static final String KEY_AD_IGNORE = "ad.ignore";
    private static final String KEY_ADS_AFTERSTOP = "ad.afterStop";

    private static final String KEY_EXTRAPARAM_1 = "extraparam.1";
    private static final String KEY_EXTRAPARAM_2 = "extraparam.2";
    private static final String KEY_EXTRAPARAM_3 = "extraparam.3";
    private static final String KEY_EXTRAPARAM_4 = "extraparam.4";
    private static final String KEY_EXTRAPARAM_5 = "extraparam.5";
    private static final String KEY_EXTRAPARAM_6 = "extraparam.6";
    private static final String KEY_EXTRAPARAM_7 = "extraparam.7";
    private static final String KEY_EXTRAPARAM_8 = "extraparam.8";
    private static final String KEY_EXTRAPARAM_9 = "extraparam.9";
    private static final String KEY_EXTRAPARAM_10 = "extraparam.10";

    /**
     * Constructor.
     * Sets the default field values.
     */
    public Options() {

        setEnabled(true);
        setHttpSecure(true);
        setHost("nqs.nice264.com");
        setAccountCode("nicetest");
        setUsername(null);
        setOffline(false);
        setAutoDetectBackground(false);
        setAutoStart(true);

        setParseHls(false);
        setParseCdnNameHeader("x-cdn-forward");
        setParseCdnNode(false);
        setParseCdnNodeList(new ArrayList<String>(5){{
            add(CDN_NAME_AKAMAI);
            add(CDN_NAME_CLOUDFRONT);
            add(CDN_NAME_LEVEL3);
            add(CDN_NAME_FASTLY);
            add(CDN_NAME_HIGHWINDS);
        }});

        setNetworkIP(null);
        setNetworkIsp(null);
        setNetworkConnectionType(null);

        setDeviceCode(null);

        setContentResource(null);
        setContentIsLive(null);
        setContentTitle(null);
        setContentTitle2(null);
        setContentDuration(null);
        setContentTransactionCode(null);
        setContentBitrate(null);
        setContentThroughput(null);
        setContentRendition(null);
        setContentCdn(null);
        setContentFps(null);
        setContentMetadata(new Bundle());

        setAdMetadata(new Bundle());
        setAdIgnore(false);
        setAdsAfterStop(-1);

        setExtraparam1(null);
        setExtraparam2(null);
        setExtraparam3(null);
        setExtraparam4(null);
        setExtraparam5(null);
        setExtraparam6(null);
        setExtraparam7(null);
        setExtraparam8(null);
        setExtraparam9(null);
        setExtraparam10(null);
    }

    /**
     * Constructor.
     * It will populate the fields by reading the values form a {@link Bundle}
     * @param b {@link Bundle} where to read the values from
     */
    public Options(Bundle b) {
        this();

        if (b != null) {

            if (b.containsKey(KEY_ENABLED)) setEnabled(b.getBoolean(KEY_ENABLED));
            if (b.containsKey(KEY_HTTP_SECURE)) setHttpSecure(b.getBoolean(KEY_HTTP_SECURE));
            if (b.containsKey(KEY_HOST)) setHost(b.getString(KEY_HOST));
            if (b.containsKey(KEY_ACCOUNT_CODE)) setAccountCode(b.getString(KEY_ACCOUNT_CODE));
            if (b.containsKey(KEY_USERNAME)) setUsername(b.getString(KEY_USERNAME));
            if (b.containsKey(KEY_OFFLINE)) setOffline(b.getBoolean(KEY_OFFLINE));
            if (b.containsKey(KEY_BACKGROUND)) setAutoDetectBackground(b.getBoolean(KEY_BACKGROUND));
            if (b.containsKey(KEY_AUTOSTART)) setAutoStart(b.getBoolean(KEY_AUTOSTART));

            if (b.containsKey(KEY_PARSE_HLS)) setParseHls(b.getBoolean(KEY_PARSE_HLS));
            if (b.containsKey(KEY_PARSE_CDN_NAME_HEADER)) setParseCdnNameHeader(b.getString(KEY_PARSE_CDN_NAME_HEADER));
            if (b.containsKey(KEY_PARSE_CDN_NODE)) setParseCdnNode(b.getBoolean(KEY_PARSE_CDN_NODE));
            if (b.containsKey(KEY_PARSE_CDN_NODE_LIST)) setParseCdnNodeList(b.getStringArrayList(KEY_PARSE_CDN_NODE_LIST));

            if (b.containsKey(KEY_NETWORK_IP)) setNetworkIP(b.getString(KEY_NETWORK_IP));
            if (b.containsKey(KEY_NETWORK_ISP)) setNetworkIsp(b.getString(KEY_NETWORK_ISP));
            if (b.containsKey(KEY_NETWORK_CONNECTION_TYPE)) setNetworkConnectionType(b.getString(KEY_NETWORK_CONNECTION_TYPE));

            if (b.containsKey(KEY_DEVICE_CODE)) setDeviceCode(b.getString(KEY_DEVICE_CODE));

            if (b.containsKey(KEY_CONTENT_RESOURCE)) setContentResource(b.getString(KEY_CONTENT_RESOURCE));
            if (b.containsKey(KEY_CONTENT_IS_LIVE)) setContentIsLive(b.getBoolean(KEY_CONTENT_IS_LIVE));
            if (b.containsKey(KEY_CONTENT_TITLE)) setContentTitle(b.getString(KEY_CONTENT_TITLE));
            if (b.containsKey(KEY_CONTENT_TITLE2)) setContentTitle2(b.getString(KEY_CONTENT_TITLE2));
            if (b.containsKey(KEY_CONTENT_DURATION)) setContentDuration(b.getDouble(KEY_CONTENT_DURATION));
            if (b.containsKey(KEY_CONTENT_TRANSACTION_CODE)) setContentTransactionCode(b.getString(KEY_CONTENT_TRANSACTION_CODE));
            if (b.containsKey(KEY_CONTENT_BITRATE)) setContentBitrate(b.getLong(KEY_CONTENT_BITRATE));
            if (b.containsKey(KEY_CONTENT_THROUGHPUT)) setContentThroughput(b.getLong(KEY_CONTENT_THROUGHPUT));
            if (b.containsKey(KEY_CONTENT_RENDITION)) setContentRendition(b.getString(KEY_CONTENT_RENDITION));
            if (b.containsKey(KEY_CONTENT_CDN)) setContentCdn(b.getString(KEY_CONTENT_CDN));
            if (b.containsKey(KEY_CONTENT_FPS)) setContentFps(b.getDouble(KEY_CONTENT_FPS));
            if (b.containsKey(KEY_CONTENT_METADATA)) setContentMetadata(b.getBundle(KEY_CONTENT_METADATA));

            if (b.containsKey(KEY_AD_METADATA)) setAdMetadata(b.getBundle(KEY_AD_METADATA));
            if (b.containsKey(KEY_AD_IGNORE)) setAdIgnore(b.getBoolean(KEY_AD_IGNORE));
            if (b.containsKey(KEY_ADS_AFTERSTOP)) setAdsAfterStop(b.getInt(KEY_ADS_AFTERSTOP));

            if (b.containsKey(KEY_EXTRAPARAM_1)) setExtraparam1(b.getString(KEY_EXTRAPARAM_1));
            if (b.containsKey(KEY_EXTRAPARAM_2)) setExtraparam2(b.getString(KEY_EXTRAPARAM_2));
            if (b.containsKey(KEY_EXTRAPARAM_3)) setExtraparam3(b.getString(KEY_EXTRAPARAM_3));
            if (b.containsKey(KEY_EXTRAPARAM_4)) setExtraparam4(b.getString(KEY_EXTRAPARAM_4));
            if (b.containsKey(KEY_EXTRAPARAM_5)) setExtraparam5(b.getString(KEY_EXTRAPARAM_5));
            if (b.containsKey(KEY_EXTRAPARAM_6)) setExtraparam6(b.getString(KEY_EXTRAPARAM_6));
            if (b.containsKey(KEY_EXTRAPARAM_7)) setExtraparam7(b.getString(KEY_EXTRAPARAM_7));
            if (b.containsKey(KEY_EXTRAPARAM_8)) setExtraparam8(b.getString(KEY_EXTRAPARAM_8));
            if (b.containsKey(KEY_EXTRAPARAM_9)) setExtraparam9(b.getString(KEY_EXTRAPARAM_9));
            if (b.containsKey(KEY_EXTRAPARAM_10)) setExtraparam10(b.getString(KEY_EXTRAPARAM_10));
        }
    }

    /**
     * Convert this Options instance into a {@link Bundle} representation.
     *
     * To get the Options object from a bundle use the {@link #Options(Bundle)} constructor.
     * This can be useful to carry the options in an {@link Intent}.
     * @return a {@link Bundle} with the YouboraOptions.
     */
    public Bundle toBundle() {

        Bundle b = new Bundle();

        b.putBoolean(KEY_ENABLED, isEnabled());
        b.putBoolean(KEY_HTTP_SECURE, isHttpSecure());
        if (getHost() != null) b.putString(KEY_HOST, getHost());
        if (getAccountCode() != null) b.putString(KEY_ACCOUNT_CODE, getAccountCode());
        if (getUsername() != null) b.putString(KEY_USERNAME, getUsername());
        b.putBoolean(KEY_OFFLINE, isOffline());
        b.putBoolean(KEY_BACKGROUND, isAutoDetectBackground());
        b.putBoolean(KEY_AUTOSTART, isAutoStart());

        b.putBoolean(KEY_PARSE_HLS, isParseHls());
        if (getParseCdnNameHeader() != null) b.putString(KEY_PARSE_CDN_NAME_HEADER, getParseCdnNameHeader());
        b.putBoolean(KEY_PARSE_CDN_NODE, isParseCdnNode());
        if (getParseCdnNodeList() != null) b.putStringArrayList(KEY_PARSE_CDN_NODE_LIST, getParseCdnNodeList());

        if (getNetworkIP() != null) b.putString(KEY_NETWORK_IP, getNetworkIP());
        if (getNetworkIsp() != null) b.putString(KEY_NETWORK_ISP, getNetworkIsp());
        if (getNetworkConnectionType() != null) b.putString(KEY_NETWORK_CONNECTION_TYPE, getNetworkConnectionType());

        if (getDeviceCode() != null) b.putString(KEY_DEVICE_CODE, getDeviceCode());

        if (getContentResource() != null) b.putString(KEY_CONTENT_RESOURCE, getContentResource());
        if (getContentIsLive() != null) b.putBoolean(KEY_CONTENT_IS_LIVE, getContentIsLive());
        if (getContentTitle() != null) b.putString(KEY_CONTENT_TITLE, getContentTitle());
        if (getContentTitle2() != null) b.putString(KEY_CONTENT_TITLE2, getContentTitle2());
        if (getContentDuration() != null) b.putDouble(KEY_CONTENT_DURATION, getContentDuration());
        if (getContentTransactionCode() != null) b.putString(KEY_CONTENT_TRANSACTION_CODE, getContentTransactionCode());
        if (getContentBitrate() != null) b.putLong(KEY_CONTENT_BITRATE, getContentBitrate());
        if (getContentThroughput() != null) b.putLong(KEY_CONTENT_THROUGHPUT, getContentThroughput());
        if (getContentRendition() != null) b.putString(KEY_CONTENT_RENDITION, getContentRendition());
        if (getContentCdn() != null) b.putString(KEY_CONTENT_CDN, getContentCdn());
        if (getContentFps() != null) b.putDouble(KEY_CONTENT_FPS, getContentFps());
        if (getContentMetadata() != null) b.putBundle(KEY_CONTENT_METADATA, getContentMetadata());

        if (getAdMetadata() != null) b.putBundle(KEY_AD_METADATA, getAdMetadata());
        b.putBoolean(KEY_AD_IGNORE, getAdIgnore());
        b.putInt(KEY_ADS_AFTERSTOP, getAdsAfterStop());

        if (getExtraparam1() != null) b.putString(KEY_EXTRAPARAM_1, getExtraparam1());
        if (getExtraparam2() != null) b.putString(KEY_EXTRAPARAM_2, getExtraparam2());
        if (getExtraparam3() != null) b.putString(KEY_EXTRAPARAM_3, getExtraparam3());
        if (getExtraparam4() != null) b.putString(KEY_EXTRAPARAM_4, getExtraparam4());
        if (getExtraparam5() != null) b.putString(KEY_EXTRAPARAM_5, getExtraparam5());
        if (getExtraparam6() != null) b.putString(KEY_EXTRAPARAM_6, getExtraparam6());
        if (getExtraparam7() != null) b.putString(KEY_EXTRAPARAM_7, getExtraparam7());
        if (getExtraparam8() != null) b.putString(KEY_EXTRAPARAM_8, getExtraparam8());
        if (getExtraparam9() != null) b.putString(KEY_EXTRAPARAM_9, getExtraparam9());
        if (getExtraparam10() != null) b.putString(KEY_EXTRAPARAM_10, getExtraparam10());

        return b;
    }

    /**
     * Getter for enabled
     * @return true if Youbora is enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Enables or disables analytics reporting
     * @param enabled the new value
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Getter for HTTPS
     * @return true if HTTPS is enabled
     */
    public boolean isHttpSecure() {
        return httpSecure;
    }

    /**
     * Enables or disables analytics https
     * @param httpSecure the new value
     */
    public void setHttpSecure(boolean httpSecure) {
        this.httpSecure = httpSecure;
    }

    /**
     * Getter for the host
     * @return the NQS host
     */
    public String getHost() {
        return host;
    }

    /**
     * Setter for the host
     * @param host the new value
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Getter for the accountCode
     * @return the accountCode
     */

    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Setter for the accountCode
     * @param accountCode the new value
     */
    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    /**
     * Getter for the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter for the username
     * @param username the new value
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter for the offline flag
     * @return true if offline tracking is on
     */
    public boolean isOffline() {
        return offline;
    }

    /**
     * Setter for the offline flag
     * @param offline the new value
     */
    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    /**
     * Getter for the parseHls flag
     * @return true if parseHls is enabled
     */
    public boolean isParseHls() {
        return parseHls;
    }

    /**
     * Setter for the parseHls
     * @param parseHls the new value
     */
    public void setParseHls(boolean parseHls) {
        this.parseHls = parseHls;
    }

    /**
     * Getter for the parseCdnNameHeader
     * @return the parseCdnNameHeader
     */
    public String getParseCdnNameHeader() {
        return parseCdnNameHeader;
    }

    /**
     * Setter for the parseCdnNameHeader
     * @param parseCdnNameHeader the new value
     */
    public void setParseCdnNameHeader(String parseCdnNameHeader) {
        this.parseCdnNameHeader = parseCdnNameHeader;
    }

    /**
     * Getter for the parseCdnNode flag
     * @return true if parseCdnNode is enabled
     */
    public boolean isParseCdnNode() {
        return parseCdnNode;
    }

    /**
     * Setter for the parseCdnNode
     * @param parseCdnNode the new value
     */
    public void setParseCdnNode(boolean parseCdnNode) {
        this.parseCdnNode = parseCdnNode;
    }

    /**
     * Getter for the parseCdnNodeList list
     * @return the Cdns to parse
     */
    public ArrayList<String> getParseCdnNodeList() {
        return parseCdnNodeList;
    }

    /**
     * Setter for the parseCdnNodeList list
     * @param parseCdnNodeList the new list
     */
    public void setParseCdnNodeList(ArrayList<String> parseCdnNodeList) {
        this.parseCdnNodeList = parseCdnNodeList;
    }

    /**
     * Getter for the networkIP
     * @return the networkIP
     */
    public String getNetworkIP() {
        return networkIP;
    }

    /**
     * Setter for the networkIP
     * @param networkIP the new value
     */
    public void setNetworkIP(String networkIP) {
        this.networkIP = networkIP;
    }

    /**
     * Getter for the networkIsp
     * @return the networkIsp
     */
    public String getNetworkIsp() {
        return networkIsp;
    }

    /**
     * Setter for the networkIsp
     * @param networkIsp the new value
     */
    public void setNetworkIsp(String networkIsp) {
        this.networkIsp = networkIsp;
    }

    /**
     * Getter for the networkConnectionType
     * @return the networkConnectionType
     */
    public String getNetworkConnectionType() {
        return networkConnectionType;
    }

    /**
     * Setter for the networkConnectionType
     * @param networkConnectionType the new value
     */
    public void setNetworkConnectionType(String networkConnectionType) {
        this.networkConnectionType = networkConnectionType;
    }

    /**
     * Getter for the deviceCode
     * @return the deviceCode
     */
    public String getDeviceCode() {
        return deviceCode;
    }

    /**
     * Setter for the deviceCode
     * @param deviceCode the new value
     */
    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    /**
     * Getter for the contentResource
     * @return the contentResource
     */
    public String getContentResource() {
        return contentResource;
    }

    /**
     * Setter for the contentResource
     * @param contentResource the new value
     */
    public void setContentResource(String contentResource) {
        this.contentResource = contentResource;
    }

    /**
     * Getter for the contentIsLive flag
     * @return the contentIsLive
     */
    public Boolean getContentIsLive() {
        return contentIsLive;
    }

    /**
     * Setter for the contentIsLive
     * @param contentIsLive the new value
     */
    public void setContentIsLive(Boolean contentIsLive) {
        this.contentIsLive = contentIsLive;
    }

    /**
     * Getter for the contentTitle
     * @return the contentTitle
     */
    public String getContentTitle() {
        return contentTitle;
    }

    /**
     * Setter for the contentTitle
     * @param contentTitle the new value
     */
    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    /**
     * Getter for the contentTitle2
     * @return the contentTitle2
     */
    public String getContentTitle2() {
        return contentTitle2;
    }

    /**
     * Setter for the contentTitle2
     * @param contentTitle2 the new value
     */
    public void setContentTitle2(String contentTitle2) {
        this.contentTitle2 = contentTitle2;
    }

    /**
     * Getter for the contentDuration
     * @return the contentDuration
     */
    public Double getContentDuration() {
        return contentDuration;
    }

    /**
     * Setter for the contentDuration
     * @param contentDuration the new value
     */
    public void setContentDuration(Double contentDuration) {
        this.contentDuration = contentDuration;
    }

    /**
     * Getter for the contentTransactionCode
     * @return the contentTransactionCode
     */
    public String getContentTransactionCode() {
        return contentTransactionCode;
    }

    /**
     * Setter for the contentTransactionCode
     * @param contentTransactionCode the new value
     */
    public void setContentTransactionCode(String contentTransactionCode) {
        this.contentTransactionCode = contentTransactionCode;
    }

    /**
     * Getter for the contentBitrate
     * @return the contentBitrate
     */
    public Long getContentBitrate() {
        return contentBitrate;
    }

    /**
     * Setter for the contentBitrate
     * @param contentBitrate the new value
     */
    public void setContentBitrate(Long contentBitrate) {
        this.contentBitrate = contentBitrate;
    }

    /**
     * Getter for the contentThroughput
     * @return the contentThroughput
     */
    public Long getContentThroughput() {
        return contentThroughput;
    }

    /**
     * Setter for the contentThroughput
     * @param contentThroughput the new value
     */
    public void setContentThroughput(Long contentThroughput) {
        this.contentThroughput = contentThroughput;
    }

    /**
     * Getter for the contentRendition
     * @return the contentRendition
     */
    public String getContentRendition() {
        return contentRendition;
    }

    /**
     * Setter for the contentRendition
     * @param contentRendition the new value
     */
    public void setContentRendition(String contentRendition) {
        this.contentRendition = contentRendition;
    }

    /**
     * Getter for the contentCdn
     * @return the contentCdn
     */
    public String getContentCdn() {
        return contentCdn;
    }

    /**
     * Setter for the contentCdn
     * @param contentCdn the new value
     */
    public void setContentCdn(String contentCdn) {
        this.contentCdn = contentCdn;
    }

    /**
     * Getter for the contentFps
     * @return the contentFps
     */
    public Double getContentFps() {
        return contentFps;
    }

    /**
     * Setter for the contentFps
     * @param contentFps the new value
     */
    public void setContentFps(Double contentFps) {
        this.contentFps = contentFps;
    }

    /**
     * Getter for the contentMetadata
     * @return the contentMetadata
     */
    public Bundle getContentMetadata() {
        return contentMetadata;
    }

    /**
     * Setter for the contentMetadata
     * @param contentMetadata the new value
     */
    public void setContentMetadata(Bundle contentMetadata) {
        this.contentMetadata = contentMetadata;
    }

    /**
     * Getter for the adMetadata
     * @return the adMetadata
     */
    public Bundle getAdMetadata() {
        return adMetadata;
    }

    /**
     * Setter for the adMetadata
     * @param adMetadata the new value
     */
    public void setAdMetadata(Bundle adMetadata) {
        this.adMetadata = adMetadata;
    }

    /**
     * Getter for the ignoreAds
     * @return the ignoreAds
     */
    public boolean getAdIgnore() {
        return ignoreAds;
    }

    /**
     * Setter for the ignoreAds
     * @param ignoreAds the new value
     */
    public void setAdIgnore(Boolean ignoreAds) {
        this.ignoreAds = ignoreAds;
    }

    /**
     * Getter for adAfterStop
     * @return the adAfterStop
     */
    public int getAdsAfterStop(){
        return adsAfterStop;
    }

    /**
     * Setter for adAfterStop
     * @param adAfterStop
     */
    public void setAdsAfterStop(int adAfterStop){
        this.adsAfterStop = adAfterStop;
    }

    /**
     * Setter for the auto background detection
     * @param autoDetectBackground the new value
     */
    public void setAutoDetectBackground(boolean autoDetectBackground){
        this.autoDetectBackground = autoDetectBackground;
    }

    /**
     * Getter for autoDetectBackground
     * @return autoBackground flag
     */
    public boolean isAutoDetectBackground(){
        return autoDetectBackground;
    }

    /**
     * Setter for the auto start feature
     * @param autoStart the new value
     */
    public void setAutoStart(boolean autoStart){
        this.autoStart = autoStart;
    }

    /**
     * Getter for autoStart
     * @return autoStart flag
     */
    public boolean isAutoStart(){
        return autoStart;
    }

    /**
     * Getter for the extraparam1
     * @return the extraparam1
     */
    public String getExtraparam1() {
        return extraparam1;
    }

    /**
     * Setter for the extraparam1
     * @param extraparam1 the new value
     */
    public void setExtraparam1(String extraparam1) {
        this.extraparam1 = extraparam1;
    }

    /**
     * Getter for the extraparam2
     * @return the extraparam2
     */
    public String getExtraparam2() {
        return extraparam2;
    }

    /**
     * Setter for the extraparam2
     * @param extraparam2 the new value
     */
    public void setExtraparam2(String extraparam2) {
        this.extraparam2 = extraparam2;
    }

    /**
     * Getter for the extraparam3
     * @return the extraparam3
     */
    public String getExtraparam3() {
        return extraparam3;
    }

    /**
     * Setter for the extraparam3
     * @param extraparam3 the new value
     */
    public void setExtraparam3(String extraparam3) {
        this.extraparam3 = extraparam3;
    }

    /**
     * Getter for the extraparam4
     * @return the extraparam4
     */
    public String getExtraparam4() {
        return extraparam4;
    }

    /**
     * Setter for the extraparam4
     * @param extraparam4 the new value
     */
    public void setExtraparam4(String extraparam4) {
        this.extraparam4 = extraparam4;
    }

    /**
     * Getter for the extraparam5
     * @return the extraparam5
     */
    public String getExtraparam5() {
        return extraparam5;
    }

    /**
     * Setter for the extraparam5
     * @param extraparam5 the new value
     */
    public void setExtraparam5(String extraparam5) {
        this.extraparam5 = extraparam5;
    }

    /**
     * Getter for the extraparam6
     * @return the extraparam6
     */
    public String getExtraparam6() {
        return extraparam6;
    }

    /**
     * Setter for the extraparam6
     * @param extraparam6 the new value
     */
    public void setExtraparam6(String extraparam6) {
        this.extraparam6 = extraparam6;
    }

    /**
     * Getter for the extraparam7
     * @return the extraparam7
     */
    public String getExtraparam7() {
        return extraparam7;
    }

    /**
     * Setter for the extraparam7
     * @param extraparam7 the new value
     */
    public void setExtraparam7(String extraparam7) {
        this.extraparam7 = extraparam7;
    }

    /**
     * Getter for the extraparam8
     * @return the extraparam8
     */
    public String getExtraparam8() {
        return extraparam8;
    }

    /**
     * Setter for the extraparam8
     * @param extraparam8 the new value
     */
    public void setExtraparam8(String extraparam8) {
        this.extraparam8 = extraparam8;
    }

    /**
     * Getter for the extraparam9
     * @return the extraparam9
     */
    public String getExtraparam9() {
        return extraparam9;
    }

    /**
     * Setter for the extraparam9
     * @param extraparam9 the new value
     */
    public void setExtraparam9(String extraparam9) {
        this.extraparam9 = extraparam9;
    }

    /**
     * Getter for the extraparam10
     * @return the extraparam10
     */
    public String getExtraparam10() {
        return extraparam10;
    }

    /**
     * Setter for the extraparam10
     * @param extraparam10 the new value
     */
    public void setExtraparam10(String extraparam10) {
        this.extraparam10 = extraparam10;
    }
}
