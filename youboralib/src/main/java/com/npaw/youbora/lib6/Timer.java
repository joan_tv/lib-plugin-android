package com.npaw.youbora.lib6;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

/**
 * An Utility class that provides timed events in a defined time interval.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Timer {

    /** List of {@link TimerEventListener}. */
    private List<TimerEventListener> callbacks;

    /** The period at which to execute the callbacks. */
    private long interval;

    /**
     * {@link Handler} to post the delayed execution of the {@link #callbacks}.
     *  This handler is created when the Timer is created, ensuring that the callback will be
     *  executed on the same thread that created the Timer.
     */
    private Handler handler;

    /** Chrono to inform the callback how much time has passed since the previous call. */
    private Chrono chrono;

    /** Whether the Timer is running or not. */
    private boolean isRunning;

    /**
     * Runnable to post in the {@link #handler}.
     */
    private final Runnable tickRunnable = new Runnable() {
        @Override
        public void run() {

            long delta = chrono.getDeltaTime();

            Chrono c = new Chrono();
            c.start();

            if (callbacks != null) {
                for (TimerEventListener listener : callbacks) {
                    listener.onTimerEvent(delta);
                }
            }

            c.stop();
            setNextTick();
        }
    };

    /**
     * Constructor.
     * Same as calling {@link #Timer(TimerEventListener, long)} with interval = 5000
     * @param callback The callback to execute periodically
     */
    public Timer(TimerEventListener callback) {
        this(callback, 5000);
    }

    /**
     * Constructor
     * @param callback The callback to execute periodically
     * @param interval Period at which the calllback will be called
     */
    public Timer(TimerEventListener callback, long interval) {
        addTimerCallback(callback);
        this.interval = interval;
        chrono = new Chrono();
        handler = new Handler();
        isRunning = false;
    }

    /**
     * Getter for isRunning property.
     * @return true if the {@link Timer} is currently running
     */
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * Getter for chrono
     * @return the Chrono instance associated with this one
     */
    public Chrono getChrono() {
        return chrono;
    }

    /**
     * Starts the timer.
     */
    public void start() {
        if (!isRunning) {
            isRunning = true;
            setNextTick();

            YouboraLog.notice("Timer started: every " + interval + " ms");
        }
    }

    /**
     * Stops the timer.
     */
    public void stop() {
        if (isRunning) {
            isRunning = false;
            handler.removeCallbacks(tickRunnable);
        }
    }

    /**
     * Schedules the {@link #handler} to run the {@link #tickRunnable} in {@link #interval} milliseconds.
     */
    private void setNextTick() {
        if (isRunning) {
            chrono.start();
            handler.postDelayed(tickRunnable, interval);
        }
    }

    /**
     * Add a {@link TimerEventListener} to the callback list. Its {@link TimerEventListener#onTimerEvent(long)}
     * will be called every {@link #interval} seconds.
     * @param listener The {@link TimerEventListener} to add
     */
    public void addTimerCallback(TimerEventListener listener) {
        if (callbacks == null) {
            callbacks = new ArrayList<>(1);
        }
        callbacks.add(listener);
    }

    /**
     * Updates the interval at which the {@link TimerEventListener} is called.
     * @param interval new interval in milliseconds
     */
    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    /**
     * Interface defined so any class willing to become a listener to Timer class events can add
     * custom behaviour.
     * @author      Nice People at Work
     * @since       6.0
     */
    public interface TimerEventListener {
        /**
         * Method called periodically by the {@link Timer} class every {@link Timer#interval}
         * seconds.
         * @param delta a time delta (difference between two timestamps) in milliseconds.
         */
        void onTimerEvent(long delta);
    }
}
