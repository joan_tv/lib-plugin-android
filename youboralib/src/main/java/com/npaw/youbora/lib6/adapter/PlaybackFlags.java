package com.npaw.youbora.lib6.adapter;

/**
 * This class contains all the flags related to view status.
 * Each {@link com.npaw.youbora.lib6.plugin.Plugin} will have an instance of this class.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class PlaybackFlags {

    private boolean preloading;
    private boolean started;
    private boolean joined;
    private boolean paused;
    private boolean seeking;
    private boolean buffering;
    private boolean ended;
    private boolean stopped;

    //Only for ads
    private boolean adInitiated;

    public PlaybackFlags() {
        reset();
    }

    /**
     * Reset all flag values to false.
     */
    public void reset() {
        preloading = false;
        started = false;
        joined = false;
        paused = false;
        seeking = false;
        buffering = false;
        ended = false;
        stopped = false;

        adInitiated = false;
    }

    /**
     * Returns the preloading flag.
     * @return the preloading flag.
     */
    public boolean isPreloading() {
        return preloading;
    }

    /**
     * Sets the preloading flag.
     * @param preloading new value
     */
    public void setPreloading(boolean preloading) {
        this.preloading = preloading;
    }

    /**
     * Returns the started flag.
     * @return the started flag.
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Sets the started flag.
     * @param started new value
     */
    public void setStarted(boolean started) {
        this.started = started;
    }

    /**
     * Returns the adInitiated flag.
     * @return the adInitiated flag.
     */
    public boolean isAdInitiated() {
        return adInitiated;
    }

    /**
     * Sets the adInitiated flag.
     * @param adInitiated new value
     */
    public void setAdInitiated(boolean adInitiated) {
        this.adInitiated = adInitiated;
    }

    /**
     * Returns the joined flag.
     * @return the joined flag.
     */
    public boolean isJoined() {
        return joined;
    }

    /**
     * Sets the joined flag.
     * @param joined new value
     */
    public void setJoined(boolean joined) {
        this.joined = joined;
    }

    /**
     * Returns the paused flag.
     * @return the paused flag.
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     * Sets the paused flag.
     * @param paused new value
     */
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    /**
     * Returns the seeking flag.
     * @return the seeking flag.
     */
    public boolean isSeeking() {
        return seeking;
    }

    /**
     * Sets the seeking flag.
     * @param seeking new value
     */
    public void setSeeking(boolean seeking) {
        this.seeking = seeking;
    }

    /**
     * Returns the buffering flag.
     * @return the buffering flag.
     */
    public boolean isBuffering() {
        return buffering;
    }

    /**
     * Sets the buffering flag.
     * @param buffering new value
     */
    public void setBuffering(boolean buffering) {
        this.buffering = buffering;
    }

    /**
     * Returns the stopped flag.
     * @return the stopped flag.
     */
    public boolean isStopped(){
        return stopped;
    }

    /**
     * Sets the stopped flag.
     * @param stopped new value
     */
    public void setStopped(boolean stopped){
        this.stopped = stopped;
    }

    /**
     * Returns the ended flag.
     * @return the ended flag.
     */
    public boolean isEnded(){
        return ended;
    }

    /**
     * Sets the ended flag.
     * @param ended flag
     */
    public void setEnded(boolean ended){
        this.ended = ended;
    }

}
