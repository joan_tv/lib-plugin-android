package com.npaw.youbora.lib6;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * An Utility class with static methods.
 * @author      Nice People at Work
 * @since       6.0
 */
public class YouboraUtil {

    /**
     * Pattern used to extract an URL's protocol.
     */
    private static Pattern stripProtocolPattern;


    /**
     * Preference name where to save offline events
     */
    public static String PREF_OFFLINE_EVENTS = "youbora_offline";

    /**
     * Preference file name where to save offline events
     */
    public static String PREF_YOUBORA = "youbora";

    /**
     * Saving the application context for later usage
     */
    public static Context context;

    /**
     * Builds a string that represents the rendition.
     *
     * The returned string will have the following format: [width]x[height]@[bitrate][suffix].
     * If either the width or height are &lt; 1, only the bitrate will be returned.
     * If bitrate is &lt; 1, only the dimensions will be returned.
     * If bitrate is &lt; and there is no dimensions, a null will be returned.
     * The bitrate will also have one of the following suffixes depending on its
     * magnitude: bps, Kbps, Mbps
     *
     * @param width The width of the asset.
     * @param height The height of the asset.
     * @param bitrate The indicated bitrate (in the manifest) of the asset.
     * @return A string with the following format: [width]x[height]@[bitrate][suffix]
     */
    public static String buildRenditionString(int width, int height, double bitrate){

        StringBuilder sb = new StringBuilder("");

        if (width > 0 && height > 0) {
            sb.append(Integer.toString(width)).append("x").append(Integer.toString(height));
            if (bitrate > 0) {
                sb.append("@");
            }
        }

        if (bitrate > 0) {
            if (bitrate < 1e3) {        // Smaller than 1Kbps
                sb.append(String.format(Locale.US, "%.0fbps", bitrate));
            } else if (bitrate < 1e6) { // Smaller than 1Mbps
                sb.append(String.format(Locale.US, "%.0fKbps", bitrate/1e3));
            } else {                    // Greater than 1Mbps
                sb.append(String.format(Locale.US, "%.2fMbps", bitrate/1e6));
            }
        }

        return sb.toString();
    }

    /**
     * Returns the JSON representation of a Map.
     * @param map Map to stringify
     * @return the JSON representation of a Map. If the map is null, null will be returned.
     */
    public static String stringifyMap(Map<String, ?> map) {
        if (map == null) {
            return null;
        } else {
            return new JSONObject(map).toString();
        }
    }

    /**
     * Returns the JSON representation of a Bundle.
     * @param b Bundle to stringify
     * @return the JSON representation of a Bundle. If the Bundle is null, null will be returned.
     */
    public static String stringifyBundle(Bundle b) {

        JSONObject json = bundleToJSON(b);

        if (json != null) {
            return json.toString();
        } else {
            return null;
        }
    }

    /**
     * Convert a Bundle to a JSONObject.
     * @param b the Bundle to convert
     * @return JSONObject with the Bundle structure if Bundle is not null.
     */
    public static JSONObject bundleToJSON(Bundle b) {
        JSONObject json = null;

        if (b != null) {
            json = new JSONObject();
            for (String key : b.keySet()) {
                Object value = b.get(key);
                if (value != null) {
                    if (value instanceof Bundle) {
                        // If the value is a Bundle, recursive call
                        value = bundleToJSON((Bundle) value);
                    } else if (value instanceof Map) {
                        value = new JSONObject((Map) value);
                    } else if (value instanceof List) {
                        value = new JSONArray((List) value);
                    } else if (value.getClass().isArray()) {
                        // Use a map as wrapper, convert to json and extract json array
                        Map<String, Object> m = new HashMap<>(1);
                        m.put("k", value);
                        try {
                            value = new JSONObject(m).getJSONArray("k");
                        } catch (JSONException e) {
                            YouboraLog.error(e);
                        }
                    }

                    try {
                        json.put(key, value);
                    } catch (JSONException e) {
                        YouboraLog.error(e);
                    }
                }
            }
        }

        return json;
    }

    /**
     * Strip [protocol]:// from the beginning of the string.
     * @param host Url
     * @return stripped url
     */
    public static String stripProtocol(String host) {

        if (host == null) {
            return null;
        }

        if (stripProtocolPattern == null) {
            stripProtocolPattern = Pattern.compile("^(.*?://|//)", Pattern.CASE_INSENSITIVE);
        }

        return stripProtocolPattern.matcher(host).replaceFirst("");
    }

    /**
     * Adds specific protocol. ie: [http[s]:]//nqs.nice264.com
     * @param url Domain of the service.
     * @param httpSecure If true will add https, if false http.
     * @return Return the complete service URL.
     */
    public static String addProtocol(String url, boolean httpSecure) {

        if (url == null) {
            url = "";
        }

        if (httpSecure) {
            return "https://" + url;
        } else {
            return "http://" + url;
        }
    }

    /**
     * Return {@code number} if it's not null, {@link Double#MAX_VALUE}, {@link Double#POSITIVE_INFINITY}
     * or {@link Double#NaN}. Otherwise, {@code defaultValue} will be returned.
     * @param number The number to be parsed
     * @param defaultValue Number to return if {@code number} is 'incorrect'
     * @return number if it's a 'real' value, {@code defaultValue} otherwise
     */
    public static Double parseNumber(Double number, Double defaultValue) {

        Double returnValue = defaultValue;

        if (number != null) {
            double d = Math.abs(number);
            if (d != Double.MAX_VALUE && !Double.isInfinite(d) && !Double.isNaN(d)) {
                returnValue = number;
            }
        }

        return returnValue;
    }

    /**
     * Return {@code number} if it's not null, {@link Integer#MAX_VALUE} or {@link Integer#MIN_VALUE}.
     * Otherwise, {@code defaultValue} will be returned.
     * @param number The number to be parsed
     * @param defaultValue Number to return if {@code number} is 'incorrect'
     * @return number if it's a 'real' value, {@code defaultValue} otherwise
     */
    public static Integer parseNumber(Integer number, Integer defaultValue) {

        Integer returnValue = defaultValue;

        if (number != null) {
            int i = number;
            if (i != Integer.MAX_VALUE && i != Integer.MIN_VALUE) {
                returnValue = number;
            }
        }

        return returnValue;
    }

    /**
     * Return {@code number} if it's not null, {@link Long#MAX_VALUE} or {@link Long#MIN_VALUE}.
     * Otherwise, {@code defaultValue} will be returned.
     * @param number The number to be parsed
     * @param defaultValue Number to return if {@code number} is 'incorrect'
     * @return number if it's a 'real' value, {@code defaultValue} otherwise
     */
    public static Long parseNumber(Long number, Long defaultValue) {

        Long returnValue = defaultValue;

        if (number != null) {
            long i = number;
            if (i != Long.MAX_VALUE && i != Long.MIN_VALUE) {
                returnValue = number;
            }
        }

        return returnValue;
    }

    /**
     * Returns a params dictionary with the error values.
     *
     * @param params Map of pre filled params or null. If this is not empty nor null, nothing will be done.
     * @return Built params
     */
    public static Map<String, String> buildErrorParams(Map<String, String> params) {

        if (params == null) {
            params = new HashMap<>();
        }

        //if (params.get("errorLevel") == null) params.put("errorLevel", "error");

        return params;
    }

    /**
     * Returns a params dictionary with the error values.
     *
     * @param msg Error Message
     * @param code Error code
     * @param errorMetadata additional error info
     * @param level Level of the error. Currently supports 'error' and 'fatal'
     * @return Built params
     */
    public static Map<String, String> buildErrorParams(String msg, String code, String errorMetadata, String level) {

        HashMap<String, String> params = new HashMap<>();

        boolean codeOk = code != null && code.length() > 0;
        boolean msgOk = msg != null && msg.length() > 0;

        if (codeOk) {
            if (!msgOk) {
                msg = code;
            }
        } else if (msgOk){
            code = msg;
        } else {
            code = msg = "PLAY_FAILURE";
        }

        params.put("errorCode", code);
        params.put("errorMsg", msg);
        if (errorMetadata != null && errorMetadata.length() > 0) params.put("errorMetadata", errorMetadata);
        if (level != null && level.length() > 0) {
            params.put("errorLevel", level);
        } /*else {
            params.put("errorLevel", "error");
        }*/

        return params;
    }

    /**
     * Convinience method just to encode a string
     *
     * @param value String to encode
     * @return Encoded string
     */
    public static String encodeBase64(String value) throws UnsupportedEncodingException {
        byte[] data = value.getBytes("UTF-8");
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    /**
     * Convinience method just to decode a string
     *
     * @param value String to decode
     * @return Decoded string
     */
    public static String decodeBase64(String value) throws UnsupportedEncodingException {
        byte[] data = Base64.decode(value, Base64.DEFAULT);
        return new String(data, "UTF-8");
    }

    /**
     * Method to retrieve saved offline events
     * @return Events in json format but as String
     */
    public static String retrieveOfflineEvents() throws UnsupportedEncodingException {
        SharedPreferences sharedPref = context.getSharedPreferences(YouboraUtil.PREF_YOUBORA, Context.MODE_PRIVATE);
        return YouboraUtil.decodeBase64(sharedPref.getString(YouboraUtil.PREF_OFFLINE_EVENTS,""));
    }

    public static void deleteOfflineEvents() {
        SharedPreferences sharedPref = context.getSharedPreferences(YouboraUtil.PREF_YOUBORA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(YouboraUtil.PREF_OFFLINE_EVENTS, "");
        editor.commit();
    }
}
