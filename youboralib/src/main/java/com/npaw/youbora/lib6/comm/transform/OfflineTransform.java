package com.npaw.youbora.lib6.comm.transform;

import android.content.Context;
import android.content.SharedPreferences;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.Request;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by Enrique on 20/07/2017.
 */

public class OfflineTransform extends Transform {

    public OfflineTransform() {
        sendRequest = false;
        isBusy = false;
    }

    @Override
    public void parse(Request request) {
        if (request != null) {
            Map<String,Object> messageData = request.getParams();
            messageData.put("request",request.getService().substring(1));
            messageData.put("unixtime",System.currentTimeMillis());
            JSONObject json = new JSONObject(messageData);
            YouboraLog.debug(String.format("Offline JSON: %s", json.toString()));
            if(YouboraUtil.context != null){
                try {
                    appendOfflineToPrefs(json.toString(), request.getService().substring(1), YouboraUtil.context);
                } catch (UnsupportedEncodingException e) {
                    YouboraLog.error(e.toString());
                }
            }else{
                YouboraLog.error("The context is null, if you are using offline mode set it with the plugin.setContext function");
            }
        }
    }

    @Override
    public boolean hasToSend(Request request) {
        return false;
    }

    @Override
    public int getState() {
        return Transform.STATE_OFFLINE;
    }

    /**
     * Appends the new offline event to the existing ones
     *
     * @param value New event to append
     * @param service
     * @param context Activity context
     */
    private void appendOfflineToPrefs(String value, String service, Context context) throws UnsupportedEncodingException {

        if(service.equals(Constants.SERVICE_INIT.substring(1))){
            return;
        }

        SharedPreferences sharedPref = context.getSharedPreferences(YouboraUtil.PREF_YOUBORA, Context.MODE_PRIVATE);
        String currentString = YouboraUtil.decodeBase64(sharedPref.getString(YouboraUtil.PREF_OFFLINE_EVENTS,""));
        if(currentString.equals("")){
            value = "\"events\":[[" + value;
        }else{
            if(service.equals("start")){
                value = "[" + value;
            }
        }
        if(service.equals("stop")){
            value += "]";
        }
        SharedPreferences.Editor editor = sharedPref.edit();
        if(currentString.equals("")){
            editor.putString(YouboraUtil.PREF_OFFLINE_EVENTS, YouboraUtil.encodeBase64(value));
        }else{
            editor.putString(YouboraUtil.PREF_OFFLINE_EVENTS, YouboraUtil.encodeBase64(String.format("%s,%s",currentString,value)));
        }
        editor.commit();
    }
}
