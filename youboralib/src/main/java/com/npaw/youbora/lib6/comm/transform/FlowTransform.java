package com.npaw.youbora.lib6.comm.transform;


import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

import static com.npaw.youbora.lib6.Constants.SERVICE_ERROR;
import static com.npaw.youbora.lib6.Constants.SERVICE_INIT;
import static com.npaw.youbora.lib6.Constants.SERVICE_OFFLINE_EVENTS;
import static com.npaw.youbora.lib6.Constants.SERVICE_START;

/**
 * This transform ensures that no requests will be sent before an /init or /start request.
 * As these are the two possible first requests that the API expects for a view.
 * <a href="http://developer.nicepeopleatwork.com/apidocs/swagger/html/?module=nqs7">API docs</a>.
 * @author      Nice People at Work
 * @since       6.0
 */
public class FlowTransform extends Transform {

    private static final List<String> EXPECTED_SERVICES = new ArrayList<String>(2){{
        add(SERVICE_INIT);
        add(SERVICE_START);
        add(SERVICE_OFFLINE_EVENTS);
    }};

    @Override
    public void parse(Request request) {
        // This Transform does not need to alter the Requests, so do nothing here.
    }

    /**
     * {@inheritDoc}
     *
     * Blocks all the requests until an /init or a /start is found.
     * The only exception to this logic is the /error, that can be sent at any time
     */
    @Override
    public boolean isBlocking(Request request) {
        if (isBusy && request != null) {
            if (EXPECTED_SERVICES.contains(request.getService())) {
                isBusy = false;
            } else if (SERVICE_ERROR.equals(request.getService())){
                // If it's an error we make an exception and bypass the blocking
                return false;
            }
        }
        return super.isBlocking(request);
    }
}
