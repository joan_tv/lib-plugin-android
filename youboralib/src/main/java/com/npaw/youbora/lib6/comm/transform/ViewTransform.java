package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.npaw.youbora.lib6.Constants.SERVICE_DATA;
import static com.npaw.youbora.lib6.Constants.SERVICE_OFFLINE_EVENTS;
import static com.npaw.youbora.lib6.Constants.SERVICE_PING;
import static com.npaw.youbora.lib6.Constants.SERVICE_START;

/**
 * Manages Fastdata service interaction and view codes generation.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class ViewTransform extends Transform{

    private Request request;
    private Map<String, String> params;
    private int viewIndex;
    private Plugin plugin;
    private String viewCode = null;

    /**
     * Instance of {@link FastDataConfig}
     */
    public FastDataConfig fastDataConfig;

    public ViewTransform(Plugin plugin) {
        super();

        this.plugin = plugin;

        fastDataConfig = new FastDataConfig();
        viewIndex = -1;

        String service = SERVICE_DATA;
        params = new HashMap<>();
        params.put("apiVersion", "v6,v7");
        params.put("outputformat", "jsonp");
        params = plugin.getRequestBuilder().buildParams(params, service);
        if (params != null) {
            if ("nicetest".equals(params.get("system"))) {
                // "nicetest" is the default accountCode.
                // If found here, it's very likely that the customer has forgotten to set it.
                YouboraLog.error("No accountCode has been set. Please set your accountCode in plugin's options.");
            }

            // Prepare request but don't send it yet
            request = createRequest(plugin.getHost(), service);
            Map<String, Object> paramsObjectMap = new HashMap<>();
            paramsObjectMap.putAll(params);
            request.setParams(paramsObjectMap);
        }
    }

    /**
     * Starts the 'FastData' fetching. This will send the initial request to YOUBORA in order to get
     * the needed info for the rest of the requests.
     *
     * This is an asynchronous process.
     *
     * When the fetch is complete, {@link #fastDataConfig} will contain the parsed info.
     * @see FastDataConfig
     */
    public void init() {
        requestData();
    }

    private void requestData() {

        request.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String responseString) {

                if (responseString == null || responseString.length() == 0) {
                    YouboraLog.error("FastData empty response");
                    return;
                }
                try {
                    responseString = responseString.substring(7, responseString.length() - 1);

                    JSONObject outerJson = createJSONFromString(responseString);

                    if (outerJson.has("q")) {
                        JSONObject innerJson = outerJson.getJSONObject("q");

                        String host = "";
                        String code = "";
                        String pt = "";

                        if (innerJson.has("h")) {
                            host = innerJson.getString("h");
                        }

                        if (innerJson.has("c")) {
                            code = innerJson.getString("c");
                        }

                        if (innerJson.has("pt")) {
                            pt = innerJson.getString("pt");
                        }

                        if (host.length() > 0 && code.length() > 0 && pt.length() > 0) {

                            if (fastDataConfig == null){
                                fastDataConfig = new FastDataConfig();
                            }

                            fastDataConfig.code = code;
                            Options options = plugin.getOptions();
                            fastDataConfig.host = YouboraUtil.addProtocol(host, options != null && options.isHttpSecure());
                            fastDataConfig.pingTime = Integer.parseInt(pt);
                            if(options.isOffline()){
                                fastDataConfig.pingTime = 60;
                            }

                            buildCode();

                            YouboraLog.notice(String.format("FastData '%s' is ready.", code));

                            done();
                        } else {
                            YouboraLog.error("FastData response is wrong.");
                        }
                    } else {
                        YouboraLog.error("FastData response is wrong.");
                    }
                } catch (Exception e) {
                    YouboraLog.error("FastData response is wrong.");
                    YouboraLog.error(e);
                }
            }
        });

        request.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                YouboraLog.error("Fastdata request failed.");
            }
        });

        request.send();
    }

    /**
     * {@inheritDoc}
     *
     * ViewTransform will set the correct view code to each request and set the pingTime param
     * for the services that need to carry it.
     */
    @Override
    public void parse(Request request) {
        Map<String, Object> params = request.getParams();
        if (request.getHost() == null || request.getHost().length() == 0) {
            request.setHost(fastDataConfig.host);
        }

        if (params.get("code") == null) {
            params.put("code", getViewCode());
        }

        // Request-specific transforms
        switch (request.getService()) {
            case SERVICE_PING:
            case SERVICE_START:
                if (params.get("pingTime") == null) params.put("pingTime", fastDataConfig.pingTime);
                break;
            case SERVICE_OFFLINE_EVENTS:
                params.put("events",addCodeToEvents(params));
                break;
            default:
        }

    }

    private String addCodeToEvents(Map<String, Object> params) {
        JSONArray newJsonEvents = new JSONArray();
        try {
            JSONArray jsonEvents = new JSONArray((String) params.get("events"));
            JSONObject jsonObject;
            for(int k = 0 ; k < jsonEvents.length(); k++){
                jsonObject = new JSONObject(jsonEvents.get(k).toString());
                jsonObject.put("code",getViewCode());
                newJsonEvents.put(jsonObject);
            }
            return newJsonEvents.toString();
        } catch (JSONException e) {
            YouboraLog.error(e.toString());
        }
        return null;
    }

    JSONObject createJSONFromString(String string) throws JSONException {
        return new JSONObject(string);
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }

    /**
     * Increments the view counter and generates a new view code.
     * @return the new view code
     */
    public String nextView() {
        viewIndex++;
        buildCode();
        return getViewCode();
    }

    /**
     * Builds the view code. It has the following scheme:
     * [fast data response code]_[view count]
     *
     * The only thing that matters is that view codes are unique. For this reason we only ask the
     * backend only once for a code, and then append a view counter that is incremented with each
     * view.
     * @see {@link #nextView()}
     */
    private void buildCode() {
        if (fastDataConfig.code != null && fastDataConfig.code.length() > 0) {
            viewCode = fastDataConfig.code + "_" + Integer.toString(viewIndex);
        } else {
            viewCode = null;
        }
    }

    /**
     * Current view code
     * @return the current view code
     */
    private String getViewCode() {
        return viewCode;
    }

    /**
     * Container class that has all the info we need from YOUBORA's 'FastData' service.
     */
    public static class FastDataConfig {

        /**
         * The YOUBORA host where to send traces
         */
        public String host;

        /**
         * Unique view identifier.
         */
        public String code;

        /**
         * Ping time; how often should pings be reported. This is per-account configurable
         * although 99% of the time this is 5 seconds.
         */
        public Integer pingTime;
    }
}
