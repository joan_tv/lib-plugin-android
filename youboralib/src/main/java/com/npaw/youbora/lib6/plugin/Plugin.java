package com.npaw.youbora.lib6.plugin;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Communication;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.FlowTransform;
import com.npaw.youbora.lib6.comm.transform.Nqs6Transform;
import com.npaw.youbora.lib6.comm.transform.OfflineTransform;
import com.npaw.youbora.lib6.comm.transform.ResourceTransform;
import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.comm.transform.ViewTransform;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.npaw.youbora.lib6.Constants.*;

/**
 * This is the main class of video analytics. You may want one instance for each video you want
 * to track. Will need {@link PlayerAdapter}s for both content and ads, manage options and general flow.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Plugin {

    private ResourceTransform resourceTransform;
    private ViewTransform viewTransform;
    private RequestBuilder requestBuilder;
    private Timer pingTimer;
    private Options options;
    private PlayerAdapter adapter;
    private PlayerAdapter adsAdapter;
    private String lastServiceSent;

    /** Activity reference just to check that the events come from our activity **/
    private Activity activity;
    /** Reference to the activity callbacks **/
    private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;

    /** Indicates that /init has been called. */
    private boolean isInitiated;
    /** Indicates that the content is preloading. */
    private boolean isPreloading;
    /** Chrono for preload times. */
    private Chrono preloadChrono;
    /** Chrono for init to join time. */
    private Chrono initChrono;

    /**
     * Instance of {@link Communication} used to send {@link Request}s.
     */
    public Communication comm;

    // "Will send" Listeners
    private List<WillSendRequestListener> willSendInitListeners;
    private List<WillSendRequestListener> willSendStartListeners;
    private List<WillSendRequestListener> willSendJoinListeners;
    private List<WillSendRequestListener> willSendPauseListeners;
    private List<WillSendRequestListener> willSendResumeListeners;
    private List<WillSendRequestListener> willSendSeekListeners;
    private List<WillSendRequestListener> willSendBufferListeners;
    private List<WillSendRequestListener> willSendErrorListeners;
    private List<WillSendRequestListener> willSendStopListeners;
    private List<WillSendRequestListener> willSendPingListeners;

    private List<WillSendRequestListener> willSendAdInitListeners;
    private List<WillSendRequestListener> willSendAdStartListeners;
    private List<WillSendRequestListener> willSendAdJoinListeners;
    private List<WillSendRequestListener> willSendAdClickListeners;
    private List<WillSendRequestListener> willSendAdPauseListeners;
    private List<WillSendRequestListener> willSendAdResumeListeners;
    private List<WillSendRequestListener> willSendAdBufferListeners;
    private List<WillSendRequestListener> willSendAdStopListeners;

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter)} with a null adapter
     * @param options instance of {@link Options}
     */
    public Plugin(Options options) {
        this(options, null);
    }

    /**
     * Constructor
     * @param options instance of {@link Options}
     * @param adapter instance of a {@link PlayerAdapter}. Can also be specified afterwards with
     * {@link #setAdapter(PlayerAdapter)}.
     */
    public Plugin(Options options, PlayerAdapter adapter) {

        if (options == null) {
            YouboraLog.warn("Options is null");
            options = createOptions();
        }

        isInitiated = false;

        isPreloading = false;

        preloadChrono = createChrono();

        initChrono = createChrono();

        this.options = options;

        if (adapter != null) {
            setAdapter(adapter);
        }
        pingTimer = createTimer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {
                sendPing(delta);
            }
        }, 5000);

        requestBuilder = createRequestBuilder(this);

        resourceTransform = createResourceTransform(this);

        viewTransform = createViewTransform(this);

        viewTransform.addTransformDoneListener(new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {
                pingTimer.setInterval(viewTransform.fastDataConfig.pingTime * 1000);
            }
        });

        viewTransform.init();

        lastServiceSent = null;


    }

    /**
     * Resets plugin to initial state
     */
    private void reset() {
        stopPings();

        resourceTransform = createResourceTransform(this);

        isInitiated = false;
        isPreloading = false;
        initChrono.reset();
        preloadChrono.reset();
    }

    /**
     * Returns the {@link ViewTransform} instance used by the Plugin
     * @return the ViewTransform instance
     */
    public ViewTransform getViewTransform() {
        return viewTransform;
    }

    /**
     * Returns the {@link ResourceTransform} instance used by the Plugin
     * @return the ResourceTransform instance
     */
    public ResourceTransform getResourceTransform() {
        return resourceTransform;
    }

    // --------------------------------- Instance builders -----------------------------------------
    Chrono createChrono() {
        return new Chrono();
    }

    Options createOptions() {
        return new Options();
    }

    Timer createTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }

    RequestBuilder createRequestBuilder(Plugin plugin) {
        return new RequestBuilder(plugin);
    }

    ResourceTransform createResourceTransform(Plugin plugin) {
        return new ResourceTransform(plugin);
    }

    ViewTransform createViewTransform(Plugin plugin) {
        return new ViewTransform(plugin);
    }

    Communication createCommunication(){
        return new Communication();
    }

    Nqs6Transform createNqs6Transform() {
        return new Nqs6Transform();
    }

    OfflineTransform createOfflineTransform() {
        return new OfflineTransform();
    }

    FlowTransform createFlowTransform() {
        return new FlowTransform();
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }

    // ------------------------------------- Options ----------------------------------------------
    /**
     * Update the options. This will replace the whole Options object. If you want to change only
     * a few options call {@link #getOptions()} and modify the received object.
     * @param options The {@link Options} to set
     */
    public void setOptions(Options options) {
        this.options = options;
    }

    /**
     * Returns the current plugin Options.
     *
     * This method returns the actual options instance, so any changes performed on them will
     * automatically be reflected without the need to call {@link #setOptions(Options)}
     * @return the options
     */
    public Options getOptions() {
        return options;
    }

    // ------------------------------------- Adapters ----------------------------------------------
    /**
     * Sets an adapter for video content.
     *
     * @param adapter The adapter to set
     */
    public void setAdapter(PlayerAdapter adapter) {

        removeAdapter();

        if (adapter != null) {

            this.adapter = adapter;
            adapter.setPlugin(this);

            // Register listeners
            adapter.addEventListener(eventListener);

            registerForActivityCallbacks();

        } else {
            YouboraLog.error("Adapter is null in setAdapter");
        }
    }

    /**
     * Returns the current adapter or null if not set
     * @return adapter
     */
    public PlayerAdapter getAdapter() {
        return adapter;
    }

    /**
     * Removes the current adapter. Fires stop if needed. Calls {@link PlayerAdapter#dispose()}
     */
    public void removeAdapter() {
        if (adapter != null) {
            adapter.dispose();

            adapter.setPlugin(null);

            // Remove listeners
            adapter.removeEventListener(eventListener);

            unregisterForActivityCallbacks();

            adapter = null;
        }
    }

    /**
     * Sets an adapter for ads.
     *
     * @param adsAdapter adapter to set
     */
    public void setAdsAdapter(PlayerAdapter adsAdapter) {

        if (adsAdapter == null) {
            YouboraLog.error("Adapter is null in setAdsAdapter");
        } else if (adsAdapter.getPlugin() != null) {
            YouboraLog.warn("Adapters can only be added to a single plugin");
        } else {
            removeAdsAdapter();

            this.adsAdapter = adsAdapter;
            this.adsAdapter.setPlugin(this);

            adsAdapter.addEventListener(adEventListener);
        }
    }

    /**
     * Returns the current ads adapter or null
     * @return the current ads adapter or null
     */
    public PlayerAdapter getAdsAdapter() {
        return adsAdapter;
    }

    /**
     * Removes the current adapter. Fires stop if needed. Calls {@link PlayerAdapter#dispose()}
     */
    public void removeAdsAdapter() {
        if (adsAdapter != null) {
            adsAdapter.dispose();

            adsAdapter.setPlugin(null);

            adsAdapter.removeEventListener(adEventListener);

            adsAdapter = null;
        }
    }

    // -------------------------------------- DISABLE ----------------------------------------------
    /**
     * Disable request sending.
     * Same as calling {@link Options#setEnabled(boolean)} with false
     */
    public void disable() {
        options.setEnabled(false);
    }

    /**
     * Re-enable request sending.
     * Same as calling {@link Options#setEnabled(boolean)} with true
     */
    public void enable() {
        options.setEnabled(true);
    }

    // -------------------------------------- ACTIVITY CALLBACKS -----------------------------------

    /**
     * Registers the activity to its events for background
     */
    private void registerForActivityCallbacks() {
        if(getOptions().isAutoDetectBackground()){
            if(getActivity() != null && getAdapter() != null){
                final Activity finalAct = activity;
                activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
                    @Override
                    public void onActivityCreated(Activity activity, Bundle bundle) {

                    }

                    @Override
                    public void onActivityStarted(Activity activity) {

                    }

                    @Override
                    public void onActivityResumed(Activity activity) {

                    }

                    @Override
                    public void onActivityPaused(Activity activity) {

                    }

                    @Override
                    public void onActivityStopped(Activity activity) {
                        if(getAdapter() != null){
                            if(getAdapter().getFlags().isStarted() && finalAct == activity){
                                getAdapter().fireStop();
                            }
                        }
                    }

                    @Override
                    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

                    }

                    @Override
                    public void onActivityDestroyed(Activity activity) {

                    }
                };
                getActivity().getApplication().registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
            }else{
                YouboraLog.error("The plugin could not send stop, plugin.setActivity mus be called before setting the adapter");
            }
        }
    }

    /**
     * Unregisters the activity to its events for background
     */
    private void unregisterForActivityCallbacks() {
        if(getActivity() != null){
            getActivity().getApplication().unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);

            activityLifecycleCallbacks = null;
        }

    }

    // ------------------------------------- LISTENERS ---------------------------------------------
    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     */
    private void send(List<WillSendRequestListener> listenerList, String service, Map<String, String> params) {

        if (params == null) {
            params = new HashMap<>();
        }

        boolean isLive = getIsLive() == null ? false : getIsLive();
        boolean optionIsLive = getOptions().getContentIsLive() == null ? false : getOptions().getContentIsLive();

        /*if(isLive || optionIsLive){
            params.put("mediaDuration",null);
            params.put("playhead",null);
        }*/

        params = requestBuilder.buildParams(params, service);

        if (listenerList != null) {
            for (Plugin.WillSendRequestListener listener : listenerList) {
                try {
                    listener.willSendRequest(service, this, params);
                } catch (Exception e) {
                    YouboraLog.error("Exception while calling willSendRequest");
                    YouboraLog.error(e);
                }
            }
        }
        if (comm != null && params != null && options.isEnabled()) {

            Request r = createRequest(null, service);

            Map<String, Object> m = new HashMap<>();
            m.putAll(params);
            r.setParams(m);

            lastServiceSent = r.getService();

            Request.RequestSuccessListener sucessListener = null;

            if(service.equals(Constants.SERVICE_OFFLINE_EVENTS)){
                r.setMethod(Request.METHOD_POST);

                sucessListener = new Request.RequestSuccessListener(){

                    @Override
                    public void onRequestSuccess(HttpURLConnection connection, String response) {
                        YouboraLog.debug("Offline events sent, deleting JSON");
                        YouboraUtil.deleteOfflineEvents();
                    }
                };
            }

            comm.sendRequest(r,sucessListener);
        }
    }

    /**
     * If the current resource is not null this will trigger the resource parsing.
     * If resource parsing has already started or has ended, it should do nothing.
     */
    private void startResourceParsing() {

        String resource = getResource();

        if (resource != null) {
            resourceTransform.init(resource);
        }
    }

    /**
     * Initializes comm and its transforms.
     */
    private void initComm() {
        comm = createCommunication();
        comm.addTransform(createFlowTransform());

        comm.addTransform(resourceTransform);
        comm.addTransform(createNqs6Transform());

        //TODO Add if enabled on options
        if(options.isOffline()){
            if(getApplicationContext() != null){
                comm.addTransform(createOfflineTransform());
            }else{
                YouboraLog.notice("To use the offline feature you have to set the application context");
            }
        }else{
            comm.addTransform(viewTransform);
        }
    }

    /**
     * Starts preloading state and chronos.
     */
    public void firePreloadBegin() {
        if (!isPreloading) {
            isPreloading = true;
            preloadChrono.start();
        }
    }

    /**
     * Ends preloading state and chronos.
     */
    public void firePreloadEnd() {
        if (isPreloading) {
            isPreloading = false;
            preloadChrono.stop();
        }
    }

    /**
     * Same as calling {@link #fireInit(Map)} with a {@code null} param
     */
    public void fireInit() {
        fireInit(null);
    }

    /**
     * Sends /init. Should be called once the user has requested the content. Does not need
     * a working adapter or player to work. It won't sent start if isInitiated is true.
     *
     * @param params Map of params to add to the request.
     */
    public void fireInit(Map<String, String> params) {
        if (!isInitiated) {
            viewTransform.nextView();
            initComm();
            startPings();

            isInitiated = true;
            initChrono.start();

            sendInit(params);
        }

        startResourceParsing();
    }

    public void fireOfflineEvents(Map<String, String> params){

        if(getApplicationContext() == null){
            YouboraLog.error("The context have to be set to send saved offline events");
            return;
        }

        if(getOptions().isOffline()){
            YouboraLog.error("To send offline events, the offline option must be disabled");
            return;
        }

        if (!isInitiated) {
            initComm();
        }

        try {
            String offLineEvents = YouboraUtil.retrieveOfflineEvents();
            if(offLineEvents== null || offLineEvents.equals("")){
                YouboraLog.debug("No offline events, skipping...");
                return;
            }
            if(offLineEvents.substring((offLineEvents.length() - 3), (offLineEvents.length() - 1)).equals("},")){
                YouboraLog.warn("Wrong JSON format, trying to fix it...");
                offLineEvents = offLineEvents.substring(0, (offLineEvents.length() - 3));
                offLineEvents = offLineEvents + "]";
            }
            JSONObject offLineJSON = new JSONObject(String.format("{%s]}",offLineEvents));
            JSONArray offlineEvents = offLineJSON.getJSONArray("events");
            for(int k = 0 ; k < offlineEvents.length() ; k++){
                sendOfflineEvents(offlineEvents.get(k).toString());
            }
        } catch (UnsupportedEncodingException | JSONException e) {
            YouboraLog.error("Imposible to fix the JSON, removing the events");
            YouboraUtil.deleteOfflineEvents();
        }
    }

    private void sendOfflineEvents(String params){
        HashMap<String,String> events = new HashMap<>();
        events.put("events",params);
        send(null, SERVICE_OFFLINE_EVENTS,events);
        viewTransform.nextView();
    }

    private void sendInit(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_INIT);
        send(willSendInitListeners, SERVICE_INIT, params);
        String titleOrResource = params.get("title");
        if (titleOrResource == null) {
            titleOrResource = params.get("mediaResource");
        }
        YouboraLog.notice(SERVICE_INIT + " " + titleOrResource);
    }

    /**
     * Basic error handler. msg, code, errorMetadata and level params can be included in the params
     * argument.
     * @param params params to add to the request. If it is null default values will be added.
     */
    public void fireError(Map<String, String> params) {
        sendError(YouboraUtil.buildErrorParams(params));
    }

    /**
     * Sends a non-fatal error (with {@code level = "error"}).
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     */
    public void fireError(String msg, String code, String errorMetadata) {
        sendError(YouboraUtil.buildErrorParams(msg, code, errorMetadata, "error"));
    }

    private void startListener(Map<String, String> params) {
        if (!isInitiated || SERVICE_ERROR.equals(lastServiceSent)) {
            viewTransform.nextView();
            initComm();
            startPings();
        }

        startResourceParsing();

        sendStart(params);
    }

    /**
     * Sends /init service. Should be called once the user has requested the content.
     *
     * Does not need an adapter or player to work.
     *
     * @param params Map of params to add to the request.
     */
    private void sendStart(Map<String, String> params) {

        /*if(getIsLive() != null){
            if(getIsLive() || getOptions().getContentIsLive()){
                if (params == null) {
                    params = new HashMap<>();
                }
            }
        }*/

        params = requestBuilder.buildParams(params, SERVICE_START);
        send(willSendStartListeners, SERVICE_START, params);
        String titleOrResource = params.get("title");
        if (titleOrResource == null) {
            titleOrResource = params.get("mediaResource");
        }
        YouboraLog.notice(SERVICE_START + " " + titleOrResource);
    }

    private void joinListener(Map<String, String> params) {
        if (adsAdapter == null || !adsAdapter.getFlags().isStarted()) {
            sendJoin(params);
        } else {
            // Revert join state
            if (adapter != null){
                if (adapter.getMonitor() != null) adapter.getMonitor().stop();
                adapter.getFlags().setJoined(false);
                adapter.getChronos().join.setStopTime(null);
            }
        }
    }

    private void sendJoin(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_JOIN);
        send(willSendJoinListeners, SERVICE_JOIN, params);
        YouboraLog.notice(SERVICE_JOIN + " " + params.get("joinDuration") + "ms");
    }

    private void pauseListener(Map<String, String> params) {
        if (adapter != null) {
            if (adapter.getFlags().isBuffering() ||
                    adapter.getFlags().isSeeking() ||
                    (adsAdapter != null && adsAdapter.getFlags().isStarted())) {
                adapter.getChronos().pause.reset();
            }
        }

        sendPause(params);
    }

    private void sendPause(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_PAUSE);
        send(willSendPauseListeners, SERVICE_PAUSE, params);
        YouboraLog.notice(SERVICE_PAUSE+ " at " + params.get("playhead") + "s");
    }

    private void resumeListener(Map<String, String> params) {
        sendResume(params);
    }

    private void sendResume(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_RESUME);
        send(willSendResumeListeners, SERVICE_RESUME, params);
        YouboraLog.notice(SERVICE_RESUME+ " " + params.get("pauseDuration") + "ms");
    }

    private void seekBeginListener() {
        if (adapter != null && adapter.getFlags().isPaused()) {
            adapter.getChronos().pause.reset();
        }
        YouboraLog.notice("Seek Begin");
    }

    private void seekEndListener(Map<String, String> params) {
        sendSeekEnd(params);
    }

    private void sendSeekEnd(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_SEEK);
        send(willSendSeekListeners, SERVICE_SEEK, params);
        YouboraLog.notice(SERVICE_SEEK + " to " + params.get("playhead") + " in " + params.get("seekDuration") + "ms");
    }

    private void bufferBeginListener() {
        if (adapter != null && adapter.getFlags().isPaused()) {
            adapter.getChronos().pause.reset();
        }
        YouboraLog.notice("Buffer begin");
    }

    private void bufferEndListener(Map<String, String> params) {
        sendBufferEnd(params);
    }

    private void sendBufferEnd(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_BUFFER);
        send(willSendBufferListeners, SERVICE_BUFFER, params);
        YouboraLog.notice(SERVICE_BUFFER + " to " + params.get("playhead") + " in " + params.get("bufferDuration") + "ms");
    }

    private void errorListener(Map<String, String> params) {

        sendError(params);

        if ("fatal".equals(params.get("errorLevel"))) {
            reset();
        }
    }

    private void sendError(Map<String, String> params) {
        if (!isInitiated) {
            initComm();
        }
        startResourceParsing();
        params = requestBuilder.buildParams(params, SERVICE_ERROR);
        send(willSendErrorListeners, SERVICE_ERROR, params);
        YouboraLog.notice(SERVICE_ERROR + " " + " " + params.get("errorCode"));
    }

    private void stopListener(Map<String, String> params) {
        sendStop(params);
        reset();
    }

    private void sendStop(Map<String, String> params) {
        if(adapter != null){
            adapter.getFlags().setEnded(true);
        }
        if(adsAdapter != null){
            getOptions().setAdsAfterStop(0);
        }
        if(getOptions().getAdsAfterStop() == 0){
            if(adapter != null){
                adapter.getFlags().setStopped(true);
            }
        }
        params = requestBuilder.buildParams(params, SERVICE_STOP);
        send(willSendStopListeners, SERVICE_STOP, params);
        YouboraLog.notice(SERVICE_STOP + " at " + params.get("playhead"));
    }

    // Ads
    private void adInitListener(Map<String, String> params) {
        if (adapter != null && adsAdapter != null) {
            adapter.fireSeekEnd();
            adapter.fireBufferEnd();
            if (adapter.getFlags().isPaused()) {
                adapter.getChronos().pause.reset();
            }
        }

        sendAdInit(params);
    }

    private void sendAdInit(Map<String, String> params) {
        String realNumber = requestBuilder.getNewAdNumber();
        params = requestBuilder.buildParams(params, SERVICE_AD_INIT);
        params.put("adNumber", realNumber);
        params.put("adDuration","0");
        adsAdapter.getFlags().setAdInitiated(true);
        send(willSendAdInitListeners, SERVICE_AD_INIT, params);
        YouboraLog.notice(SERVICE_AD_INIT + " " + params.get("position") + params.get("adNumber") + " at " + params.get("playhead") + "s");
    }

    private void adStartListener(Map<String, String> params) {
        if (adapter != null) {
            adapter.fireSeekEnd();
            adapter.fireBufferEnd();
            if (adapter.getFlags().isPaused()) {
                adapter.getChronos().pause.reset();
            }
        }

        sendAdStart(params);
    }

    private void sendAdStart(Map<String, String> params) {
        String realNumber = adsAdapter.getFlags().isAdInitiated() ? requestBuilder.getLastSent().get("adNumber") : requestBuilder.getNewAdNumber();
        params = requestBuilder.buildParams(params, SERVICE_AD_START);
        params.put("adNumber", realNumber);
        params.put("adDuration","0");
        send(willSendAdStartListeners, SERVICE_AD_START, params);
        YouboraLog.notice(SERVICE_AD_START + " " + params.get("position") + params.get("adNumber") + " at " + params.get("playhead") + "s");
    }


    private void adJoinListener(Map<String, String> params) {
        sendAdJoin(params);
    }

    private void sendAdJoin(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_JOIN);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdJoinListeners, SERVICE_AD_JOIN, params);
        YouboraLog.notice(SERVICE_AD_JOIN + " " + params.get("adJoinDuration") + "ms");
    }

    private void adPauseListener(Map<String, String> params) {
        sendAdPause(params);
    }

    private void sendAdPause(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_PAUSE);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdPauseListeners, SERVICE_AD_PAUSE, params);
        YouboraLog.notice(SERVICE_AD_PAUSE + " at " + params.get("adPlayhead") + "s");
    }

    private void adResumeListener(Map<String, String> params) {
        sendAdResume(params);
    }

    private void sendAdResume(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_RESUME);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdResumeListeners, SERVICE_AD_RESUME, params);
        YouboraLog.notice(SERVICE_AD_RESUME + " " + params.get("adPauseDuration") + "ms");
    }

    private void adBufferBeginListener() {
        if (adsAdapter != null && adsAdapter.getFlags().isPaused()) {
            adsAdapter.getChronos().pause.reset();
        }
        YouboraLog.notice("Ad Buffer Begin");
    }

    private void adBufferEndListener(Map<String, String> params) {
        sendAdBufferEnd(params);
    }

    private void sendAdBufferEnd(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_BUFFER);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdBufferListeners, SERVICE_AD_BUFFER, params);
        YouboraLog.notice(SERVICE_AD_BUFFER + " " + params.get("adBufferDuration") + "s");
    }

    private void adStopListener(Map<String, String> params) {
        // Remove time from joinDuration, "delaying" the start time
        if (adapter != null && !adapter.getFlags().isJoined() && adsAdapter != null) {
            Long startTime = adapter.getChronos().join.getStartTime();
            if (startTime == null) {
                startTime = Chrono.getNow();
            }
            Long adStartTime = adsAdapter.getChronos().total.getStartTime();
            if (adStartTime == null) {
                adStartTime = Chrono.getNow();
            }
            startTime = Math.min(startTime + adStartTime, Chrono.getNow());
            adapter.getChronos().join.setStartTime(startTime);
        }
        sendAdStop(params);
    }

    private void sendAdStop(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_STOP);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdStopListeners, SERVICE_AD_STOP, params);
        YouboraLog.notice(SERVICE_AD_STOP + " " + params.get("adTotalDuration") + "ms");
    }

    private void adClickListener(Map<String, String> params){
        sendAdClick(params);
    }

    private void sendAdClick(Map<String, String> params){
        params = requestBuilder.buildParams(params, SERVICE_AD_CLICK);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdClickListeners, SERVICE_AD_CLICK, params);
        YouboraLog.notice(SERVICE_AD_CLICK + " " + params.get("adPlayhead") + "ms");
    }


    // ----------------------------------------- PINGS ---------------------------------------------
    private void startPings() {
        if (!pingTimer.isRunning()) {
            pingTimer.start();
        }
    }

    private void stopPings() {
        pingTimer.stop();
    }


    private void sendPing(long diffTime) {
        Map<String, String> params = new HashMap<>();
        params.put("diffTime", Long.toString(diffTime));
        Map<String, String> changedEntities = requestBuilder.getChangedEntities();
        if (changedEntities != null && !changedEntities.isEmpty()) {
            params.put("entities", YouboraUtil.stringifyMap(changedEntities));
        }

        if (adapter != null) {

            List<String> paramList = new LinkedList<>();

            if (adapter.getFlags().isPaused()) {
                paramList.add("pauseDuration");
            } else {
                paramList.add("bitrate");
                paramList.add("throughput");
                paramList.add("fps");

                if (adsAdapter != null && adsAdapter.getFlags().isStarted()) {
                    paramList.add("adBitrate");
                }
            }

            if (adapter.getFlags().isJoined()) {
                paramList.add("playhead");
            }

            if (adapter.getFlags().isBuffering()) {
                paramList.add("bufferDuration");
            }

            if (adapter.getFlags().isSeeking()) {
                paramList.add("seekDuration");
            }

            if (adsAdapter != null) {
                if (adsAdapter.getFlags().isStarted()) {
                    paramList.add("adPlayhead");
                }

                if (adsAdapter.getFlags().isBuffering()) {
                    paramList.add("adBufferDuration");
                }
            }

            params = requestBuilder.fetchParams(params, paramList, false);

        }

        send(willSendPingListeners, SERVICE_PING, params);
        YouboraLog.debug(SERVICE_PING);
    }

    // ------------------------------------- INFO GETTERS ------------------------------------------
    /**
     * Returns the host where all the NQS traces are sent.
     * @return the gost
     */
    public String getHost() {
        return YouboraUtil.addProtocol(YouboraUtil.stripProtocol(options.getHost()), options.isHttpSecure());
    }

    /**
     * Returns the parse HLS flag
     * @return the parse HLS flag
     */
    public boolean isParseHls() {
        return options.isParseHls();
    }

    /**
     * Returns the parse Cdn node flag
     * @return the parse Cdn node flag
     */
    public boolean isParseCdnNode() {
        return options.isParseCdnNode();
    }

    /**
     * Returns the list of Cdn names that are enabled for the {@link ResourceTransform}.
     * @return the cdn list
     */
    public List<String> getParseCdnNodeList() {
        return options.getParseCdnNodeList();
    }

    /**
     * Returns the Cdn name header. This value is later passed to
     * {@link com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser#setBalancerHeaderName(String)}.
     * @return the cdn name header.
     */
    public String getParseCdnNodeNameHeader() {
        return options.getParseCdnNameHeader();
    }

    /**
     * Returns the content's playhead in seconds
     * @return the content's playhead
     */
    public Double getPlayhead() {
        Double ph = null;
        if (adapter != null) {
            try {
                ph = adapter.getPlayhead();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayhead");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(ph, 0.0);
    }

    /**
     * Returns the content's Playrate
     * @return the content's Playrate
     */
    public Double getPlayrate() {
        Double val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayrate");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 1.0);
    }

    /**
     * Returns the content's FPS
     * @return the content's FPS
     */
    public Double getFramesPerSecond() {
        Double val = options.getContentFps();
        if (val == null && adapter != null) {
            try {
                val = adapter.getFramesPerSecond();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getFramesPerSecond");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's dropped frames
     * @return the content's dropped frames
     */
    public Integer getDroppedFrames() {
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getDroppedFrames();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getDroppedFrames");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns the content's duration in seconds
     * @return the content's duration
     */
    public Double getDuration() {
        Double val = options.getContentDuration();
        if (val == null && adapter != null) {
            try {
                val = adapter.getDuration();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getDuration");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns the content's bitrate in bits per second
     * @return the content's bitrate
     */
    public Long getBitrate() {
        Long val = options.getContentBitrate();
        if (val == null && adapter != null) {
            try {
                val = adapter.getBitrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getBitrate");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns the content's throughput in bits per second
     * @return the content's throughput
     */
    public Long getThroughput() {
        Long val = options.getContentThroughput();
        if (val == null && adapter != null) {
            try {
                val = adapter.getThroughput();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getThroughput");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns the content's rendition
     * @return the content's rendition
     */
    public String getRendition() {
        String val = options.getContentRendition();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getRendition();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getRendition");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's title
     * @return the content's title
     */
    public String getTitle() {
        String val = options.getContentTitle();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getTitle();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getTitle");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's title2
     * @return the content's title2
     */
    public String getTitle2() {
        String val = options.getContentTitle2();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getTitle2();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getTitle2");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns whether the content is live or not
     * @return whether the content is live or not
     */
    public Boolean getIsLive() {
        Boolean val = options.getContentIsLive();
        if (val == null && adapter != null) {
            try {
                val = adapter.getIsLive();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getIsLive");
                YouboraLog.error(e);
            }
        }

        return val != null ? val : false;
    }

    /**
     * Returns the content's resource after being parsed by the {@link ResourceTransform}
     * @return the content's resource
     */
    public String getResource() {
        String val = null;
        if (!resourceTransform.isBlocking(null)) {
            val = resourceTransform.getResource();
        }

        if (val == null) {
            val = getOriginalResource();
        }

        return val;
    }

    /**
     * Returns the content's original resource (before being parsed by the {@link ResourceTransform})
     * @return the content's original resource
     */
    public String getOriginalResource() {
        String val = options.getContentResource();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getResource();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getResource");
                YouboraLog.error(e);
            }
        }

        if (val != null && val.length() == 0) {
            val = null;
        }

        return val;
    }

    /**
     * Returns the transaction code
     * @return the transaction code
     */
    public String getTransactionCode() {
        return options.getContentTransactionCode();
    }

    /**
     * Returns the content metadata
     * @return the content metadata
     */
    public String getContentMetadata() {
        return YouboraUtil.stringifyBundle(options.getContentMetadata());
    }

    /**
     * Returns the version of the player that is used to play the content
     * @return the player version
     */
    public String getPlayerVersion() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayerVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayerVersion");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the name of the player that is used to play the content
     * @return the player name
     */
    public String getPlayerName() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayerName();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayerName");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the content cdn
     * @return the content cdn
     */
    public String getCdn() {
        String cdn = null;
        if (!resourceTransform.isBlocking(null)) {
            cdn = resourceTransform.getCdnName();
        }
        if (cdn == null) {
            cdn = options.getContentCdn();
        }
        return cdn;
    }

    /**
     * Returns the PluginVersion
     * @return the PluginVersion
     */
    public String getPluginVersion() {
        String ret = getAdapterVersion();
        if (ret == null) {
            ret = BuildConfig.VERSION_NAME + "-adapterless";
        }
        return ret;
    }

    /**
     * Returns the content {@link PlayerAdapter} version if available
     * @return the content Adapter version
     */
    private String getAdapterVersion() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdapterVersion");
                YouboraLog.error(e);
            }
        }
        return val;
    }

    /**
     * Returns content's Extraparam1
     * @return extraparam 1 value
     */
    public String getExtraparam1() {
        return options.getExtraparam1();
    }

    /**
     * Returns content's Extraparam2
     * @return extraparam 2 value
     */
    public String getExtraparam2() {
        return options.getExtraparam2();
    }

    /**
     * Returns content's Extraparam
     * @return extraparam 3 value
     */
    public String getExtraparam3() {
        return options.getExtraparam3();
    }

    /**
     * Returns content's Extraparam4
     * @return extraparam 4 value
     */
    public String getExtraparam4() {
        return options.getExtraparam4();
    }

    /**
     * Returns content's Extraparam5
     * @return extraparam 5 value
     */
    public String getExtraparam5() {
        return options.getExtraparam5();
    }

    /**
     * Returns content's Extraparam6
     * @return extraparam 6 value
     */
    public String getExtraparam6() {
        return options.getExtraparam6();
    }

    /**
     * Returns content's Extraparam7
     * @return extraparam 7 value
     */
    public String getExtraparam7() {
        return options.getExtraparam7();
    }

    /**
     * Returns content's Extraparam8
     * @return extraparam 8 value
     */
    public String getExtraparam8() {
        return options.getExtraparam8();
    }

    /**
     * Returns content's Extraparam9
     * @return extraparam 9 value
     */
    public String getExtraparam9() {
        return options.getExtraparam9();
    }

    /**
     * Returns content's Extraparam10
     * @return extraparam 10 value
     */
    public String getExtraparam10() {
        return options.getExtraparam10();
    }

    /**
     * Returns the version of the player that is used to play the ad(s)
     * @return the player version
     */
    public String getAdPlayerVersion() {
        String val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getPlayerVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPlayerVersion");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the ad position as YOUBORA expects it; "pre", "mid", "post" or "unknown"
     * @return the ad position
     */
    public String getAdPosition() {
        PlayerAdapter.AdPosition pos = PlayerAdapter.AdPosition.UNKNOWN;
        if (adsAdapter != null) {
            try {
                pos = adsAdapter.getPosition();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPosition");
                YouboraLog.error(e);
            }
        }

        if (pos == PlayerAdapter.AdPosition.UNKNOWN && adapter != null) {
            pos = adapter.getFlags().isJoined()? PlayerAdapter.AdPosition.MID : PlayerAdapter.AdPosition.PRE;
        }

        String position;

        switch (pos) {
            case PRE:
                position = "pre";
                break;
            case MID:
                position = "mid";
                break;
            case POST:
                position = "post";
                break;
            case UNKNOWN:
            default:
                position = "unknown";
        }

        return position;
    }

    /**
     * Returns ad's playhead in seconds
     * @return ad's playhead
     */
    public Double getAdPlayhead() {
        Double ph = null;
        if (adsAdapter != null) {
            try {
                ph = adsAdapter.getPlayhead();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPlayhead");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(ph, 0.0);
    }

    /**
     * Returns ad's duration in seconds
     * @return ad's duration
     */
    public Double getAdDuration() {
        Double val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getDuration();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdDuration");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns ad's bitrate in bits per second
     * @return ad's bitrate
     */
    public Long getAdBitrate() {
        Long val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getBitrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdBitrate");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns ad's title
     * @return ad's title
     */
    public String getAdTitle() {
        String val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getTitle();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdTitle");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns ad's resource
     * @return ad's resource
     */
    public String getAdResource() {
        String val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getResource();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdResource");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the ad adapter version
     * @return the ad adapter version
     */
    public String getAdAdapterVersion() {
        String val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdAdapterVersion");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the ad metadata
     * @return the ad metadata
     */
    public String getAdMetadata() {
        return YouboraUtil.stringifyBundle(options.getAdMetadata());
    }

    /**
     * Returns a json-formatted string with plugin info
     * @return plugin info
     */
    public String getPluginInfo() {
        Map<String, String> info = new HashMap<>(3);
        info.put("lib", BuildConfig.VERSION_NAME);
        info.put("adapter", getAdapterVersion());
        info.put("adAdapter", getAdAdapterVersion());
        return YouboraUtil.stringifyMap(info);
    }

    /**
     * Returns the Ip
     * @return the Ip
     */
    public String getIp() {
        return options.getNetworkIP();
    }

    /**
     * Returns the Isp
     * @return the Isp
     */
    public String getIsp() {
        return options.getNetworkIsp();
    }

    /**
     * Returns the connection type
     * @return the conneciton type
     */
    public String getConnectionType() {
        return options.getNetworkConnectionType();
    }

    /**
     * Returns the device code
     * @return the device code
     */
    public String getDeviceCode() {
        return options.getDeviceCode();
    }

    /**
     * Returns the account code
     * @return the account code
     */
    public String getAccountCode() {
        return options.getAccountCode();
    }

    /**
     * Returns the username
     * @return the username
     */
    public String getUsername() {
        return options.getUsername();
    }

    /**
     * Get CDN node
     * @return the CDN node or null if unknown
     */
    public String getNodeHost() {
        return resourceTransform.getNodeHost();
    }

    /**
     * Get CDN type, parsed from the type string
     * @return the CDN type
     */
    public String getNodeType() {
        return resourceTransform.getNodeType();
    }

    /**
     * Get CDN type string, as returned in the cdn header response
     * @return the CDN type string
     */
    public String getNodeTypeString() {
        return resourceTransform.getNodeTypeString();
    }

    // ---------------------------------------- Background --------------------------------------------
    /**
     * Setter for the activity to listen to callback events (needed for background detection)
     */
    public void setActivity(Activity activity){
        this.activity = activity;
    }

    /**
     * Getter for the activity (needed for background detection)
     * @return Application context
     */
    public Activity getActivity(){
        return activity;
    }

    // ---------------------------------------- CHRONOS --------------------------------------------
    /**
     * Returns preload chrono delta time
     * @return the preload duration
     */
    public long getPreloadDuration() {
        return preloadChrono.getDeltaTime(false);
    }

    /**
     * Returns init chrono delta time
     * @return the init duration
     */
    public long getInitDuration() {
        return initChrono.getDeltaTime(false);
    }

    /**
     * Returns JoinDuration chrono delta time
     * @return the join duration
     */
    public long getJoinDuration() {
        if (isInitiated) return getInitDuration();
        return adapter != null ? adapter.getChronos().join.getDeltaTime(false) : -1;
    }

    /**
     * Returns BufferDuration chrono delta time
     * @return the buffer duration
     */
    public long getBufferDuration() {
        return adapter != null ? adapter.getChronos().buffer.getDeltaTime(false) : -1;
    }

    /**
     *  Returns SeekDuration chrono delta time
     * @return the seek duration
     */
    public long getSeekDuration() {
        return adapter != null ? adapter.getChronos().seek.getDeltaTime(false) : -1;
    }

    /**
     * Returns pauseDuration chrono delta time
     * @return the pause duration
     */
    public long getPauseDuration() {
        return adapter != null ? adapter.getChronos().pause.getDeltaTime(false) : -1;
    }

    /**
     * Returns AdJoinDuration chrono delta time
     * @return the ad join duration
     */
    public long getAdJoinDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().join.getDeltaTime(false) : -1;
    }

    /**
     * Returns AdBufferDuration chrono delta time
     * @return the ad buffer duration
     */
    public long getAdBufferDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().buffer.getDeltaTime(false) : -1;
    }

    /**
     * Returns AdPauseDuration chrono delta time
     * @return the ad pause duration
     */
    public long getAdPauseDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().pause.getDeltaTime(false) : -1;
    }

    /**
     * Returns total totalAdDuration chrono delta time
     * @return the total ad duration
     */
    public long getAdTotalDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().total.getDeltaTime(false) : -1;
    }

    /**
     * Returns the {@link RequestBuilder} instance that this Plugin is using
     * @return the {@link RequestBuilder} instance that this Plugin is using
     */
    public RequestBuilder getRequestBuilder() {
        return requestBuilder;
    }

    // Add listeners
    /**
     * Adds an Init listener
     * @param listener to add
     */
    public void addOnWillSendInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners == null)
            willSendInitListeners = new ArrayList<>(1);
        willSendInitListeners.add(listener);
    }

    /**
     * Adds a Start listener
     * @param listener to add
     */
    public void addOnWillSendStartListener(WillSendRequestListener listener) {
        if (willSendStartListeners == null)
            willSendStartListeners = new ArrayList<>(1);
        willSendStartListeners.add(listener);
    }

    /**
     * Adds a Join listener
     * @param listener to add
     */
    public void addOnWillSendJoinListener(WillSendRequestListener listener) {
        if (willSendJoinListeners == null)
            willSendJoinListeners = new ArrayList<>(1);
        willSendJoinListeners.add(listener);
    }

    /**
     * Adds a Pause listener
     * @param listener to add
     */
    public void addOnWillSendPauseListener(WillSendRequestListener listener) {
        if (willSendPauseListeners == null)
            willSendPauseListeners = new ArrayList<>(1);
        willSendPauseListeners.add(listener);
    }

    /**
     * Adds a Resume listener
     * @param listener to add
     */
    public void addOnWillSendResumeListener(WillSendRequestListener listener) {
        if (willSendResumeListeners == null)
            willSendResumeListeners = new ArrayList<>(1);
        willSendResumeListeners.add(listener);
    }

    /**
     * Adds a Seek listener
     * @param listener to add
     */
    public void addOnWillSendSeekListener(WillSendRequestListener listener) {
        if (willSendSeekListeners == null)
            willSendSeekListeners = new ArrayList<>(1);
        willSendSeekListeners.add(listener);
    }

    /**
     * Adds a Buffer listener
     * @param listener to add
     */
    public void addOnWillSendBufferListener(WillSendRequestListener listener) {
        if (willSendBufferListeners == null)
            willSendBufferListeners = new ArrayList<>(1);
        willSendBufferListeners.add(listener);
    }

    /**
     * Adds a Error listener
     * @param listener to add
     */
    public void addOnWillSendErrorListener(WillSendRequestListener listener) {
        if (willSendErrorListeners == null)
            willSendErrorListeners = new ArrayList<>(1);
        willSendErrorListeners.add(listener);
    }

    /**
     * Adds a Stop listener
     * @param listener to add
     */
    public void addOnWillSendStopListener(WillSendRequestListener listener) {
        if (willSendStopListeners == null)
            willSendStopListeners = new ArrayList<>(1);
        willSendStopListeners.add(listener);
    }

    /**
     * Adds a Ping listener
     * @param listener to add
     */
    public void addOnWillSendPingListener(WillSendRequestListener listener) {
        if (willSendPingListeners == null)
            willSendPingListeners = new ArrayList<>(1);
        willSendPingListeners.add(listener);
    }

    /**
     * Adds an ad Start listener
     * @param listener to add
     */
    public void addOnWillSendAdStartListener(WillSendRequestListener listener) {
        if (willSendAdStartListeners == null)
            willSendAdStartListeners = new ArrayList<>(1);
        willSendAdStartListeners.add(listener);
    }

    /**
     * Adds an ad Join listener
     * @param listener to add
     */
    public void addOnWillSendAdJoinListener(WillSendRequestListener listener) {
        if (willSendAdJoinListeners == null)
            willSendAdJoinListeners = new ArrayList<>(1);
        willSendAdJoinListeners.add(listener);
    }

    /**
     * Adds an ad Pause listener
     * @param listener to add
     */
    public void addOnWillSendAdPauseListener(WillSendRequestListener listener) {
        if (willSendAdPauseListeners == null)
            willSendAdPauseListeners = new ArrayList<>(1);
        willSendAdPauseListeners.add(listener);
    }

    /**
     * Adds an ad Resume listener
     * @param listener to add
     */
    public void addOnWillSendAdResumeListener(WillSendRequestListener listener) {
        if (willSendAdResumeListeners == null)
            willSendAdResumeListeners = new ArrayList<>(1);
        willSendAdResumeListeners.add(listener);
    }

    /**
     * Adds an ad Buffer listener
     * @param listener to add
     */
    public void addOnWillSendAdBufferListener(WillSendRequestListener listener) {
        if (willSendAdBufferListeners == null)
            willSendAdBufferListeners = new ArrayList<>(1);
        willSendAdBufferListeners.add(listener);
    }

    /**
     * Adds an ad Stop listener
     * @param listener to add
     */
    public void addOnWillSendAdStopListener(WillSendRequestListener listener) {
        if (willSendAdStopListeners == null)
            willSendAdStopListeners = new ArrayList<>(1);
        willSendAdStopListeners.add(listener);
    }

    /**
     * Adds an ad Stop listener
     * @param listener to add
     */
    public void addOnWillSendAdClickListener(WillSendRequestListener listener) {
        if (willSendAdClickListeners == null)
            willSendAdClickListeners = new ArrayList<>(1);
        willSendAdClickListeners.add(listener);
    }

    /**
     * Adds an ad Init listener
     * @param listener to add
     */
    public void addOnWillSendAdInitListener(WillSendRequestListener listener) {
        if (willSendAdInitListeners == null)
            willSendAdInitListeners = new ArrayList<>(1);
        willSendAdInitListeners.add(listener);
    }

    // Remove listeners
    /**
     * Removes an Init listener
     * @param listener to remove
     */
    public void removeOnWillSendInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners != null)
            willSendInitListeners.remove(listener);
    }

    // Remove listeners
    /**
     * Removes an ad Init listener
     * @param listener to remove
     */
    public void removeOnWillSendAdInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners != null)
            willSendInitListeners.remove(listener);
    }

    /**
     * Removes a Start listener
     * @param listener to remove
     */
    public void removeOnWillSendStartListener(WillSendRequestListener listener) {
        if (willSendStartListeners != null)
            willSendStartListeners.remove(listener);
    }

    /**
     * Removes a Join listener
     * @param listener to remove
     */
    public void removeOnWillSendJoinListener(WillSendRequestListener listener) {
        if (willSendJoinListeners != null)
            willSendJoinListeners.remove(listener);
    }

    /**
     * Removes a Pause listener
     * @param listener to remove
     */
    public void removeOnWillSendPauseListener(WillSendRequestListener listener) {
        if (willSendPauseListeners != null)
            willSendPauseListeners.remove(listener);
    }

    /**
     * Removes a Resume listener
     * @param listener to remove
     */
    public void removeOnWillSendResumeListener(WillSendRequestListener listener) {
        if (willSendResumeListeners != null)
            willSendResumeListeners.remove(listener);
    }

    /**
     * Removes a Seek listener
     * @param listener to remove
     */
    public void removeOnWillSendSeekListener(WillSendRequestListener listener) {
        if (willSendSeekListeners != null)
            willSendSeekListeners.remove(listener);
    }

    /**
     * Removes a Buffer listener
     * @param listener to remove
     */
    public void removeOnWillSendBufferListener(WillSendRequestListener listener) {
        if (willSendBufferListeners != null)
            willSendBufferListeners.remove(listener);
    }

    /**
     * Removes a Error listener
     * @param listener to remove
     */
    public void removeOnWillSendErrorListener(WillSendRequestListener listener) {
        if (willSendErrorListeners != null)
            willSendErrorListeners.remove(listener);
    }

    /**
     * Removes a Stop listener
     * @param listener to remove
     */
    public void removeOnWillSendStopListener(WillSendRequestListener listener) {
        if (willSendStopListeners != null)
            willSendStopListeners.remove(listener);
    }

    /**
     * Removes a Ping listener
     * @param listener to remove
     */
    public void removeOnWillSendPingListener(WillSendRequestListener listener) {
        if (willSendPingListeners != null)
            willSendPingListeners.remove(listener);
    }

    /**
     * Removes an ad Start listener
     * @param listener to remove
     */
    public void removeOnWillSendAdStartListener(WillSendRequestListener listener) {
        if (willSendAdStartListeners != null)
            willSendAdStartListeners.remove(listener);
    }

    /**
     * Removes an ad Join listener
     * @param listener to remove
     */
    public void removeOnWillSendAdJoinListener(WillSendRequestListener listener) {
        if (willSendAdJoinListeners != null)
            willSendAdJoinListeners.remove(listener);
    }

    /**
     * Removes an ad Pause listener
     * @param listener to remove
     */
    public void removeOnWillSendAdPauseListener(WillSendRequestListener listener) {
        if (willSendAdPauseListeners != null)
            willSendAdPauseListeners.remove(listener);
    }

    /**
     * Removes an ad Resume listener
     * @param listener to remove
     */
    public void removeOnWillSendAdResumeListener(WillSendRequestListener listener) {
        if (willSendAdResumeListeners != null)
            willSendAdResumeListeners.remove(listener);
    }

    /**
     * Removes an ad Buffer listener
     * @param listener to remove
     */
    public void removeOnWillSendAdBufferListener(WillSendRequestListener listener) {
        if (willSendAdBufferListeners != null)
            willSendAdBufferListeners.remove(listener);
    }

    /**
     * Removes an ad Stop listener
     * @param listener to remove
     */
    public void removeOnWillSendAdStopListener(WillSendRequestListener listener) {
        if (willSendAdStopListeners != null)
            willSendAdStopListeners.remove(listener);
    }

    /**
     * Removes
     *
     */

    public void removeOnWillSendAdClick(WillSendRequestListener listener) {
        if (willSendAdClickListeners != null)
            willSendAdClickListeners.remove(listener);
    }

    /**
     * Saves the context
     */
    //TODO Context NEVER should be static, find a way to remove it
    public void setApplicationContext(Context context){
        YouboraUtil.context = context;
    }

    /**
     * Saves the context
     */
    public Context getApplicationContext(){
        return YouboraUtil.context;
    }

    // Content Adapter listener
    private PlayerAdapter.AdapterEventListener eventListener = new PlayerAdapter.AdapterEventListener() {

        @Override
        public void onStart(Map<String, String> params) {
            startListener(params);
        }

        @Override
        public void onJoin(Map<String, String> params) {
            joinListener(params);
        }

        @Override
        public void onPause(Map<String, String> params) {
            pauseListener(params);
        }

        @Override
        public void onResume(Map<String, String> params) {
            resumeListener(params);
        }

        @Override
        public void onStop(Map<String, String> params) {
            stopListener(params);
        }

        @Override
        public void onBufferBegin(Map<String, String> params, boolean convertFromBuffer) {
            bufferBeginListener();
        }

        @Override
        public void onBufferEnd(Map<String, String> params) {
            bufferEndListener(params);
        }

        @Override
        public void onSeekBegin(Map<String, String> params, boolean convertFromBuffer) {
            seekBeginListener();
        }

        @Override
        public void onSeekEnd(Map<String, String> params) {
            seekEndListener(params);
        }

        @Override
        public void onClick(Map<String, String> params) {

        }

        @Override
        public void onAdInit(Map<String, String> params) {
            adInitListener(params);
        }

        @Override
        public void onError(Map<String, String> params) {
            errorListener(params);
        }
    };

    // Ad Adapter listener
    private PlayerAdapter.AdapterEventListener adEventListener =  new PlayerAdapter.AdapterEventListenerImpl() {

        @Override
        public void onStart(Map<String, String> params) {
            adStartListener(params);
        }

        @Override
        public void onJoin(Map<String, String> params) {
            adJoinListener(params);
        }

        @Override
        public void onPause(Map<String, String> params) {
            adPauseListener(params);
        }

        @Override
        public void onResume(Map<String, String> params) {
            adResumeListener(params);
        }

        @Override
        public void onStop(Map<String, String> params) {
            adStopListener(params);
        }

        @Override
        public void onBufferBegin(Map<String, String> params, boolean convertFromBuffer) {
            adBufferBeginListener();
        }

        @Override
        public void onBufferEnd(Map<String, String> params) {
            adBufferEndListener(params);
        }

        @Override
        public void onClick(Map<String, String> params) {
            adClickListener(params);
        }

        @Override
        public void onAdInit(Map<String, String> params) {
            adInitListener(params);
        }
    };

    /**
     * Will send Request Interface.
     * This callback will be invoked just before an event is sent, to offer the chance to
     * modify the params that will be sent.
     */
    public interface WillSendRequestListener {

        /**
         * Called just before a {@link Request} is sent
         * @param serviceName the service name
         * @param plugin the {@link Plugin} that is calling this callback
         * @param params a Map of params that will be sent in the Request
         */
        void willSendRequest(String serviceName, Plugin plugin, Map<String, String> params);/**
         * Called just before a {@link Request} is sent
         * @param serviceName the service name
         * @param plugin the {@link Plugin} that is calling this callback
         * @param params an ArrayList of {@link JSONObject}
         */
        void willSendRequest(String serviceName, Plugin plugin, ArrayList<JSONObject> params);
    }

}
