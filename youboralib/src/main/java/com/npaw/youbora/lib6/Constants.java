package com.npaw.youbora.lib6;

/**
 * Created by joan on 25/01/2017.
 */

public class Constants {
    // Service constants
    /** /data service */
    public static final String SERVICE_DATA = "/data";
    /** /init service */
    public static final String SERVICE_INIT = "/init";
    /** /start service */
    public static final String SERVICE_START = "/start";
    /** /joinTime service */
    public static final String SERVICE_JOIN = "/joinTime";
    /** /pause service */
    public static final String SERVICE_PAUSE = "/pause";
    /** /resume service */
    public static final String SERVICE_RESUME = "/resume";
    /** /seek service */
    public static final String SERVICE_SEEK = "/seek";
    /** /bufferUnderrun service */
    public static final String SERVICE_BUFFER = "/bufferUnderrun";
    /** /error service */
    public static final String SERVICE_ERROR = "/error";
    /** /stop service */
    public static final String SERVICE_STOP = "/stop";
    /** /ping service */
    public static final String SERVICE_PING = "/ping";
    /** /ping service */
    public static final String SERVICE_OFFLINE_EVENTS = "/offlineEvents";

    /** /adInit service */
    public static final String SERVICE_AD_INIT = "/adInit";
    /** /adStart service */
    public static final String SERVICE_AD_START = "/adStart";
    /** /adJoin service */
    public static final String SERVICE_AD_JOIN = "/adJoin";
    /** /adClick service */
    public static final String SERVICE_AD_CLICK = "/adClick";
    /** /adPause service */
    public static final String SERVICE_AD_PAUSE = "/adPause";
    /** /adResume service */
    public static final String SERVICE_AD_RESUME = "/adResume";
    /** /adBufferUnderrun service */
    public static final String SERVICE_AD_BUFFER = "/adBufferUnderrun";
    /** /adStop service */
    public static final String SERVICE_AD_STOP = "/adStop";
}
