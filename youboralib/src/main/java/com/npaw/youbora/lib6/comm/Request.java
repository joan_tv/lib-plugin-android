package com.npaw.youbora.lib6.comm;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.npaw.youbora.lib6.YouboraLog.Level.VERBOSE;

/**
 * Facade for an underlying Http request implementation.
 * This class encapsulates the http requests. It performs as an interface against the system http
 * calls. Requests are highly customizable via the params property and the
 * {@link #addOnSuccessListener(RequestSuccessListener)} and {@link #addOnErrorListener(RequestErrorListener)}
 * methods.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Request {

    private List<RequestSuccessListener> successListenerList = new ArrayList<>(1);
    private List<RequestErrorListener> errorListenerList = new ArrayList<>(1);

    private static List<RequestSuccessListener> onEverySuccessListenerList;
    private static List<RequestErrorListener> onEveryErrorListenerList;

    // Fields
    private String host;
    private String service;
    private Map<String, Object> params;

    // Default method is GET
    private String method = METHOD_GET;
    private Map<String, String> requestHeaders;
    private int maxRetries = 3;
    private int pendingAttempts;
    private int retryInterval = 5000;

    // Method constants
    /** HTTP GET method */
    public static final String METHOD_GET = "GET";
    /** HTTP POST method */
    public static final String METHOD_POST = "POST";
    /** HTTP HEAD method */
    public static final String METHOD_HEAD = "HEAD";
    /** HTTP OPTIONS method */
    public static final String METHOD_OPTIONS = "OPTIONS";
    /** HTTP PUT method */
    public static final String METHOD_PUT = "PUT";
    /** HTTP DELETE method */
    public static final String METHOD_DELETE = "DELETE";
    /** HTTP TRACE method */
    public static final String METHOD_TRACE = "TRACE";

    /**
     * Constructor
     * @param host host where to send the request
     * @param service service to consume
     */
    public Request(String host, String service) {
        this.host = host;
        this.service = service;
    }

    /**
     * Sends this Request over the network.
     */
    public void send() {
        pendingAttempts = maxRetries + 1;
        new RequestThread().start();
    }

    /**
     * Builds the url. It consists of the following: {@link #getHost()} + {@link #getService()}  +
     * {@link #getQuery()}
     * @return the full query url
     */
    public String getUrl() {
        StringBuilder sb = new StringBuilder();
        String s = getHost();
        if (s != null) sb.append(s);
        s = getService();
        if (s != null) sb.append(s);
        s = getQuery();
        if (s != null) sb.append(s);
        return sb.toString();
    }

    /**
     * Parses the request parameters key/value list into an URL encoded query string.
     *
     * @return the String representation of the class {@link #params}.
     */
    public String getQuery() {

        if (params != null && !params.isEmpty()) {

            Uri.Builder uriBuilder = Uri.parse("?").buildUpon();

            for (Map.Entry<String, Object> entry : params.entrySet()) {

                String value = null;

                if (entry.getValue() instanceof Map) {
                    value = YouboraUtil.stringifyMap((Map) entry.getValue());
                } else if (entry.getValue() instanceof Bundle) {
                    value = YouboraUtil.stringifyBundle((Bundle) entry.getValue());
                } else {
                    if (entry.getValue() != null) {
                        value = entry.getValue().toString();
                    }
                }

                if (value != null && value.length() > 0 && !entry.getKey().equals("events")) {
                    uriBuilder.appendQueryParameter(entry.getKey(), value);
                }

            }

            return uriBuilder.toString();
        }

        return "";
    }

    // Setters

    /**
     * Sets the host
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Sets the service
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * Sets the params
     * @param params the params to set
     */
    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    /**
     * Sets one key-value pair onto the params
     * @param key param key
     * @param value param value
     */
    public void setParam(String key, Object value) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put(key, value);
    }

    /**
     * Sets the http method. Should be one of the following; {@link #METHOD_GET}, {@link #METHOD_DELETE},
     * {@link #METHOD_HEAD}, {@link #METHOD_OPTIONS}, {@link #METHOD_POST}, {@link #METHOD_PUT},
     * {@link #METHOD_TRACE}.
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Set the headers that should be added to the request.
     * @param requestHeaders the http requests to set.
     */
    public void setRequestHeaders(Map<String, String> requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    /**
     * Set the number of retries for this request. The default value is 3.
     * @param maxRetries retries
     */
    public void setMaxRetries(int maxRetries) {
        if (maxRetries >= 0) {
            this.maxRetries = maxRetries;
        }
    }

    /**
     * Set the retry interval for this request. In milliseconds. The default value is 5000.
     * @param retryInterval interval.
     */
    public void setRetryInterval(int retryInterval) {
        if (retryInterval >= 0) {
            this.retryInterval = retryInterval;
        }
    }

    // Getters

    /**
     * Get host
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * Get service
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * The whole params Map
     * @return params
     */
    public Map<String, Object> getParams() {
        return params;
    }

    /**
     * Returns the param value for the given key
     * @param key the key for the desired value
     * @return the param value
     */
    public Object getParam(String key) {
        if (params != null) {
            return params.get(key);
        } else {
            return null;
        }
    }

    /**
     * The http method that will be sued
     * @return http mehtod
     */
    public String getMethod() {
        return method;
    }

    /**
     * Returns the http headers that will be added to the request
     * @return the http headers
     */
    public Map<String, String> getRequestHeaders() {
        return requestHeaders;
    }

    /**
     * Get retries. 3 by default
     * @return number of retries
     */
    public int getMaxRetries() {
        return maxRetries;
    }

    /**
     * Get retry interval. 5000 milliseconds by default
     * @return the retry interval.
     */
    public int getRetryInterval() {
        return retryInterval;
    }

    // Events

    /**
     * Adds a success listener.
     * @param listener the listener to add.
     */
    public void addOnSuccessListener(RequestSuccessListener listener) {
        successListenerList.add(listener);
    }

    /**
     * Removes a success listener
     * @param listener the listener ot remove
     * @return whether the listener has been removed or not.
     */
    public boolean removeOnSuccessListener(RequestSuccessListener listener) {
        return successListenerList.remove(listener);
    }

    /**
     * Adds an error listener. These listeners will be called for each failed retry. If you want to
     * implement you own retry management, do so here and {@link #setMaxRetries(int)} to 0.
     * @param listener the listener to add
     */
    public void addOnErrorListener(RequestErrorListener listener) {
        errorListenerList.add(listener);
    }

    /**
     * Remove an error listener
     * @param listener the listener to remove
     * @return whether the listener has been removed or not.
     */
    public boolean removeOnErrorListener(RequestErrorListener listener) {
        return errorListenerList.remove(listener);
    }

    /**
     * Adds a <b>global</b> success listener. These listeners will be called <b>for all the Requests</b>.
     * @param listener the global listener to add.
     */
    public static void addOnEverySuccessListener(RequestSuccessListener listener) {
        if (onEverySuccessListenerList == null) {
            onEverySuccessListenerList = new ArrayList<>(1);
        }
        onEverySuccessListenerList.add(listener);
    }

    /**
     * Removes a global success listener.
     * @param listener the global listener to remove
     * @return whether the listener has been removed or not.
     */
    public static boolean removeOnEverySuccessListener(RequestSuccessListener listener) {
        if (onEverySuccessListenerList == null) {
            return false;
        } else {
            return onEverySuccessListenerList.remove(listener);
        }
    }

    /**
     * Adds a <b>global</b> error listener. These listeners will be called <b>for all the Requests</b>.
     * @param listener the global listener to add.
     */
    public static void addOnEveryErrorListener(RequestErrorListener listener) {
        if (onEveryErrorListenerList == null) {
            onEveryErrorListenerList = new ArrayList<>(1);
        }
        onEveryErrorListenerList.add(listener);
    }

    /**
     * Removes a global error listener.
     * @param listener the global listener to remove
     * @return whether the listener has been removed or not.
     */
    public static boolean removeOnEveryErrorListener(RequestErrorListener listener) {
        if (onEveryErrorListenerList == null) {
            return false;
        } else {
            return  onEveryErrorListenerList.remove(listener);
        }
    }

    /**
     * Class used to send requests over the network. A Thread is used since the {@link HttpURLConnection}
     * works in a synchronous way.
     */
    private class RequestThread extends Thread {

        private Handler originHandler;

        private HttpURLConnection httpURLConnection;
        private String response;

        public RequestThread() {
            super();

            /*
             * We create a Handler here in order to post the callback messages
             * to the same thread that is creating this instance
             */
            originHandler = new Handler();
        }

        @Override
        public void run() {
            sendRequest();
        }

        private void sendRequest() {
            pendingAttempts--;
            try {
                URL url = new URL(getUrl());

                if (YouboraLog.debugLevel().isAtLeast(VERBOSE)) {
                    YouboraLog.requestLog("XHR Req: " + url.toExternalForm());
                }

                httpURLConnection = (HttpURLConnection) url.openConnection();

                httpURLConnection.setRequestMethod(getMethod());

                if (getRequestHeaders() != null) {
                    for (Map.Entry<String, String> entry : getRequestHeaders().entrySet()) {
                        httpURLConnection.setRequestProperty(entry.getKey(), entry.getValue());
                    }
                }

                if(getService().equals(Constants.SERVICE_OFFLINE_EVENTS)){
                    OutputStream os = httpURLConnection.getOutputStream();
                    os.write(((String)getParam("events")).getBytes("UTF-8"));
                    os.close();
                }

                int responseCode = httpURLConnection.getResponseCode();

                if (responseCode >= 200 && responseCode < 400) {

                    BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

                    StringBuilder responseBuilder = new StringBuilder();
                    String inputLine;
                    boolean firstLine = true;
                    while ((inputLine = reader.readLine()) != null) {
                        if (!firstLine) {
                            responseBuilder.append('\n');
                        }
                        firstLine = false;
                        responseBuilder.append(inputLine);
                    }
                    reader.close();
                    response = responseBuilder.toString();

                    success();

                } else {
                    error();
                }

            } catch (Exception e) {
                error();
                YouboraLog.error(e);
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
        }

        private void success() {
            originHandler.post(new Runnable() {
                @Override
                public void run() {
                    // Callbacks
                    if (successListenerList != null){
                        for (RequestSuccessListener l : successListenerList) {
                            l.onRequestSuccess(httpURLConnection, response);
                        }
                    }

                    if (onEverySuccessListenerList != null) {
                        for (RequestSuccessListener l : onEverySuccessListenerList) {
                            l.onRequestSuccess(httpURLConnection, response);
                        }
                    }
                }
            });
        }

        private void error() {
            originHandler.post(new Runnable() {
                @Override
                public void run() {
                    // Callbacks
                    if (errorListenerList != null) {
                        for (RequestErrorListener l : errorListenerList) {
                            l.onRequestError(httpURLConnection);
                        }
                    }

                    if (onEveryErrorListenerList != null) {
                        for (RequestErrorListener l : onEveryErrorListenerList) {
                            l.onRequestError(httpURLConnection);
                        }
                    }
                }
            });

            // Retry
            if (pendingAttempts > 0) {
                YouboraLog.warn("Request \"" + getService() + "\" failed. Retry \"" + (maxRetries + 1 - pendingAttempts) + "\" of " +
                        maxRetries + " in " + retryInterval + "ms.");
                try {
                    Thread.sleep(retryInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sendRequest();
            } else {
                YouboraLog.error("Aborting failed request \"" + getService() + "\". Max retries reached (" + maxRetries + ").");
            }
        }
    }

    /**
     * Request success interface.
     * @see #addOnSuccessListener(RequestSuccessListener)
     * @see #addOnEverySuccessListener(RequestSuccessListener)
     */
    public interface RequestSuccessListener {
        /**
         * Callback invoked when a {@link Request} has been successful.
         * @param connection the underlying {@link HttpURLConnection}
         * @param response the response of the request (if any)
         */
        void onRequestSuccess(HttpURLConnection connection, String response);
    }

    /**
     * Request error interface.
     */
    public interface RequestErrorListener {

        /**
         * Callback invoked when a {@link Request} failed.
         * @param connection the underlying {@link HttpURLConnection}
         */
        void onRequestError(HttpURLConnection connection);
    }

}
