package com.npaw.youbora.lib6.comm.transform.resourceparse;

import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnConfig;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnParsableResponseHeader;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser;
import com.npaw.youbora.lib6.YouboraLog;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Class that asynchronously tries to get information about the CDN where a given resource is
 * hosted.
 * The info we care about is the CDN code itself, the node host and node type.
 *
 * The CDN is queried with http HEAD requests. This only will work if the customer has properly
 * configured their CDN.
 *
 * When HEAD requests are performed against the resources, the CDN returns a set of headers that
 * containing info about the cdn header and / or cdn type.
 *
 * Each CDN is different; some require special headers to be set when the HEAD request is performed
 * and others don't. Also, the info can come back in any fashion of ways, sometimes both type and host
 * come in the same response header while sometimes they're in different headers. The format of these
 * response headers is also different from CDN to CDN, so a different regex is used for each CDN.
 *
 * Lastly, as the values indicating the CDN type are also different, we need a specific mapping for
 * each one.
 *
 * This is the process to add a new CDN to the parser.
 * <ol>
 *      <li>CDN code. The class that holds all the needed info is {@link CdnConfig}. It should be
 *      constructed passing the CDN code that represents this CDN. This is a YOUBORA code and can
 *      be found <a href="http://mapi.youbora.com:8081/cdns">here</a>.</li>
 *      <li>Response headers. The CDN will answer the HEAD request with headers that contain the
 *      info we're looking for. We should add as many {@link CdnParsableResponseHeader} as needed.
 *      The constructor needs three things:
 *      <ul>
 *          <li>Element. Specify if this header will contain the Host, Type, or both. Note that
 *          {@link com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnParsableResponseHeader.Element#HostAndType}
 *          is not the same as {@link com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnParsableResponseHeader.Element#TypeAndHost}.
 *          The order is the same as how will they will appear in the header response value. This can
 *          also have the "Name" value, indicating that this field value is actually the CDN name</li>
 *          <li>Header response name. Header from where the info will be got from.</li>
 *          <li>Regex. Regular expression to extract host and/or type. If only one of them is expected
 *          define one capturing group, and two otherwise.</li>
 *      </ul>
 *      </li>
 *      <li>Request headers. Call {@link CdnConfig#setRequestHeader(String, String)} if this CDN
 *      requires special headers to be set in order to respond with the info we want.</li>
 *      <li>Type parser. Once the CDN Type value is found (using one of the previously set
 *      {@link CdnParsableResponseHeader}) this parser will be called passing it the found value
 *      as a String. This parser should return {@link com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser.Type#Hit}
 *      or {@link com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser.Type#Miss}
 *      depending on the Type string parameter. If it doesn't match with what would you expect,
 *      return {@link com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser.Type#Unknown}
 *      instead.</li>
 *
 * </ol>
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class CdnParser {

    private List<CdnTransformListener> listeners = new ArrayList<>();
    //private boolean isBusy = true;

    private CdnConfig cdnConfig;

    // Previously "cached" responses
    private Map<Map<String, String>, Map<String, List<String>>> responses;

    // Extracted info
    private String cdnNodeHost;
    private String cdnNodeTypeString;
    private String cdnName;
    private CdnTypeParser.Type cdnNodeType = CdnTypeParser.Type.Unknown;

    /** Level3 CDN name constant. */
    public static final String CDN_NAME_LEVEL3 =        "Level3";
    /** Cloudfront CDN name constant. */
    public static final String CDN_NAME_CLOUDFRONT =    "Cloudfront";
    /** Akamai CDN name constant. */
    public static final String CDN_NAME_AKAMAI =        "Akamai";
    /** Highwindws CDN name constant. */
    public static final String CDN_NAME_HIGHWINDS =     "Highwindws";
    /** Fastly CDN name constant. */
    public static final String CDN_NAME_FASTLY =        "Fastly";
    /**
     * Balancer CDN name constant.
     * @see #setBalancerHeaderName(String)
     */
    public static final String CDN_BALANCER =           "Balancer";

    private static Map<String, CdnConfig> cdnDefinitions = new HashMap<String, CdnConfig>(){{

        CdnConfig cdnConfig;

        cdnConfig = new CdnConfig("LEVEL3");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.HostAndType, "X-WR-DIAG", "Host:(.+)\\sType:(.+)"))
                .setRequestHeader("X-WR-DIAG", "host")
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "TCP_HIT":
                            case "TCP_MEM_HIT":
                            case "TCP_IMS_HIT":
                                return Type.Hit;
                            case "TCP_MISS":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        put(CDN_NAME_LEVEL3, cdnConfig);

        cdnConfig = new CdnConfig("CLOUDFRT");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Host, "X-Amz-Cf-Id", "(.+)"))
                .addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Type, "X-Cache", "(\\S+)\\s.+"))
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "Hit":
                                return Type.Hit;
                            case "Miss":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        put(CDN_NAME_CLOUDFRONT, cdnConfig);

        cdnConfig = new CdnConfig("AKAMAI");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.TypeAndHost, "X-Cache", "(.+)\\sfrom\\s.+\\(.+\\/(.+)\\).*"))
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "TCP_HIT":
                                return Type.Hit;
                            case "TCP_MISS":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        put(CDN_NAME_AKAMAI, cdnConfig);

        cdnConfig = new CdnConfig("HIGHNEGR");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.HostAndType, "X-HW", ".+,[0-9]+\\.(.+)\\.(.+)"))
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "c":
                            case "x":
                                return Type.Hit;
                            default:
                                return Type.Miss;
                        }
                    }
                });

        put(CDN_NAME_HIGHWINDS, cdnConfig);

        cdnConfig = new CdnConfig("FASTLY");
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Host, "X-Served-By", "([^,\\s]+)$"))
                .addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Type, "X-Cache", "([^,\\s]+)$"))
                .setRequestHeader("X-WR-DIAG", "host")
                .setTypeParser(new CdnTypeParser() {
                    @Override
                    public Type parseCdnType(String type) {
                        switch (type) {
                            case "HIT":
                                return Type.Hit;
                            case "MISS":
                                return Type.Miss;
                            default:
                                return Type.Unknown;
                        }
                    }
                });

        put(CDN_NAME_FASTLY, cdnConfig);

        cdnConfig = new CdnConfig(null);
        cdnConfig.addParser(new CdnParsableResponseHeader(CdnParsableResponseHeader.Element.Name, null, "(.+)"));

        put(CDN_BALANCER, cdnConfig);

    }};

    private CdnParser(CdnConfig cdnConfig) {
        this.cdnConfig = cdnConfig;
    }

    /**
     * Returns the request responses from this CdnParser.
     * This is filled with the responses from the constructor, or created empty if null.
     * Then the performed request response (if any) is added to this map.
     * Call this method after "using" the CdnParser and pass the responses to the following
     * CdnParser so it can use the responses if it applies.
     * @return the request responses
     */
    public Map<Map<String, String>, Map<String, List<String>>> getResponses() {
        return responses;
    }

    /**
     * Returns the node host
     * @return the node host
     */
    public String getNodeHost() {
        return cdnNodeHost;
    }

    /**
     * Returns the raw value parsed from the header response as node type
     * @return the node type string
     */
    public String getNodeTypeString() {
        return cdnNodeTypeString;
    }

    /**
     * Returns the parsed node type as returned by a {@link CdnTypeParser}.
     * @return the node type
     */
    public CdnTypeParser.Type getNodeType() {
        return cdnNodeType;
    }

    /**
     * Start parsing the CDN for the given resource.
     * @param url the resource url to parse the CDN
     * @param responses previous http responses that may be re-used so we can avoid making them again
     */
    public void parse(String url, Map<Map<String, String>, Map<String, List<String>>> responses) {
        if (responses == null) {
            responses = new HashMap<>();
        }
        this.responses = responses;
        // If we already have a response for the request headers of this CDN, use it
        Map<String, List<String>> response = responses.get(this.cdnConfig.getRequestHeaders());
        if (response != null) {
            // Use the old response headers
            parseResponse(response);
        } else{
            // First time we query with these headers, perform Request
            requestResponse(url);
        }
    }

    private void requestResponse(String url) {
        Request r = createRequest(url, null);
        r.setMethod(Request.METHOD_HEAD);
        r.setRequestHeaders(cdnConfig.getRequestHeaders());
        /*for (Map.Entry<String, String> entry : cdnConfig.getRequestHeaders().entrySet()) {
            r.setParam(entry.getKey(), entry.getValue());
        }*/
        r.setMaxRetries(0);
        r.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response) {
                //Map<String, List<String>> requestHeaders = connection.getRequestProperties();
                Map<String, List<String>> responseHeaders = connection.getHeaderFields();

                responses.put(cdnConfig.getRequestHeaders(), responseHeaders);
                parseResponse(responseHeaders);
            }
        });
        r.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                done();
            }
        });
        r.send();
    }

    private void parseResponse(Map<String, List<String>> response) {

        for (CdnParsableResponseHeader parser : cdnConfig.getParsers()) {
            for (Map.Entry<String, List<String>> responseHeader : response.entrySet()) {
                if (parser.headerName != null &&
                        parser.headerName.equals(responseHeader.getKey())) {
                    // Get first value if more than one (rare case)
                    executeParser(parser, responseHeader.getValue().get(0));
                }
            }
        }
        done();
    }

    private void executeParser(CdnParsableResponseHeader parser, String headerValue) {

        Pattern pattern;

        try {
            pattern = Pattern.compile(parser.regexPattern, Pattern.CASE_INSENSITIVE);
        } catch (PatternSyntaxException ex) {
            YouboraLog.warn("Resource parser: error compiling regex: " + parser.regexPattern);
            return;
        }

        if (headerValue == null || headerValue.length() == 0) {
            YouboraLog.debug("Header value is null or empty");
            return;
        }

        Matcher m = pattern.matcher(headerValue);

        if (!m.matches()) {
            m.find();
            if (!m.matches()){
                return;
            }
        }

        String firstValue = m.group(1);

        if (firstValue == null || firstValue.length() == 0) {
            return;
        }

        cdnName = cdnConfig.getCode();

        switch (parser.element) {
            case Host:
                cdnNodeHost = firstValue;
                break;
            case Type:
                cdnNodeTypeString = firstValue;
                break;
            case HostAndType:
                cdnNodeHost = firstValue;
                cdnNodeTypeString = m.group(2);
                break;
            case TypeAndHost:
                cdnNodeTypeString = firstValue;
                cdnNodeHost = m.group(2);
                break;
            case Name:
                cdnName = firstValue.toUpperCase(Locale.US);
                break;
        }


        if (cdnNodeTypeString != null && cdnNodeType == CdnTypeParser.Type.Unknown) {
            CdnTypeParser typeParser = cdnConfig.getTypeParser();
            if (typeParser != null) {
                cdnNodeType = typeParser.parseCdnType(cdnNodeTypeString);
            }
        }
    }

    /**
     * This is a special case. The {@link CdnParser#CDN_BALANCER} is a custom CDN definition
     * that tries to get the CDN name directly from one of the headers. This method can be used
     * as a shortcut to creating a new CDN definition.
     * This is usually used with DNS-based load balance services, such as Cedexis.
     * @param cdnNameHeader the header response name where to get the CDN name from.
     */
    public static void setBalancerHeaderName(String cdnNameHeader) {
        cdnDefinitions.get(CDN_BALANCER).getParsers().get(0).headerName = cdnNameHeader;
    }

    /**
     * Create one of the pre-defined CDN definitions.
     * @param cdnName Name of the CDN
     * @return a {@link CdnParser} instance or null if the names does not match any CDN
     */
    public static CdnParser create(String cdnName) {
        CdnConfig cdnConfig = cdnDefinitions.get(cdnName);
        if (cdnConfig == null) {
            return null;
        } else {
            return new CdnParser(cdnConfig);
        }
    }

    private void done() {
        //isBusy = false;
        for (CdnTransformListener l : listeners) {
            l.onCdnTransformDone(this);
        }
    }

    /**
     * Add a listener that will be notified whenever this {@link CdnParser} finishes.
     * @param listener the listener to add.
     */
    public void addCdnTransformListener(CdnTransformListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener
     * @param listener the listener to remove
     * @return whether the listener has been removed or not.
     */
    public boolean removeCdnTransformListener(CdnTransformListener listener) {
        return listeners.remove(listener);
    }

    /**
     * Returns the Cdn name. This will be the code specified in the {@link CdnConfig#CdnConfig(String)}
     * constructor unless {@link com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnParsableResponseHeader.Element#Name}
     * is specified in a {@link CdnParsableResponseHeader} and a match is found.
     * @return the Cdn name
     */
    public String getCdnName() {
        return cdnName;
    }

    /**
     * Callback interface to inform observers that the parsing is done.
     */
    public interface CdnTransformListener {
        void onCdnTransformDone(CdnParser cdnParser);
    }

    /**
     * Adds the given CDN config to the cdn definitions.
     * @param cdnName The name that will identify the CDN
     * @param cdn The {@link CdnConfig} that defines the CDN
     */
    public static void addCdn(String cdnName, CdnConfig cdn) {
        cdnDefinitions.put(cdnName, cdn);
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }
}
